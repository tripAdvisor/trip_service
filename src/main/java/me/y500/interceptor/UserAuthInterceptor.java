package me.y500.interceptor;


import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import me.y500.dataobject.UserAuth;
import me.y500.dataobject.UserInfo;
import me.y500.dataobject.WxUserInfo;
import me.y500.mapper.UserAuthMapper;
import me.y500.mapper.UserInfoMapper;
import me.y500.mapper.WxUserInfoMapper;
import me.y500.result.BaseResult;
import me.y500.util.Constant;
import me.y500.util.Util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;



public class UserAuthInterceptor extends HandlerInterceptorAdapter{

	private final static Logger logger = Logger.getLogger(UserAuthInterceptor.class);
	@Autowired
    private UserAuthMapper userAuthMapper;
	@Autowired
    private UserInfoMapper userInfoMapper;
	@Autowired
    private WxUserInfoMapper wxUserInfoMapper;
	
//	@Autowired
//    private UserRepository userRepository;
	
	

	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		PrintWriter pwout = response.getWriter();
		 String auth = request.getHeader("Authorization");
	       
	        try {
	        	UserAuth userAuth = null;
	        	if (auth != null && auth.length() > 0 && auth.startsWith("device ")) {
					auth = auth.substring(7);
					userAuth = userAuthMapper.getUserByDeviceToken(auth);
					if (userAuth == null) {
						BaseResult result = new BaseResult();
						result.setStat(14);
						result.setMsg("网络异常~");
						pwout.print(Util.VerifyObjToXml(result, "plain"));
						return false;
					}
				}else if (auth != null && auth.length() > 0 && auth.startsWith("phone ")) {
					auth = auth.substring(6);
					userAuth = userAuthMapper.getUserByPhoneToken(auth);
				}else if (auth != null && auth.length() > 0 && auth.startsWith("wx ")) {
					auth = auth.substring(3);
					userAuth = userAuthMapper.getUserByWxToken(auth);
				}else {
//					return true;
					BaseResult result = new BaseResult();
					result.setStat(14);
					result.setMsg("网络异常~");
					pwout.print(Util.VerifyObjToXml(result, "plain"));
					return false;
				}
	        	if (userAuth != null) {
					UserInfo userBean = userInfoMapper.selectByPrimaryKey(userAuth.getUid());
					if (userBean == null) {
						UserInfo user = new UserInfo();
						user.setUid(userAuth.getUid());
						request.setAttribute(Constant.SESSION_USERBEAN, user);	
					}else {
						request.setAttribute(Constant.SESSION_USERBEAN, userBean);
					}
					
					if (StringUtils.isNotBlank(userAuth.getWxId())) {
						WxUserInfo wxUserInfo = wxUserInfoMapper.selectByOpenid(userAuth.getWxId());
						if (wxUserInfo != null) {
							wxUserInfo.setUid(userAuth.getUid());
							request.setAttribute(Constant.SESSION_WX, wxUserInfo);
						}
					}
					
				}else {
					BaseResult result = new BaseResult();
					result.setStat(13);
					result.setMsg("网络异常~");
					pwout.print(Util.VerifyObjToXml(result, "plain"));
					return false;
				}
	        } catch (Exception e) {
				logger.info(Util.exceptionMsg(e));
			}
	        return true;
	}
}
