package me.y500.interceptor;

import java.net.URLDecoder;
import java.security.Key;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import me.y500.bo.SecretBo;
import me.y500.util.Tools;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;



/**
 * 该拦截器用与记录用户操作日志，便于排错
 * 
 * @author Alvise
 * 
 */
public class OperationLogInterceptor extends HandlerInterceptorAdapter {
    private final static Logger logger = Logger.getLogger(OperationLogInterceptor.class);
    private Tools tools;
    @Autowired 
    private SecretBo secretBo;
    
    public Tools getTools() {
		return tools;
	}

	public void setTools(Tools tools) {
		this.tools = tools;
	}

    
	@SuppressWarnings("rawtypes")
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String platform = request.getParameter("platform");
		String p = request.getParameter("p");
    	StringBuilder logText = new StringBuilder();
        String servletPath = request.getRequestURI();
        logText.append("servletPath:").append(servletPath);
        String auth = request.getHeader("Authorization");
        logText.append("?auth=").append(auth);
        String[] authArray = {};
//        if (auth != null && auth.length() > 0 ) {
//        	if (auth.startsWith("Basic ")) {
//                auth = auth.substring(6);
//                // 校验数据
//                authArray = Base64Util.getFromBASE64(auth).split(":");
//            }else if (auth.startsWith("High ")) {
//    			Key k = AESCoder.toKey(AESCoder.parseHexStr2Byte(UserCheckInterceptor.huoliKey));
//    			auth = auth.substring(5);
//    			auth = Base64Util.getFromBASE64(auth);
//    			byte[] decryptData = AESCoder.decrypt(AESCoder.parseHexStr2Byte(auth), k);
//    			authArray = new String(decryptData).split(":");
//    		}
//                if (authArray != null && authArray.length == 2) {
//                    // 校验密码
//                    authArray[0] = URLDecoder.decode(authArray[0], "utf-8");
//                    logText.append("&username=" + authArray[0] + "&password=" + authArray[1]);
//                }
//		}
        
        

        // 取包头中的useragent信息
        String userAgent = request.getHeader("UserAgent");
        if (userAgent == null || userAgent.length() == 0) {
            userAgent = request.getHeader("clientinfo");
        }
        logText.append("&userAgent=" + userAgent);
        String ipAddressStr = request.getHeader("X-Real-IP");
        if (ipAddressStr == null || ipAddressStr.length() == 0) {
            ipAddressStr = request.getRemoteAddr();
        }
        logText.append("&ip=" + ipAddressStr);
        Map map = request.getParameterMap();
        String data = "";
        String time = "";
        String sign = "";
        String mode = "";
        if (map != null && !map.isEmpty()) {
            Iterator iter = map.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                Object key = entry.getKey();
                Object val = request.getParameter((String) key);
                if (key.equals("data")) {
                	data = (String)val;
				}else if(key.equals("time")){
					time = (String)val;
				}else if (key.equals("sign")) {
					sign = (String)val;
				}else if (key.equals("mode")) {
					mode = (String)val;
				}
            }
            data = secretBo.decrypt(data, time, sign, mode);
            logText.append("&").append("data").append("=").append(data);
//            while (iter.hasNext()) {
//                Map.Entry entry = (Map.Entry) iter.next();
//                Object key = entry.getKey();
//                Object val = request.getParameter((String) key);
//                String data = "";
//                String time = "";
//                String sign = "";
//                if (key.equals("data")) {
//                	data = secretBo.decrypt(data, time, sign);
//				}
//                logText.append("&").append(key).append("=").append(val);
//            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(logText.toString());
        }
        return true;
    }
	 public void afterCompletion(
				HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
				throws Exception {
	    	tools.removePublicParams();
		}
}
