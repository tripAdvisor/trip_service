//package me.y500.mongodb;
//
//import java.util.List;
//import java.util.regex.Pattern;
//
//import me.y500.dataobject.HotelRatePlan;
//
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.query.Criteria;
//import org.springframework.data.mongodb.core.query.Query;
//
//
//public class HotelRatePlanRepository implements AbstractRepository{
//	private Logger logger = Logger.getLogger(HotelRatePlanRepository.class);
//	@Autowired
//	private MongoTemplate mongoTemplate;
//	
//	@Override
//	public void insert(HotelRatePlan rate) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public HotelRatePlan findOne(String hid, String date) {
//		// TODO Auto-generated method stub
//		Criteria criteria = Criteria.where("hid").is(hid).and("date").is(date);
//		Query query = new Query();
//		query.addCriteria(criteria);
//		HotelRatePlan rate = mongoTemplate.findOne(query, HotelRatePlan.class);
//		return rate;
//	}
//	
//	public List<HotelRatePlan> findIn(List<String> hids, String date) {
//		// TODO Auto-generated method stub
//		if(hids != null && hids.size() > 0){
//			Criteria criteria = Criteria.where("hid").in(hids).and("date").is(date);
//			Query query = new Query();
//			query.addCriteria(criteria);
//			List<HotelRatePlan> rate = mongoTemplate.find(query, HotelRatePlan.class);
//			return rate;
//		}
//		return null;
//	}
//
//	@Override
//	public List<HotelRatePlan> findAll() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public List<HotelRatePlan> findByRegex(String regex) {
//		// TODO Auto-generated method stub
//		Pattern pattern = Pattern.compile(regex,Pattern.CASE_INSENSITIVE);     
//        Criteria criteria = new Criteria("name").regex(pattern.toString());     
//          return mongoTemplate.find(new Query(criteria), HotelRatePlan.class); 
//	}
//
//	@Override
//	public void removeOne(String id) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void removeAll() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void findAndModify(String id) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public List<HotelRatePlan> findBetween(String hid, String checkIn, String checkOut) {
//		// TODO Auto-generated method stub
//		Criteria criteria = Criteria.where("hid").is(hid).and("date").gte(checkIn).lt(checkOut);
//		Query query = new Query();
//		query.addCriteria(criteria);
//		List<HotelRatePlan> rates = mongoTemplate.find(query, HotelRatePlan.class);
//		return rates;
//	}
//
//}
