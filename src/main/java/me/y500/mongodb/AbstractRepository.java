//package me.y500.mongodb;
//
//import java.util.List;
//
//import me.y500.dataobject.HotelRatePlan;
//
//
//public interface AbstractRepository {
//	/** 
//     *  
//     *<b>function:</b>添加对象 
//     * @author wuc 
//     * @createDate 2014-10-24 11:41:30 
//     */  
//    public void insert(HotelRatePlan rate);   
//      
//    /** 
//     *  
//     *<b>function:</b>根据ID查找对象 
//     * @author wuc 
//     * @createDate 2014-10-24 11:41:30 
//     */  
//    public HotelRatePlan findOne(String hid, String date);     
//    /** 
//     *  
//     *<b>function:</b>查询所有 
//     * @author wuc 
//     * @createDate 2014-10-24 11:41:30 
//     */  
//    public List<HotelRatePlan> findAll();     
//      
//    public List<HotelRatePlan> findByRegex(String regex);  
//    /** 
//     *  
//     *<b>function:</b>删除指定的ID对象 
//     * @author wuc 
//     * @createDate 2014-10-24 11:41:30 
//     */  
//    public void removeOne(String id);     
//    /** 
//     *  
//     *<b>function:</b>删除所有 
//     * @author wuc 
//     * @createDate 2014-10-24 11:41:30 
//     */  
//    public void removeAll();     
//    /** 
//     * 通过ID找到并修改 
//     *<b>function:</b> 
//     * @author wuc 
//     * @createDate 2014-10-24 11:41:30 
//     */  
//    public void findAndModify(String id);   
//    
//    /** 
//     * 查找范围值
//     *<b>function:</b> 
//     * @author wuc 
//     * @createDate 2014-10-24 11:41:30 
//     */  
//    public List<HotelRatePlan> findBetween(String date, String checkIn, String checkOut);   
//  
//      
//}
