package me.y500.mongodb.result;


import me.y500.mongodb.result.SimpleAnswer;
import me.y500.mongodb.result.SimpleUser;

import org.apache.commons.lang.builder.ToStringBuilder;


public class DetailAnswerResult {
	private SimpleUser guest;
	private SimpleAnswer answer;
	public String toString() {
		return ToStringBuilder.reflectionToString(DetailAnswerResult.class);
	}
	public SimpleUser getGuest() {
		return guest;
	}
	public void setGuest(SimpleUser guest) {
		this.guest = guest;
	}
	public SimpleAnswer getAnswer() {
		return answer;
	}
	public void setAnswer(SimpleAnswer answer) {
		this.answer = answer;
	}
	
}
