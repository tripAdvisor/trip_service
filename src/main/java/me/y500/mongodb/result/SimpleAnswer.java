package me.y500.mongodb.result;

import java.text.ParseException;
import java.util.List;

import me.y500.mongodb.dataobject.Answer;
import me.y500.result.Image;
import me.y500.util.DateUtil;


public class SimpleAnswer {
	private String aid;
	private String content;
	private String createTime;
	private List<Image> images;
	
	public SimpleAnswer(Answer answer, Long uid, List<Image> images) {
		super();
		this.aid = answer.getAid();
		this.content = answer.getContent();
		try {
			this.createTime = DateUtil.format(DateUtil.parse(answer.getCreate_time(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.images = images;
	}

	

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}





	public List<Image> getImages() {
		return images;
	}



	public void setImages(List<Image> images) {
		this.images = images;
	}



	public String getAid() {
		return aid;
	}



	public void setAid(String aid) {
		this.aid = aid;
	}

	
}
