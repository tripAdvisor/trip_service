package me.y500.mongodb.result;


import org.apache.commons.lang.StringUtils;

import me.y500.mongodb.dataobject.User;
import me.y500.util.Constant;


public class SimpleUser {
	private String uid;
	private String nick;
	private String level;
	private String image;
	private String about;
	
	public SimpleUser(User userInfo) {
		super();
		if (userInfo != null) {
			this.uid = userInfo.getUid();
			this.nick = userInfo.getNick();
			this.level = userInfo.getLevel();
			this.image = StringUtils.isBlank(userInfo.getImage()) ? Constant.IMAGE_ICON_PATH : userInfo.getImage();
			this.about = userInfo.getAbout();
		}
		
	}
	

	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	
	public String getUid() {
		return uid;
	}


	public void setUid(String uid) {
		this.uid = uid;
	}


	public String getLevel() {
		return level;
	}


	public void setLevel(String level) {
		this.level = level;
	}


	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getAbout() {
		return about;
	}
	public void setAbout(String about) {
		this.about = about;
	}
	
}
