package me.y500.mongodb;

import java.util.List;

import me.y500.mongodb.dataobject.Answer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class AnswerRepository {
	@Autowired
	private MongoTemplate mongoTemplate;

	public Answer selectByPrimaryKey(String id, String type) {
		// TODO Auto-generated method stub
		Criteria criteria = Criteria.where("aid").is(id).and("type").is(type);
		Query query = new Query();
		query.addCriteria(criteria);
		Answer rate = mongoTemplate.findOne(query, Answer.class);
		return rate;
	}

	public long selectByQid(String qid) {
		// TODO Auto-generated method stub
		Criteria criteria = Criteria.where("qlqid").is(qid);
		Query query = new Query();
		query.addCriteria(criteria);
		long rate = mongoTemplate.count(query, Answer.class);
		return rate;
	}
	
	public List<Answer> selectListByQid(String qid, int num) {
		// TODO Auto-generated method stub
		Criteria criteria = Criteria.where("qid").is(qid);
		Query query = new Query();
		query.addCriteria(criteria);
		query.with(new Sort(Direction.DESC, "create_time")).limit(num);
		List<Answer> rate = mongoTemplate.find(query, Answer.class);
		return rate;
	}

	public List<Answer> selectOldListByQid(String qid, String foodTime, int num) {
		// TODO Auto-generated method stub
		Criteria criteria = Criteria.where("qid").is(qid).and("create_time")
				.lt(foodTime);
		Query query = new Query();
		query.addCriteria(criteria);
		query.with(new Sort(Direction.DESC, "create_time")).limit(num);
		List<Answer> rate = mongoTemplate.find(query, Answer.class);
		return rate;
	}

	public void insert(Answer answer) {
		// TODO Auto-generated method stub
		mongoTemplate.save(answer);
	}

	public void update(Answer answer) {
		Criteria criteria = Criteria.where("aid").is(answer.getAid());
		Query query = new Query(criteria);
		Update update = Update.update("is_accept", answer.getIs_accept()).set(
				"image_name", answer.getImage_name());
		mongoTemplate.updateMulti(query, update, Answer.class);
	}
}
