package me.y500.mongodb;

import java.util.List;


import me.y500.mongodb.dataobject.Question;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

public class QuestionRepository {
	@Autowired
	private MongoTemplate mongoTemplate;
	
	public Question selectByPrimaryKey(String id) {
		// TODO Auto-generated method stub
		Criteria criteria = Criteria.where("qid").is(id);
		Query query = new Query();
		query.addCriteria(criteria);
		Question rate = mongoTemplate.findOne(query, Question.class);
		return rate;
	}
//	select 
//    *
//    from question
//    where 1=1
//    <if test="destination!=null">
//			and destination = #{destination}
//	</if> 
//	order by create_time desc limit ${num}
	public List<Question> selectList(int num) {
		// TODO Auto-generated method stub
//		Criteria criteria = Criteria.where("id").is(241908);
		Query query = new Query();
//		query.addCriteria(criteria);
		query.with(new Sort(Direction.DESC, "create_time")).limit(num);
		List<Question> rate = mongoTemplate.find(query, Question.class);
//		List<Question> rate = mongoTemplate.findAll(Question.class);
		return rate;
	}
	
	
//	 select 
//	   *
//	    from question
//	    where 
//	    <![CDATA[ 
//	    id > #{footAid} 
//	    ]]>
//	      <if test="destination!=null">
//				and destination=#{destination}
//		</if> 
//		order by create_time desc  limit ${num}
	
	public List<Question> selectOldList(String foodTime, int num) {
		// TODO Auto-generated method stub
		Criteria criteria = Criteria.where("create_time").lt(foodTime);
		Query query = new Query();
		query.with(new Sort(Direction.DESC, "create_time ")).limit(num);
		List<Question> rate = mongoTemplate.find(query, Question.class);
		return rate;
	}
	
//	 <insert id="insert" parameterType="me.y500.dataobject.Question" >
//	    insert into question (uid, title, 
//	      destination, create_time, status, 
//	      point, update_time, 
//	      content, label,image_name)
//	    values (#{uid,jdbcType=BIGINT}, #{title,jdbcType=VARCHAR}, 
//	      #{destination,jdbcType=VARCHAR}, sysdate(), #{status,jdbcType=INTEGER}, 
//	      #{point,jdbcType=INTEGER}, sysdate(), 
//	      #{content,jdbcType=LONGVARCHAR}, #{label,jdbcType=LONGVARCHAR},#{imageName,jdbcType=LONGVARCHAR})
//	  </insert>
	    
	    public void insert(Question question) {
			// TODO Auto-generated method stub
//			Criteria criteria = Criteria.
			mongoTemplate.save(question);
		}
}
