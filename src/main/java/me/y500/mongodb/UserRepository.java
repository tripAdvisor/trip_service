package me.y500.mongodb;

import java.util.List;

import me.y500.mongodb.dataobject.Answer;
import me.y500.mongodb.dataobject.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class UserRepository {
	@Autowired
	private MongoTemplate mongoTemplate;

	public User selectByPrimaryKey(String uid) {
		// TODO Auto-generated method stub
		Criteria criteria = Criteria.where("uid").is(uid);
		Query query = new Query();
		query.addCriteria(criteria);
		User rate = mongoTemplate.findOne(query, User.class);
		return rate;
	}

	public void insert(User user) {
		// TODO Auto-generated method stub
		mongoTemplate.save(user);
	}

	public void update(User user) {
		Criteria criteria = Criteria.where("id").is(user.getUid());
		Query query = new Query(criteria);
		Update update = Update.update("nick", user.getNick()).set(
				"about", user.getAbout()).set("sex", user.getSex());
		mongoTemplate.updateMulti(query, update, User.class);
	}
}
