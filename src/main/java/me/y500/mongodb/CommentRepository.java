//package me.y500.mongodb;
//
//import java.util.List;
//
//import me.y500.mongodb.dataobject.Answer;
//import me.y500.mongodb.dataobject.Comment;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Sort;
//import org.springframework.data.domain.Sort.Direction;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.query.Criteria;
//import org.springframework.data.mongodb.core.query.Query;
//import org.springframework.data.mongodb.core.query.Update;
//
//public class CommentRepository {
//	@Autowired
//	private MongoTemplate mongoTemplate;
//
//
//	public List<Comment> selectListByAid(String aid, int num, String type) {
//		// TODO Auto-generated method stub
//		Criteria criteria = Criteria.where("aid").is(aid).and("type").is(type);
//		Query query = new Query();
//		query.addCriteria(criteria);
//		query.with(new Sort(Direction.DESC, "create_time")).limit(num);
//		List<Comment> rate = mongoTemplate.find(query, Comment.class);
//		return rate;
//	}
//
//	public List<Comment> selectOldListByAid(String aid, String foodTime, int num, String type) {
//		// TODO Auto-generated method stub
//		Criteria criteria = Criteria.where("aid").is(aid).and("type").is(type).and("create_time")
//				.lt(foodTime);
//		Query query = new Query();
//		query.addCriteria(criteria);
//		query.with(new Sort(Direction.DESC, "create_time")).limit(num);
//		List<Comment> rate = mongoTemplate.find(query, Comment.class);
//		return rate;
//	}
//
//	public void insert(Comment comment) {
//		// TODO Auto-generated method stub
//		mongoTemplate.save(comment);
//	}
//
//	
//}
