package me.y500.mapper;

import java.util.List;

import me.y500.dataobject.CityInfo;

public interface CityInfoMapper {
	List<CityInfo> select(Integer provinceId);
}
