package me.y500.mapper;

import java.util.List;

import me.y500.dataobject.Agree;

public interface AgreeMapper {
	Agree selectByPrimaryKey(Long id);
	int insert(Agree query);
	List<Agree> select(Agree query);
}
