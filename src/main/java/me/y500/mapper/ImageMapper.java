package me.y500.mapper;

import java.util.List;

import me.y500.dataobject.Image;

public interface ImageMapper {
	int insert(Image image);
	List<Image> selectByName(Long name);
}
