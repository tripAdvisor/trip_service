package me.y500.mapper;

import me.y500.dataobject.UserVerifyLog;


public interface UserVerifyLogMapper {
	UserVerifyLog select(UserVerifyLog query);
	int update(UserVerifyLog query);
	int insert(UserVerifyLog query);
}
