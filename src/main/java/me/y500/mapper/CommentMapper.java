package me.y500.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import me.y500.dataobject.Answer;
import me.y500.dataobject.Comment;

public interface CommentMapper {
	int insert(Comment comment);
	List<Comment> selectListByAid(@Param("aid")long aid,@Param("num")int num);
	List<Comment> selectOldListByAid(@Param("aid")long aid,@Param("footTime")String footTime,@Param("num")int num);
	List<Comment> select(@Param("aid")long aid);
}
