package me.y500.mapper;

import me.y500.dataobject.UserInfo;


public interface UserInfoMapper {
	 UserInfo selectByPrimaryKey(Long uid);
	 int insert(UserInfo userInfo);
	 int update(UserInfo userInfo);
}
