package me.y500.mapper;

import java.util.List;

import me.y500.dataobject.ProvinceInfo;

public interface ProvinceInfoMapper {
	List<ProvinceInfo> select();
}
