package me.y500.mapper;

import java.util.List;

import me.y500.dataobject.Follow;

public interface FollowMapper {
	List<Follow> select(Follow query);
	int insert(Follow query);
}
