package me.y500.mapper;
import java.util.List;

import me.y500.dataobject.Answer;
import me.y500.dataobject.Question;

import org.apache.ibatis.annotations.Param;

public interface AnswerMapper {
	Answer selectByPrimaryKey(Long id);
	List<Answer> selectListByQid(@Param("qid")long qid,@Param("num")int num);
	List<Answer> selectOldListByQid(@Param("qid")long qid,@Param("footTime")String footTime,@Param("num")int num);
	int insert(Answer answer);
	int update(Answer answer);
	List<Answer> selectByQid(Long qid);
	List<Answer> selectByUid(Long uid);
}
