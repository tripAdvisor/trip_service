package me.y500.mapper;

import java.util.List;

import me.y500.dataobject.Follow;
import me.y500.dataobject.SameQuestion;

public interface SameQuestionMapper {
	List<SameQuestion> select(SameQuestion query);
	int insert(SameQuestion query);
}
