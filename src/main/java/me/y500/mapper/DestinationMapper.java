package me.y500.mapper;

import java.util.List;

import me.y500.dataobject.Destination;

public interface DestinationMapper {
	List<Destination> select();
}
