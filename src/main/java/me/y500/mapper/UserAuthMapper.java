package me.y500.mapper;

import java.util.List;

import me.y500.dataobject.UserAuth;


public interface UserAuthMapper {
	UserAuth getUserByDeviceToken(String deviceToken);
	UserAuth getUserByPhoneToken(String phoneToken);
	UserAuth getUserByWxToken(String wxToken);
	UserAuth getUserByWxId(String wxId);
	Integer insert(UserAuth query);
	UserAuth getUserById(int loginId);
	Integer update(UserAuth query);
	UserAuth getUserByPhone(String phone);
	List<UserAuth> queryByData(UserAuth query);
	Integer insertUser(UserAuth query);
	Integer logout(int uid);
	Integer updateLoginId(UserAuth query);
	UserAuth getUser(UserAuth query);
	int deleteByUid(int uid);
	int unBindingPhone(int uid);
	int unBindingWx(int uid);
	int updateImei(UserAuth query);
}
