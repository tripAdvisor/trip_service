package me.y500.mapper;

import java.util.List;

import me.y500.dataobject.UserVerifyInfo;


public interface UserVerifyInfoMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(UserVerifyInfo record);

    List<UserVerifyInfo> queryOfVerifyInfo(UserVerifyInfo query);

    Integer getVerifyInfoCount(UserVerifyInfo query);
    
    int insertusetime(UserVerifyInfo query);
    
    int updateverify(UserVerifyInfo query);

}