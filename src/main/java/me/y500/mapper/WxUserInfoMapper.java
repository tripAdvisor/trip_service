package me.y500.mapper;

import me.y500.dataobject.WxUserInfo;


public interface WxUserInfoMapper {
	int insert(WxUserInfo query);
	int update(WxUserInfo query);
	WxUserInfo selectByOpenid(String openid);
	int updateById(WxUserInfo query);
}
