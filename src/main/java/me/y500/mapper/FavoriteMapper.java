package me.y500.mapper;

import java.util.List;

import me.y500.dataobject.Favorite;
import me.y500.dataobject.SameQuestion;

public interface FavoriteMapper {
	List<Favorite> select(Favorite query);
	int insert(Favorite query);
}
