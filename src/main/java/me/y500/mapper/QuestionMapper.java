package me.y500.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import me.y500.dataobject.Question;

public interface QuestionMapper {
	Question selectByPrimaryKey(Long id);
	List<Question> selectList(@Param("num")int num, @Param("destinationCode")String destinationCode);
	List<Question> selectOldList(@Param("footTime")String footTime, @Param("destinationCode")String destinationCode, @Param("num")int num);
	int insert(Question record);
	int update(Question question);
	List<Question> selectByUid(Long uid);
}
