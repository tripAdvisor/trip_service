package me.y500.test;

import java.io.File;
import java.util.List;

import me.y500.dataobject.Question;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriter.MaxFieldLength;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.wltea.analyzer.lucene.IKAnalyzer;
import org.wltea.analyzer.lucene.IKQueryParser;
import org.wltea.analyzer.lucene.IKSimilarity;



public class Demo1 {
	@Autowired
	private MongoTemplate mongoTemplate;
	public void test(){
		try {
			List<Question> questions = mongoTemplate.findAll(Question.class);
			 //先在数据库中拿到要创建索引的数据 
//	      Mongo mongo = new Mongo();
//	      DB db = mongo.getDB("zhang");
//	      DBCollection msg = db.getCollection("test3");
	      for (Question question : questions) {
	          //是否重新创建索引文件，false：在原有的基础上追加
	          boolean create = true;
	          //创建索引
	          Directory directory = FSDirectory.open(new File("E:\\lucene\\index"));
	          Analyzer analyzer = new IKAnalyzer();//IK中文分词器 
	          IndexWriter indexWriter = new IndexWriter(directory,analyzer,MaxFieldLength.LIMITED);

	          //System.out.println(cursor.next().get("text").toString());
	          Document doc = new Document();
	          Field fieldText = new Field("text",question.getContent().toString(),Field.Store.YES, 
	                        Field.Index.ANALYZED, Field.TermVector.WITH_POSITIONS_OFFSETS);
	          doc.add(fieldText);
	          indexWriter.addDocument(doc);
	          //optimize()方法是对索引进行优化
	          indexWriter.optimize();     
	          //最后关闭索引
	          indexWriter.close();
	      }
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
      
      
      
//      //是否重新创建索引文件，false：在原有的基础上追加
//      boolean create = true;
//      //创建索引
//      Directory directory = FSDirectory.open(new File("E:\\lucene\\index"));
//      Analyzer analyzer = new IKAnalyzer();//IK中文分词器 
//      IndexWriter indexWriter = new IndexWriter(directory,analyzer,MaxFieldLength.LIMITED);
//
//	  boolean exist = cursor.hasNext();
//      while(exist){
//          //System.out.println(cursor.next().get("text").toString());
//          Document doc = new Document();
//          Field fieldText = new Field("text",cursor.next().get("text").toString(),Field.Store.YES, 
//                    Field.Index.ANALYZED, Field.TermVector.WITH_POSITIONS_OFFSETS);
//          doc.add(fieldText);
//          indexWriter.addDocument(doc);
//          exist = cursor.hasNext();
//      }
//      cursor = null;
//      //optimize()方法是对索引进行优化
//      indexWriter.optimize();     
//      //最后关闭索引
//      indexWriter.close();
  
	public void comment(){
		try {
			long starttime = System.currentTimeMillis();
	        IndexReader reader =IndexReader.open(FSDirectory.open(new File("E:\\lucene\\index")),true); 
	        IndexSearcher searcher = new IndexSearcher(reader);
	        searcher.setSimilarity(new IKSimilarity());   //在索引器中使用IKSimilarity相似度评估器 
	        //String[] keys = {"4","testtest"};      //关键字数组
	        //String[] fields = {"id","title"};  //搜索的字段
	        //BooleanClause.Occur[] flags = {BooleanClause.Occur.MUST,BooleanClause.Occur.MUST};    //BooleanClause.Occur[]数组,它表示多个条件之间的关系 
	        //使用 IKQueryParser类提供的parseMultiField方法构建多字段多条件查询
	        //Query query = IKQueryParser.parseMultiField(fields,keys, flags);     //IKQueryParser多个字段搜索  
	        Query query = IKQueryParser.parse("text","上海人");  //IK搜索单个字段       
	        IKAnalyzer analyzer = new IKAnalyzer();
	        //Query query =MultiFieldQueryParser.parse(Version.LUCENE_CURRENT, keys, fields, flags,analyzer);   //用MultiFieldQueryParser得到query对象   
	        System.out.println("query:"+query.toString()); //查询条件    
	        /*TopScoreDocCollector topCollector = TopScoreDocCollector.create(searcher.maxDoc(), false);
	        searcher.search(query,topCollector);
	         
	        ScoreDoc[] docs = topCollector.topDocs(3).scoreDocs;
	        System.out.println(docs.length);*/
	         
	        /**
	        *得到TopDocs对象之后,可以获取它的成员变量totalHits和scoreDocs.这两个成员变量的访问权限是public的,所以可以直接访问
	        */
	        TopDocs topDocs = searcher.search(query,1000001);
	        Integer count = topDocs.totalHits;
	        ScoreDoc[] scoreDocs = topDocs.scoreDocs;
	        for(int i = 0;i<count;i++){
	            ScoreDoc scoreDoc = scoreDocs[i];
	            Document document = searcher.doc(scoreDoc.doc);
	            document.get("text");
	        }
	        System.out.println("查找数据量:"+count);
	        long endtime = System.currentTimeMillis();
	        System.out.println(endtime-starttime);
	        reader.close(); //关闭索引  
		} catch (Exception e) {
			// TODO: handle exception
		}
		 
	}
	
	

  }
	
	
    
       
