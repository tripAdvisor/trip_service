package me.y500.param;

public class PublicParams {
	private String source;
	private String os;
	private String appname;
	private String version;
	private String ptype;
	private String imei;//android的imei（可以刷，会变化）
	private String mac; //iphone为初始广告id（唯一值）
	private String platform;
	private String deviceId;//android唯一id（唯一值）
	private String adId;//iphone广告id，重置后会变化（重置后会变化）
	private String webId;//iphone里safari唯一秘钥（不会变）
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public String getAppname() {
		return appname;
	}
	public void setAppname(String appname) {
		this.appname = appname;
	}
	public String getVersion() {
		if (version.length() == 5) {
			version = version + ".0";
		}
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getPtype() {
		return ptype;
	}
	public void setPtype(String ptype) {
		this.ptype = ptype;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getAdId() {
		return adId;
	}
	public void setAdId(String adId) {
		this.adId = adId;
	}
	public String getWebId() {
		return webId;
	}
	public void setWebId(String webId) {
		this.webId = webId;
	}
	
}
