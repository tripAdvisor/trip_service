package me.y500.param;



public class DetailParams extends BaseParams{
	private String qid;
	private String footTime;
	private String type;
	private int num;

	
	public String getQid() {
		return qid;
	}
	public void setQid(String qid) {
		this.qid = qid;
	}
	public String getFootTime() {
		return footTime;
	}
	public void setFootTime(String footTime) {
		this.footTime = footTime;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	
	
}
