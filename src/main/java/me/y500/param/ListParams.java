package me.y500.param;

public class ListParams extends BaseParams{
	private String destination;
	private String listType;//new 最新问题 mostanswer 最多回答 noanswer 未解答 mostfollow 最多关注
	private String footTime;
	private String type;
	private Integer num;
	
	
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getListType() {
		return listType;
	}
	public void setListType(String listType) {
		this.listType = listType;
	}
	
	public String getFootTime() {
		return footTime;
	}
	public void setFootTime(String footTime) {
		this.footTime = footTime;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
}
