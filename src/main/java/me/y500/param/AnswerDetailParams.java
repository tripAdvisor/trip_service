package me.y500.param;

public class AnswerDetailParams {
	private String aid;
	private String footTime;
	private String type;
	private int num;
	
	public String getAid() {
		return aid;
	}
	public void setAid(String aid) {
		this.aid = aid;
	}
	
	public String getFootTime() {
		return footTime;
	}
	public void setFootTime(String footTime) {
		this.footTime = footTime;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	
}
