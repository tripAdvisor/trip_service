package me.y500.dataobject;

import java.io.Serializable;
import java.util.Date;

public class UserVerifyInfo implements Serializable {

    private static final long serialVersionUID = -5139158250504764901L;

    private Integer id;

    private String loginname;
    
    private Integer uid;

    private Date regTime;

    private Date lostTime;

    private String verifyCode;

    private Integer status;
    private String extraInfo;//扩展信息
    private String messageid;//短信通道返回的id
    private Integer smsSuccess;//0 不成功 1 成功
    
    private String appname;//来源
    
    private Date userTime;//验证码使用时间
    
    private Integer regType;//注册码类型（0、预注册 1、其他 2、手机号存在验证后登陆流程的验证码）
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname == null ? null : loginname.trim();
    }

    public Date getRegTime() {
        return regTime;
    }

    public void setRegTime(Date regTime) {
        this.regTime = regTime;
    }

    public Date getLostTime() {
        return lostTime;
    }

    public void setLostTime(Date lostTime) {
        this.lostTime = lostTime;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode == null ? null : verifyCode.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }



	public String getExtraInfo() {
		return extraInfo;
	}

	public void setExtraInfo(String extraInfo) {
		this.extraInfo = extraInfo;
	}

	public String getMessageid() {
		return messageid;
	}

	public void setMessageid(String messageid) {
		this.messageid = messageid;
	}

	public Integer getSmsSuccess() {
		return smsSuccess;
	}

	public void setSmsSuccess(Integer smsSuccess) {
		this.smsSuccess = smsSuccess;
	}



	public String getAppname() {
		return appname;
	}

	public void setAppname(String appname) {
		this.appname = appname;
	}



	public Date getUserTime() {
		return userTime;
	}

	public void setUserTime(Date userTime) {
		this.userTime = userTime;
	}

	public Integer getRegType() {
		return regType;
	}

	public void setRegType(Integer regType) {
		this.regType = regType;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}
    
    
}