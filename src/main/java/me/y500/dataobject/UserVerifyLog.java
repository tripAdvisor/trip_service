package me.y500.dataobject;

public class UserVerifyLog {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_verify_log.id
     *
     * @mbggenerated Mon Oct 27 20:35:18 CST 2014
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_verify_log.phone
     *
     * @mbggenerated Mon Oct 27 20:35:18 CST 2014
     */
    private Integer uid;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_verify_log.dt
     *
     * @mbggenerated Mon Oct 27 20:35:18 CST 2014
     */
    private String dt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_verify_log.verify_count
     *
     * @mbggenerated Mon Oct 27 20:35:18 CST 2014
     */
    private Integer verifyCount;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_verify_log.id
     *
     * @return the value of user_verify_log.id
     *
     * @mbggenerated Mon Oct 27 20:35:18 CST 2014
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_verify_log.id
     *
     * @param id the value for user_verify_log.id
     *
     * @mbggenerated Mon Oct 27 20:35:18 CST 2014
     */
    public void setId(Integer id) {
        this.id = id;
    }

   
    public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	/**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_verify_log.dt
     *
     * @return the value of user_verify_log.dt
     *
     * @mbggenerated Mon Oct 27 20:35:18 CST 2014
     */
    public String getDt() {
        return dt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_verify_log.dt
     *
     * @param dt the value for user_verify_log.dt
     *
     * @mbggenerated Mon Oct 27 20:35:18 CST 2014
     */
    public void setDt(String dt) {
        this.dt = dt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_verify_log.verify_count
     *
     * @return the value of user_verify_log.verify_count
     *
     * @mbggenerated Mon Oct 27 20:35:18 CST 2014
     */
    public Integer getVerifyCount() {
        return verifyCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_verify_log.verify_count
     *
     * @param verifyCount the value for user_verify_log.verify_count
     *
     * @mbggenerated Mon Oct 27 20:35:18 CST 2014
     */
    public void setVerifyCount(Integer verifyCount) {
        this.verifyCount = verifyCount;
    }
}