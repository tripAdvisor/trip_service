package me.y500.dataobject;

public class ImageRequest {
	private String imageLong;
	private String location;
	private String width;
	private String imageName;
	public String getImageLong() {
		return imageLong;
	}
	public void setImageLong(String imageLong) {
		this.imageLong = imageLong;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	
}
