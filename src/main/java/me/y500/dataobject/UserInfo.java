package me.y500.dataobject;

import java.util.Date;

public class UserInfo {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_info.uid
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    private Long uid;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_info.nick
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    private String nick;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_info.about
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    private String about;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_info.image
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    private String image;
    private String location;
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_info.sex
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    private Byte sex;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_info.level
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    private Integer level;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_info.accept
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    private Integer accept;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_info.common_point
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    private Integer commonPoint;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_info.reputation_point
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    private Integer reputationPoint;



    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_info.blacklist
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    private Byte blacklist;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_info.create_time
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_info.update_time
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    private Date updateTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_info.status
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    private Byte status;

   


	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	/**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_info.nick
     *
     * @return the value of user_info.nick
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public String getNick() {
        return nick;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_info.nick
     *
     * @param nick the value for user_info.nick
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public void setNick(String nick) {
        this.nick = nick;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_info.about
     *
     * @return the value of user_info.about
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public String getAbout() {
        return about;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_info.about
     *
     * @param about the value for user_info.about
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public void setAbout(String about) {
        this.about = about;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_info.image
     *
     * @return the value of user_info.image
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public String getImage() {
        return image;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_info.image
     *
     * @param image the value for user_info.image
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public void setImage(String image) {
        this.image = image;
    }

   
    public Byte getSex() {
		return sex;
	}

	public void setSex(Byte sex) {
		this.sex = sex;
	}

	/**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_info.level
     *
     * @return the value of user_info.level
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_info.level
     *
     * @param level the value for user_info.level
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_info.accept
     *
     * @return the value of user_info.accept
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public Integer getAccept() {
        return accept;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_info.accept
     *
     * @param accept the value for user_info.accept
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public void setAccept(Integer accept) {
        this.accept = accept;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_info.common_point
     *
     * @return the value of user_info.common_point
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public Integer getCommonPoint() {
        return commonPoint;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_info.common_point
     *
     * @param commonPoint the value for user_info.common_point
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public void setCommonPoint(Integer commonPoint) {
        this.commonPoint = commonPoint;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_info.reputation_point
     *
     * @return the value of user_info.reputation_point
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public Integer getReputationPoint() {
        return reputationPoint;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_info.reputation_point
     *
     * @param reputationPoint the value for user_info.reputation_point
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public void setReputationPoint(Integer reputationPoint) {
        this.reputationPoint = reputationPoint;
    }

   


    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_info.blacklist
     *
     * @return the value of user_info.blacklist
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public Byte getBlacklist() {
        return blacklist;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_info.blacklist
     *
     * @param blacklist the value for user_info.blacklist
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public void setBlacklist(Byte blacklist) {
        this.blacklist = blacklist;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_info.create_time
     *
     * @return the value of user_info.create_time
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_info.create_time
     *
     * @param createTime the value for user_info.create_time
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_info.update_time
     *
     * @return the value of user_info.update_time
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_info.update_time
     *
     * @param updateTime the value for user_info.update_time
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_info.status
     *
     * @return the value of user_info.status
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public Byte getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_info.status
     *
     * @param status the value for user_info.status
     *
     * @mbggenerated Sun Aug 02 10:32:51 CST 2015
     */
    public void setStatus(Byte status) {
        this.status = status;
    }

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
    
}