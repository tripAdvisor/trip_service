package me.y500.result;

import java.util.List;

public class MyDetailAnswer {
	private SimpleQuestion simpleQuestion;
	private SimpleUser guest;
	private DetailAnswerResult answer;
	public SimpleQuestion getSimpleQuestion() {
		return simpleQuestion;
	}
	public void setSimpleQuestion(SimpleQuestion simpleQuestion) {
		this.simpleQuestion = simpleQuestion;
	}
	public SimpleUser getGuest() {
		return guest;
	}
	public void setGuest(SimpleUser guest) {
		this.guest = guest;
	}
	public DetailAnswerResult getAnswer() {
		return answer;
	}
	public void setAnswer(DetailAnswerResult answer) {
		this.answer = answer;
	}
	
}
