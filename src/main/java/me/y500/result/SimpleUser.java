package me.y500.result;


import org.apache.commons.lang.StringUtils;

import me.y500.dataobject.UserInfo;
import me.y500.util.Constant;


public class SimpleUser {
	private Long uid;
	private String nick;
	private Integer level;
	private String image;
	private String about;
	
	public SimpleUser(UserInfo userInfo) {
		super();
		if (userInfo != null) {
			this.uid = userInfo.getUid();
			this.nick = userInfo.getNick();
			this.level = userInfo.getLevel();
			this.image = StringUtils.isBlank(userInfo.getImage()) ? Constant.IMAGE_ICON_PATH : userInfo.getImage();
			this.about = userInfo.getAbout();
		}
		
	}
	

	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	
	public Long getUid() {
		return uid;
	}


	public void setUid(Long uid) {
		this.uid = uid;
	}


	public Integer getLevel() {
		return level;
	}


	public void setLevel(Integer level) {
		this.level = level;
	}


	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getAbout() {
		return about;
	}
	public void setAbout(String about) {
		this.about = about;
	}
	
}
