package me.y500.result;

import java.util.List;

public class DestinationResult {
	private List<DRLabel> labels;

	public List<DRLabel> getLabels() {
		return labels;
	}

	public void setLabels(List<DRLabel> labels) {
		this.labels = labels;
	}

	
}
