package me.y500.result;

import org.omg.CORBA.PRIVATE_MEMBER;

public class ShowPersonResult{
	private Long uid;
	private String nick;
	private String about;
	private Integer sex;
	private String image;
	private Integer level;
	private Integer accept;
	private Integer commonPoint;
	private Integer reputationPoint;
	private String location;
	
	
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getAbout() {
		return about;
	}
	public void setAbout(String about) {
		this.about = about;
	}
	
	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	public Integer getSex() {
		return sex;
	}
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	public Integer getAccept() {
		return accept;
	}
	public void setAccept(Integer accept) {
		this.accept = accept;
	}
	public Integer getCommonPoint() {
		return commonPoint;
	}
	public void setCommonPoint(Integer commonPoint) {
		this.commonPoint = commonPoint;
	}
	public Integer getReputationPoint() {
		return reputationPoint;
	}
	public void setReputationPoint(Integer reputationPoint) {
		this.reputationPoint = reputationPoint;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
}
