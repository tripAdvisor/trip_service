package me.y500.result;

import java.util.List;

public class DomesticLabel {
	private String lableName;
	private List<DestinationDesc> domestic;
	public String getLableName() {
		return lableName;
	}
	public void setLableName(String lableName) {
		this.lableName = lableName;
	}
	public List<DestinationDesc> getDomestic() {
		return domestic;
	}
	public void setDomestic(List<DestinationDesc> domestic) {
		this.domestic = domestic;
	}
	
}
