package me.y500.result;

import java.util.List;

public class DRLabel {
	private String name;
	private List<DestinationDesc> destination;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<DestinationDesc> getDestination() {
		return destination;
	}
	public void setDestination(List<DestinationDesc> destination) {
		this.destination = destination;
	}
	
	
}
