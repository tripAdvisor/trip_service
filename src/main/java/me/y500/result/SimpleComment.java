package me.y500.result;

import java.text.ParseException;

import me.y500.dataobject.Comment;
import me.y500.util.DateUtil;



public class SimpleComment {
	private String content;
	private String createTime;
	private int isReply;
	private String replyNick;
	
	public SimpleComment(Comment comment, String nick) {
		super();
		this.content = comment.getContent();
		try {
			this.createTime = DateUtil.format(DateUtil.parse(comment.getCreateTime(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (comment.getFid() != null) {
			this.isReply = 1;
		}else {
			this.isReply = 0;
		}
		this.replyNick = nick;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public int getIsReply() {
		return isReply;
	}
	public void setIsReply(int isReply) {
		this.isReply = isReply;
	}
	public String getReplyNick() {
		return replyNick;
	}
	public void setReplyNick(String replyNick) {
		this.replyNick = replyNick;
	}
	
}
