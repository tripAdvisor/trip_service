package me.y500.result;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class DetailAnswerListResult {
	private SimpleQuestion simpleQuestion;
	private SimpleUser guest;
	private List<DetailAnswerResult> answers;

	public String toString() {
		return ToStringBuilder.reflectionToString(DetailAnswerListResult.class);
	}

	public SimpleQuestion getSimpleQuestion() {
		return simpleQuestion;
	}

	public void setSimpleQuestion(SimpleQuestion simpleQuestion) {
		this.simpleQuestion = simpleQuestion;
	}

	
	public SimpleUser getGuest() {
		return guest;
	}

	public void setGuest(SimpleUser guest) {
		this.guest = guest;
	}

	public List<DetailAnswerResult> getAnswers() {
		return answers;
	}

	public void setAnswers(List<DetailAnswerResult> answers) {
		this.answers = answers;
	}
}
