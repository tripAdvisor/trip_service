package me.y500.result;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;

import me.y500.dataobject.Question;
import me.y500.util.DateUtil;




public class SimpleQuestion {
	private Long qid;
	private String content;
	private List<String> labels;
	private List<Image> images;
	private int followNum;
	private int isFollow = 0;
	private int isSamequestion = 0;
	private long answerNum;
	private String createTime;
	
	public SimpleQuestion(Question question, Long uid, Integer follow, boolean isFollow, boolean isSameQuestion, List<Image> images, long answerNum) {
		super();
		this.qid = question.getId();
		this.content = question.getContent();
		List<String> lables  = new ArrayList<String>();
		if (question.getLabel().indexOf("`") > 0) {
			String[] labelStrings = question.getLabel().split("`");
			for (int i = 0; i < labelStrings.length; i++) {
				lables.add(labelStrings[i]);
			}
		}else {
			lables.add(question.getLabel());
		}
		this.labels = lables;
		this.followNum = follow;
		if (isFollow) {
			this.isFollow = 1;
		}
		if (isSameQuestion) {
			this.isSamequestion = 1;
		}
		
		try {
			this.createTime = DateUtil.format(DateUtil.parse(question.getCreateTime(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.images = images;
		this.answerNum = answerNum;
	}

	

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<String> getLabels() {
		return labels;
	}

	public void setLabels(List<String> labels) {
		this.labels = labels;
	}

	public int getFollowNum() {
		return followNum;
	}

	public void setFollowNum(int followNum) {
		this.followNum = followNum;
	}

	

	public int getIsFollow() {
		return isFollow;
	}

	public void setIsFollow(int isFollow) {
		this.isFollow = isFollow;
	}







	public Long getQid() {
		return qid;
	}



	public void setQid(Long qid) {
		this.qid = qid;
	}



	public String getCreateTime() {
		return createTime;
	}



	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}



	public int getIsSamequestion() {
		return isSamequestion;
	}

	public void setIsSamequestion(int isSamequestion) {
		this.isSamequestion = isSamequestion;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}



	public long getAnswerNum() {
		return answerNum;
	}



	public void setAnswerNum(long answerNum) {
		this.answerNum = answerNum;
	}




	
}
