package me.y500.result;

import java.util.List;

public class QuestionListResult {
	private List<QuestionList> questionList;

	public List<QuestionList> getQuestionList() {
		return questionList;
	}

	public void setQuestionList(List<QuestionList> questionList) {
		this.questionList = questionList;
	}
	
}
