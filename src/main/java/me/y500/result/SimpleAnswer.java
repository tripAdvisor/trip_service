package me.y500.result;

import java.text.ParseException;
import java.util.List;

import me.y500.dataobject.Answer;
import me.y500.util.DateUtil;


public class SimpleAnswer {
	private Long aid;
	private String content;
	private String createTime;
	private int isAccept;
	private int favoriteNum;
	private int isFavorite;
	private int agreeNum;
	private int isAgree;
	private List<Image> images;
	private int commentNum;
	
	public SimpleAnswer(Answer answer, Long uid, Integer agree, boolean isAgree, Integer favorite, boolean isFavorite, List<Image> images, Integer commentNum) {
		super();
		this.aid = answer.getId();
		this.content = answer.getContent();
		try {
			this.createTime = DateUtil.format(DateUtil.parse(answer.getCreateTime(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.isAccept = answer.getIsAccept();
		this.favoriteNum = favorite;
		this.agreeNum = agree;

		if (isFavorite) {
			this.isFavorite = 1;
		}
		if (isAgree) {
			this.isAgree = 1;
		}
		this.images = images;
		this.commentNum = commentNum;
	}

	
	public Long getAid() {
		return aid;
	}


	public void setAid(Long aid) {
		this.aid = aid;
	}


	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public int getIsAccept() {
		return isAccept;
	}

	public void setIsAccept(int isAccept) {
		this.isAccept = isAccept;
	}

	public int getFavoriteNum() {
		return favoriteNum;
	}

	public void setFavoriteNum(int favoriteNum) {
		this.favoriteNum = favoriteNum;
	}

	public int getIsFavorite() {
		return isFavorite;
	}

	public void setIsFavorite(int isFavorite) {
		this.isFavorite = isFavorite;
	}

	public int getAgreeNum() {
		return agreeNum;
	}

	public void setAgreeNum(int agreeNum) {
		this.agreeNum = agreeNum;
	}

	public int getIsAgree() {
		return isAgree;
	}

	public void setIsAgree(int isAgree) {
		this.isAgree = isAgree;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}


	public int getCommentNum() {
		return commentNum;
	}


	public void setCommentNum(int commentNum) {
		this.commentNum = commentNum;
	}

	
}
