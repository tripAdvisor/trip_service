package me.y500.result;

import java.util.List;

public class ForeignLabel {
	private String lableName;
	private List<DestinationDesc> foreign;
	public String getLableName() {
		return lableName;
	}
	public void setLableName(String lableName) {
		this.lableName = lableName;
	}
	public List<DestinationDesc> getForeign() {
		return foreign;
	}
	public void setForeign(List<DestinationDesc> foreign) {
		this.foreign = foreign;
	}
	
}
