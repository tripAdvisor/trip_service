package me.y500.result;

import java.util.List;

public class MyDetailAnswerResult {
	private List<MyDetailAnswer> myDetailAnswers;

	public List<MyDetailAnswer> getMyDetailAnswers() {
		return myDetailAnswers;
	}

	public void setMyDetailAnswers(List<MyDetailAnswer> myDetailAnswers) {
		this.myDetailAnswers = myDetailAnswers;
	}

	
}
