package me.y500.result;

public class resultDesc {
	private String type; //1、问题 2、答案 3、评论
	private Long qid;
	private String content;
	private Integer AnswerNum;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getQid() {
		return qid;
	}
	public void setQid(Long qid) {
		this.qid = qid;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Integer getAnswerNum() {
		return AnswerNum;
	}
	public void setAnswerNum(Integer answerNum) {
		AnswerNum = answerNum;
	}
	
}
