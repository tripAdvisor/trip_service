package me.y500.result;

import java.util.List;

public class SearchResult {
	private List<resultDesc> resultDesc;
	private Integer num;
	private Integer foodId; 
	
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public List<resultDesc> getResultDesc() {
		return resultDesc;
	}
	public void setResultDesc(List<resultDesc> resultDesc) {
		this.resultDesc = resultDesc;
	}
	public Integer getFoodId() {
		return foodId;
	}
	public void setFoodId(Integer foodId) {
		this.foodId = foodId;
	}
	
}
