package me.y500.result;

import java.util.List;

public class AnswerDetailResult {
	private SimpleQuestion question;
	private SimpleUser guest;
	private SimpleAnswer answer;
	private List<CommentList> comments;
	public SimpleUser getGuest() {
		return guest;
	}
	public void setGuest(SimpleUser guest) {
		this.guest = guest;
	}
	public SimpleAnswer getAnswer() {
		return answer;
	}
	public void setAnswer(SimpleAnswer answer) {
		this.answer = answer;
	}
	public List<CommentList> getComments() {
		return comments;
	}
	public void setComments(List<CommentList> comments) {
		this.comments = comments;
	}
	public SimpleQuestion getQuestion() {
		return question;
	}
	public void setQuestion(SimpleQuestion question) {
		this.question = question;
	}
	
}
