package me.y500.result;

public class CommentList {
	private SimpleUser guest;
	private SimpleComment comment;
	public SimpleUser getGuest() {
		return guest;
	}
	public void setGuest(SimpleUser guest) {
		this.guest = guest;
	}
	public SimpleComment getComment() {
		return comment;
	}
	public void setComment(SimpleComment comment) {
		this.comment = comment;
	}
	
}
