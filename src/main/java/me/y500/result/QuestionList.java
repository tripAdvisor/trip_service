package me.y500.result;

public class QuestionList {
	private SimpleUser guest;
	private SimpleQuestion simpleQuestion;
	public SimpleUser getGuest() {
		return guest;
	}
	public void setGuest(SimpleUser guest) {
		this.guest = guest;
	}
	public SimpleQuestion getSimpleQuestion() {
		return simpleQuestion;
	}
	public void setSimpleQuestion(SimpleQuestion simpleQuestion) {
		this.simpleQuestion = simpleQuestion;
	}
}
