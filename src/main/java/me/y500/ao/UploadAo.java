package me.y500.ao;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import me.y500.dataobject.Answer;
import me.y500.dataobject.Comment;
import me.y500.dataobject.Image;
import me.y500.dataobject.ImageRequest;
import me.y500.dataobject.Question;
import me.y500.dataobject.UserInfo;
import me.y500.mapper.AnswerMapper;
import me.y500.mapper.CommentMapper;
import me.y500.mapper.ImageMapper;
import me.y500.mapper.QuestionMapper;
import me.y500.param.AnswerParams;
import me.y500.param.ReplyParams;
import me.y500.param.SubmitParams;
import me.y500.result.BaseResult;
import me.y500.util.Constant;
import me.y500.util.DateUtil;
import me.y500.util.Util;

import org.apache.commons.lang.ObjectUtils.Null;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

public class UploadAo {
	private final static Logger logger = Logger.getLogger(UploadAo.class);
	@Autowired
	private QuestionMapper questionMapper;
	@Autowired
	private ImageMapper imageMapper;
	@Autowired
	private CommentMapper commentMapper;
	@Autowired
	private AnswerMapper answerMapper;
//	@Autowired
//	private QuestionRepository questionRepository;
//	@Autowired
//	private AnswerRepository answerRepository;
//	@Autowired
//	private CommentRepository commentRepository;
	/**
	 * 提交问题
	 * 
	 * @param params
	 * @param userInfo
	 * @return
	 */
	public BaseResult submitQuestion(HttpServletRequest request, SubmitParams params, UserInfo userInfo){
		BaseResult result = new BaseResult();
		result.setStat(2);
		result.setMsg("网络问题，请重试~");
		String content = params.getContent();
		String labels = params.getLabels().replace("###", "`");
		String destinationCode = params.getDestination();
		String images = params.getImage();
		if (StringUtils.isBlank(content)) {
			result.setStat(2);
			result.setMsg("写点什么呗？");
			return result;
		}
		try {
			List<ImageRequest> imageRequests = imageRequest(images);
			List<String> imageName = new ArrayList<String>();
			Map<String, String> names = this.getImage(request);
			if (imageRequests != null && imageRequests.size() > 0) {
				for (ImageRequest imageRequest : imageRequests) {
					Image image = new Image();
					String[] namearray = names.get(imageRequest.getImageName()).split("\\.");
					image.setName(Long.valueOf(namearray[0]));
					image.setFullName(names.get(imageRequest.getImageName()));
					imageName.add(namearray[0]);
					image.setImageLong(Float.valueOf(imageRequest.getImageLong()));
					image.setWidth(Float.valueOf(imageRequest.getWidth()));
					image.setLocation(Integer.valueOf(imageRequest.getLocation()));
					imageMapper.insert(image);
				}
			}
			
			Question record = new Question();
			record.setContent(content);
			record.setLabel(labels);
			record.setStatus(0);
			record.setUid(userInfo.getUid());
			record.setDestinationCode(destinationCode);
			record.setImageName(listChangeArray(imageName));
			questionMapper.insert(record);
			result.setStat(1);
			result.setMsg("success");
			return result;
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("网路有问题，请重试~");
			return result;
		}
	}
	
	public List<String> arrayChangeList(String s){
		if (StringUtils.isBlank(s)) {
			return null;
		}
		List<String> list = new ArrayList<String>();
		if (s.indexOf(",") > -1) {
			String[] arrayString = s.split(",");
			for (int i = 0; i < arrayString.length; i++) {
				list.add(arrayString[i]);
			}
		}else{
			list.add(s);
		}
		return list;
	}
	
	public String listChangeArray(List<String> list){
		String s = "";
		if (list.size() == 1) {
			s = list.get(0);
		}else {
			for (int i = 0; i < list.size(); i++) {
				if (StringUtils.isBlank(s)) {
					s = list.get(i);
				}else {
					s = s + "," + list.get(i);
				}
				
			}
		}
		
		return s;
	}
	
	public List<ImageRequest> imageRequest(String image){
		if (StringUtils.isBlank(image)) {
			return null;
		}
		List<ImageRequest> imageRequests = new ArrayList<ImageRequest>();
		if (StringUtils.isNotBlank(image) && image.indexOf("|") > -1) {
			String[] imageArray = image.split("\\|");
			System.out.println(imageArray[0] + imageArray[1]);
			for (int i = 0; i < imageArray.length; i++) {
				ImageRequest imageRequest = new ImageRequest();
				String imageString = imageArray[i].replace("(", "").replace(")", "");
				String[] imageArr = imageString.split(",");
				imageRequest.setImageName(imageArr[0]);
				imageRequest.setImageLong(imageArr[1]);
				imageRequest.setWidth(imageArr[2]);
				imageRequest.setLocation(imageArr[3]);
				imageRequests.add(imageRequest);
			}
		}else {
			ImageRequest imageRequest = new ImageRequest();
			String imageString = image.replace("(", "").replace(")", "");
			String[] imageArr = imageString.split(",");
			imageRequest.setImageName(imageArr[0]);
			imageRequest.setImageLong(imageArr[1]);
			imageRequest.setWidth(imageArr[2]);
			imageRequest.setLocation(imageArr[3]);
			imageRequests.add(imageRequest);
		}
		return imageRequests;
	}
	

	
	public Map<String, String> getImage(HttpServletRequest request){
		Map<String, String> names = new HashMap<String, String>();
		try {
			//创建一个通用的多部分解析器  
		    CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());  
		    //判断 request 是否有文件上传,即多部分请求 
		    if(multipartResolver.isMultipart(request)){  
		        //转换成多部分request    
		        MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest)request;  
		        //取得request中的所有文件名  
		        Iterator<String> iter = multiRequest.getFileNames(); 
		        while(iter.hasNext()){  
		            //记录上传过程起始时的时间，用来计算上传时间  
		            int pre = (int) System.currentTimeMillis();  
		            //取得上传文件  
		            MultipartFile file = multiRequest.getFile(iter.next());  
		            if(file != null){  
		                //取得当前上传文件的文件名称  
		                String myFileName = file.getOriginalFilename();  
		                //如果名称不为“”,说明该文件存在，否则说明该文件不存在  
		                if(myFileName.trim() !=""){  
		                    //重命名上传后的文件名  
		                    String oldFileName = file.getOriginalFilename(); 
		                    //自定义文件名
		                    String newFileName = new Date().getTime() + Util.getVerifyCode();
		                    String[] files = oldFileName.split("\\.");
		                    //定义上传路径  
		                    String path =  Constant.UPLOAD_PATH + newFileName + "." + files[files.length -1];  
		                    File localFile = new File(path);  
		                    file.transferTo(localFile); 
		                    names.put(oldFileName, newFileName + "." + files[files.length -1]);
		                    logger.info("upload success=====" + newFileName);  
		                }  
		            }  
		            //记录上传该文件后的时间  
		            int finaltime = (int) System.currentTimeMillis();  
		            logger.info(finaltime - pre);  
		        }  
		          
		    }  
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
		}
		return names;
	}
	
	/**
	 * 答问
	 * 
	 * @param params
	 * @param userInfo
	 * @return
	 */
	public BaseResult answer(HttpServletRequest request, AnswerParams params, UserInfo userInfo){
		BaseResult result = new BaseResult();
		result.setStat(2);
		result.setMsg("网络问题，请重试~");
		String content = params.getContent();
		Long qid = params.getQid();
		String images = params.getImage();
		
		try {
			
			List<ImageRequest> imageRequests = imageRequest(images);
			List<String> imageName = new ArrayList<String>();
			Map<String, String> names = this.getImage(request);
			if (imageRequests != null && imageRequests.size() > 0) {
				for (ImageRequest imageRequest : imageRequests) {
					Image image = new Image();
					String[] namearray = names.get(imageRequest.getImageName()).split("\\.");
					image.setName(Long.valueOf(namearray[0]));
					image.setFullName(names.get(imageRequest.getImageName()));
					imageName.add(namearray[0]);
					image.setImageLong(Float.valueOf(imageRequest.getImageLong()));
					image.setWidth(Float.valueOf(imageRequest.getWidth()));
					image.setLocation(Integer.valueOf(imageRequest.getLocation()));
					imageMapper.insert(image);
				}
			}
			//插入回答表
			Answer answer = new Answer();
			answer.setContent(content);
			answer.setQid(qid);
			answer.setUid(userInfo.getUid());
			answer.setImageName(listChangeArray(imageName));
			answerMapper.insert(answer);
			result.setStat(1);
			result.setMsg("success");
			return result;
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("网络有问题，请重试~");
			return result;
		}
	}
	
	/**
	 * 回复
	 * 
	 * @param params
	 * @param userInfo
	 * @return
	 */
	public BaseResult reply(ReplyParams params, UserInfo userInfo){
		BaseResult result = new BaseResult();
		result.setStat(2);
		result.setMsg("网络问题，请重试~");
		String content = params.getContent();
		Long aid = params.getAid();
		Long fid = params.getFid();
		
		try {
				//插入评论表
			Comment comment = new Comment();
			comment.setAid(aid);
			comment.setContent(content);
			comment.setFid(fid);
			comment.setUid(userInfo.getUid());
			commentMapper.insert(comment);
			result.setStat(1);
			result.setMsg("success");
			return result;
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("网络有问题，请重试~");
			return result;
		}
	}
	public static void main(String[] args) {
		UploadAo ao = new UploadAo();
		String s = new Date().getTime() + Util.getVerifyCode();
		System.out.println(s);
		System.out.println(s.substring(10, s.length()));
	}
	
}



