package me.y500.ao;

import java.io.IOException;
import java.net.URLDecoder;
import java.security.Key;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.sound.midi.MidiDevice.Info;

import me.y500.bo.SecretBo;
import me.y500.bo.UploadBo;
import me.y500.bo.UserBo;
import me.y500.dataobject.Answer;
import me.y500.dataobject.Favorite;
import me.y500.dataobject.Question;
import me.y500.dataobject.UserAuth;
import me.y500.dataobject.UserInfo;
import me.y500.dataobject.UserVerifyInfo;
import me.y500.dataobject.WxUserInfo;
import me.y500.mapper.AnswerMapper;
import me.y500.mapper.FavoriteMapper;
import me.y500.mapper.FollowMapper;
import me.y500.mapper.QuestionMapper;
import me.y500.mapper.UserAuthMapper;
import me.y500.mapper.UserInfoMapper;
import me.y500.mapper.UserVerifyInfoMapper;
import me.y500.mapper.WxUserInfoMapper;
import me.y500.param.AuthPhoneParams;
import me.y500.param.BaseParams;
import me.y500.param.PhoneAuthParams;
import me.y500.param.UserInfoSettingParams;
import me.y500.result.AuthResult;
import me.y500.result.BaseResult;
import me.y500.result.DetailAnswerListResult;
import me.y500.result.DetailAnswerResult;
import me.y500.result.MyDetailAnswer;
import me.y500.result.MyDetailAnswerResult;
import me.y500.result.PhoneSmsResult;
import me.y500.result.ShowPersonResult;
import me.y500.result.SimpleAnswer;
import me.y500.result.SimpleQuestion;
import me.y500.result.SimpleUser;
import me.y500.service.SMSService;
import me.y500.util.DateUtil;
import me.y500.util.Util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;





public class UserAo {
	private final static Logger logger = Logger.getLogger(UserAo.class);
	@Autowired
	private SecretBo secretBo;
	@Autowired
	private UserAuthMapper userAuthMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
//	@Autowired
//	private UserRepository userRepository;
	@Autowired
	private UserBo userBo;
	@Autowired
	private UserVerifyInfoMapper userVerifyInfoMapper;
	@Autowired
	private FavoriteMapper favoriteMapper;
	@Autowired
	private FollowMapper followMapper;
	@Autowired
	private AnswerMapper answerMapper;
	@Autowired
	private QuestionMapper questionMapper;
	@Autowired
	private UploadBo uploadBo;
	@Autowired
	private UploadAo uploadAo;
//	@Autowired
//	private AnswerRepository answerRepository;
	/**
	 * 用户注册认证协议
	 *
	 * @return
	 */
	public BaseResult userAuth(UserInfo userLoginInfo, BaseParams params) {
		String appname = params.getP().getAppname();
		String imei = params.getP().getImei();
		String mac = params.getP().getMac();
		String os = params.getP().getOs();
		String platform = params.getP().getPlatform().toLowerCase();
		String ptype = params.getP().getPtype();
		String source = params.getP().getSource();
		String version = params.getP().getVersion();
		String deviceId = params.getP().getDeviceId();
		String webId = params.getP().getWebId();
		String adId = params.getP().getAdId();
		//iphone使用广告id作为唯一字段
		String deviceToken = secretBo.parseHexStr2Byte(imei, new Date().getTime() + Util.getVerifyCode());
		AuthResult authResult = new AuthResult();
		BaseResult result = new BaseResult();
		try {
			UserAuth authQuery = new UserAuth();
			authQuery.setAppname(appname);
			authQuery.setImei(imei);
			List<UserAuth> userAuths = userAuthMapper.queryByData(authQuery);
			
			Long uid = 0l;
			if (userAuths != null && userAuths.size() > 0) {
				deviceToken = userAuths.get(0).getDeviceToken();
				uid = userAuths.get(0).getUid();
			}else {
				UserAuth query = new UserAuth();
				query.setImei(imei);
				query.setAppname(appname);
				query.setDeviceToken(deviceToken);
				query.setPlatform(platform);
				query.setSource(source);
				userAuthMapper.insert(query);		
			}
	
			authResult.setToken("device " + deviceToken);
			authResult.setUid(uid);
			result.setStat(1);
			result.setMsg("注册成功！");
			result.setResult(authResult);
				
		} catch (Exception e) {
			result.setStat(2);
			result.setMsg("注册失败！");
			logger.info(Util.exceptionMsg(e));
		}
		return result;
	}
	
//	//记录用户渠道变更情况
//	public void userLog(int uid, String source){
//		try {
//			UserSourceHistory query = new UserSourceHistory();
//			query.setUid(uid);
//			UserSourceHistory userSourceHistory = userSourceHistoryMapper.select(query);
//			if (userSourceHistory != null && !userSourceHistory.getSource().equalsIgnoreCase(source)) {
//				query.setSource(source);
//				userSourceHistoryMapper.insert(query);
//			}else if(userSourceHistory == null){
//				query.setSource(source);
//				userSourceHistoryMapper.insert(query);
//			}
//		} catch (Exception e) {
//			logger.info(Util.exceptionMsg(e));
//		}
//		
//	}
	
//	//记录用户唯一设备变更情况
//	public void userDevice(String imei, String deviceId){
//		try {
//			UserDeviceInfo query = new UserDeviceInfo();
//			query.setImei(imei);
//			query.setImeiAndroid(deviceId);
//			UserDeviceInfo userDeviceInfo =  userDeviceInfoMapper.select(query);
//			if (userDeviceInfo != null) {
//				userDeviceInfoMapper.update(query);
//			}else {
//				userDeviceInfoMapper.insertAndroid(query);
//			}
//		} catch (Exception e) {
//			logger.info(Util.exceptionMsg(e));
//		}
//		
//	}
	
	
	/**
	 * 用户手机号绑定协议
	 *
	 * @return
	 */
	public BaseResult authPhone(UserInfo user, WxUserInfo wxUser, AuthPhoneParams params) {
		logger.info("authPhone=" + user + " " + wxUser);
		String phone = params.getPhone();
		String name = params.getName();
		String idCard = params.getIdcard();
		String verify = params.getVerify();
		String appname = params.getP().getAppname();
		String imei = params.getP().getImei();
		String mac = params.getP().getMac();
		String os = params.getP().getOs();
		String platform = params.getP().getPlatform();
		String ptype = params.getP().getPtype();
		String source = params.getP().getSource();
		String version = params.getP().getVersion();
		
		
		AuthResult authResult = new AuthResult();
		BaseResult result = new BaseResult();
		
		String phoneToken = secretBo.parseHexStr2Byte(phone, new Date().getTime() + Util.getVerifyCode());
		try {
			
			if (!userBo.checkVerifyCode(phone, null, verify, null)) {
				result.setStat(901);
				result.setMsg("验证码不正确");
				return result;
			} 
			UserAuth userAuth = userAuthMapper.getUserByPhone(phone);
			
			//只进行过设备注册的新用户
			if (userAuth == null && wxUser == null) {
				UserAuth query = new UserAuth();
				query.setUid(Long.valueOf(user.getUid()));
				query.setPhone(phone);
				query.setPhoneToken(phoneToken);
				query.setPhoneRegTime(new Date());
				query.setPlatform(platform);
				query.setPhoneBindingStatus((byte)1);
				userAuthMapper.update(query);
				
				
				UserInfo userQuery = new UserInfo();
				userQuery.setUid(user.getUid());
				userQuery.setSex((byte)0);
				userQuery.setLevel(1);
				userQuery.setAccept(0);
				userQuery.setCommonPoint(0);
				userQuery.setReputationPoint(0);
				userQuery.setStatus((byte)1);
				
				userInfoMapper.insert(userQuery);
				
			}else if (userAuth == null && wxUser != null){ //微信已注册，绑定手机号
				UserAuth query = new UserAuth();
				query.setUid(wxUser.getUid());
				query.setPhone(phone);
				query.setPhoneToken(phoneToken);
				query.setPhoneRegTime(new Date());
				query.setPlatform(platform);
				query.setPhoneBindingStatus((byte)1);
				userAuthMapper.update(query);
				
				
			}else if (userAuth != null){
				UserAuth query = new UserAuth();
				query.setUid(userAuth.getUid());
				query.setPlatform(platform);
				query.setPhoneBindingStatus((byte)1);
				userAuthMapper.update(query);
				phoneToken = userAuth.getPhoneToken();
				
			}
			userBo.usertime(phone, verify);
			if (phoneToken.startsWith("phone ")) {
				authResult.setToken(phoneToken);
			}else {
				authResult.setToken("phone " + phoneToken);
			}
			result.setStat(1);
			result.setMsg("注册成功！");
			result.setResult(authResult);
			logger.info("authPhone success: phone=" + phone + ", verify=" + verify + ", phoneToken=" + authResult.getToken());
		} catch (Exception e) {
			result.setStat(2);
			result.setMsg("注册失败！");
			logger.info(Util.exceptionMsg(e));
		}
		return result;
	}

//	
//	/**
//	 * 用户注册认证协议
//	 *
//	 * @return
//	 */
//	public BaseResult authparse(AuthParseParams params) {
//		String auth = params.getAuth();
//		AuthInfoResult authInfoResult = new AuthInfoResult();
//		BaseResult result = new BaseResult();
//		String[] authArray = {};
//		UserAuth userAuth = null;
//		result.setStat(14);
//		result.setMsg("fail");
//		try {
//			if (auth != null && auth.length() > 0 && auth.startsWith("device ")) {
//				auth = auth.substring(7);
//				userAuth = userAuthMapper.getUserByDeviceToken(auth);
//				if (userAuth == null) {
//					result.setStat(14);
//					result.setMsg("服务器出了点小问题，客官请重试一下下！");
//				}
//			}else if (auth != null && auth.length() > 0 && auth.startsWith("phone ")) {
//				auth = auth.substring(6);
//				userAuth = userAuthMapper.getUserByPhoneToken(auth);
//				if (userAuth == null) {
//					result.setStat(13);
//					result.setMsg("服务器出了点小问题，客官请重试一下下！");
//				}
//			}else if (auth != null && auth.length() > 0 && auth.startsWith("wx ")) {
//				auth = auth.substring(3);
//				userAuth = userAuthMapper.getUserByWxToken(auth);
//			}
//			
//			
//			if (userAuth != null) {
//				UserLoginInfo query = new UserLoginInfo();
//				UserLoginInfo user = userLoginInfoMapper.queryByid(userAuth.getUid());
//				if (user != null) {
//					if (StringUtils.isNotBlank(user.getNick()) && user.getNick().startsWith("nick_")) {
//						user.setNick(null);
//					}
//					if (StringUtils.isNotBlank(user.getPhone()) && user.getPhone().startsWith("phone_")) {
//						user.setPhone(null);
//					}
//					if (StringUtils.isNotBlank(user.getIdcard()) && user.getIdcard().startsWith("idcard_")) {
//						user.setIdcard(null);
//					}
//					
//					result.setResult(this.userToAuth(user));
//					result.setStat(1);
//					result.setMsg("success");
//				}
//			}
//		} catch (Exception e) {
//			logger.info(Util.exceptionMsg(e));
//			WxControl.Control(Util.exceptionMsg(e));
//		}
//		return result;
//	}
//	
//	public AuthInfoResult userToAuth(UserLoginInfo user){
//		AuthInfoResult userAuth = new AuthInfoResult();
//		userAuth.setUid(user.getUid());
//		userAuth.setPhone(user.getPhone());
//		userAuth.setName(user.getNick());
//		userAuth.setIdcard(user.getIdcard());
//		userAuth.setImei(user.getImei());
//		userAuth.setPushToken(user.getPushToken());
//		userAuth.setPlatform(user.getPlatform());
//		userAuth.setSource(user.getSource());
//		userAuth.setRegTime(user.getRegTime());
//		userAuth.setPtype(user.getPtype());
//		return userAuth;
//	}
//	
	/**
	 * 向手机发送校验码
	 * 
	 * @param phone
	 * @param isVerify
	 *            用于标示是否判断手机号注册过
	 * @return
	 */
	public BaseResult phoneauth(Long uid, PhoneAuthParams params) {
		BaseResult result = new BaseResult();
		PhoneSmsResult smsResult = new PhoneSmsResult();
		String phone = params.getPhone();
//		String validateCode = params.getValidateCode();
		result.setStat(2);
		result.setMsg("fail");
	
		if (!Util.phoneNumberCheck(phone)) {
			result.setMsg("亲，你输入的电话号码不对～");
			return result;
		}

//		if (!userBo.limitCount(uid.intValue())) {
//			//发送短信超限，判断有没有图形验证码，有则验证，然后发送短信，
//			if (StringUtils.isNotBlank(validateCode)) {
//				if (!userBo.checkVerifyCode(null, uid.intValue(), validateCode, 4)) {
//					result.setStat(2);
//					result.setMsg("验证码错误");
//					return result;
//				}else {
//					userBo.usertimebyuid(uid.intValue(), validateCode);
//					smsResult.setValidateCode(0);
//				}
//			}else {
//				result.setStat(1);
//				result.setMsg("success");
//				smsResult.setValidateCode(1);
//				result.setResult(smsResult);
//				return result;
//			}
//		}else {
//			smsResult.setValidateCode(0);
//		}
//		result.setResult(smsResult);

		// 5分钟只能发3条
		int sendCount1 = userBo.getSendCount(phone, -5);
		// 1分钟只能发一条
		int sendCount2 = userBo.getSendCount(phone, -1);
		if (sendCount1 >= 3 || sendCount2 >= 1) {
			result.setStat(2);
			result.setMsg("您的请求太频繁啦，人家受不袅了…");
			return result;
		}

		// 加入到临时表中
		UserVerifyInfo uvi = new UserVerifyInfo();
		uvi.setLoginname(phone);
		uvi.setUid(uid.intValue());
		uvi.setVerifyCode(Util.getVerifyCode());
		Date curTime = new Date();
		uvi.setRegTime(curTime);
		uvi.setLostTime(DateUtil.addMinute(curTime, 30));
		uvi.setStatus(1);
		try {
			result.setStat(2);
			
			UserVerifyInfo query = new UserVerifyInfo();
			query.setLoginname(phone);
			query.setRegTime(DateUtil.parse(DateUtil.getToday() + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
			int count = userVerifyInfoMapper.getVerifyInfoCount(query);
			if (!"".equals(SMSService.SendSMS(phone,
					String.format("验证码:%s。", uvi.getVerifyCode()), count))) {
				userVerifyInfoMapper.insert(uvi);
				result.setStat(1);
				result.setMsg("校验通过，已经发送了验证码");
			} else {
				result.setStat(1);
				result.setMsg("校验通过，已经发送了验证码");
			}
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
			result.setMsg("短信发送失败！");
		}
		return result;
	}
	
//	
//	/**
//	 * 返回客户端图形验证码
//	 * 
//	 * @param uid
//	 * @return
//	 */
//	public BaseResult alidateCode(Long uid) {
//		BaseResult result = new BaseResult();
//		result.setStat(2);
//        result.setMsg("fail");
//		AlidateCodeResult aliResult = new AlidateCodeResult();
//		ValidateCode vCode = new ValidateCode(120,40,4,100);
//		try {  
//			long codedate = new Date().getTime();
//            String path="/usr/local/nginx/html/innmall/alidatecode/"+ codedate +".png";  
//            logger.info("alidateCode:uid=" + uid +  " code="+ vCode.getCode()+" > "+path);  
//            vCode.write(path);  
//            aliResult.setCodeUrl("http://h.innmall.cn/alidatecode/" + codedate + ".png");
//            
//            UserVerifyInfo uvi = new UserVerifyInfo();
//    		uvi.setUid(uid.intValue());
//    		uvi.setVerifyCode(vCode.getCode().toLowerCase());
//    		Date curTime = new Date();
//    		uvi.setRegTime(curTime);
//    		uvi.setRegType(4);
//    		uvi.setLostTime(DateUtil.addMinute(curTime, 30));
//    		uvi.setStatus(1);
//    		userVerifyInfoMapper.insert(uvi);
//            
//            
//            result.setStat(1);
//            result.setMsg("success");
//            result.setResult(aliResult);
//        } catch (IOException e) {  
//        	logger.info(Util.exceptionMsg(e));
//			WxControl.Control(Util.exceptionMsg(e));
//            result.setStat(2);
//            result.setMsg("fail");
//        }
//		return result;
//	}
//	
	public BaseResult setting(UserInfo user, UserInfoSettingParams params){
		String nick = params.getNick();
		Integer sex = params.getSex();
		String about = params.getAbout();
		String location = params.getLocation();
		BaseResult result = new BaseResult();
		result.setStat(2);
		try {
			if (StringUtils.isNotBlank(nick) && !Util.checkNick(nick)) {
				result.setMsg("昵称为字母，数字或者中文\r\n请检查输入是否正确!");
				return result;
			}
			if (StringUtils.isNotBlank(about) && about.length() > 30) {
				result.setMsg("亲，简介不要超过30个字噢！");
				return result;
			}
			UserInfo userQuery = new UserInfo();
			userQuery.setUid(user.getUid());
			userQuery.setAbout(about);
			userQuery.setSex(sex != null ? (byte)sex.intValue() : null);
			userQuery.setNick(nick);
			userQuery.setLocation(location);
			userInfoMapper.update(userQuery);
			result.setStat(1);
			result.setMsg("修改成功");
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(Util.exceptionMsg(e));
			result.setStat(1);
			result.setMsg("修改失败！");
		}
		return result;
		
	}

	
	public BaseResult show(UserInfo user){
		BaseResult result = new BaseResult();
		ShowPersonResult showPersonResult = new ShowPersonResult();
		result.setStat(2);
		try {
			if (user != null) {
				showPersonResult.setUid(user.getUid());
				if (StringUtils.isNotBlank(user.getNick())) {
					showPersonResult.setNick(user.getNick());
				}
				if (StringUtils.isNotBlank(user.getAbout())) {
					showPersonResult.setAbout(user.getAbout());
				}
				if (StringUtils.isNotBlank(user.getImage())) {
					showPersonResult.setImage(user.getImage());
				}
				showPersonResult.setSex(user.getSex().intValue());
				showPersonResult.setLevel(user.getLevel());
				showPersonResult.setAccept(user.getAccept());
				showPersonResult.setCommonPoint(user.getCommonPoint());
				showPersonResult.setReputationPoint(user.getReputationPoint());
				showPersonResult.setLocation(user.getLocation());
				result.setResult(showPersonResult);
				result.setStat(1);
				result.setMsg("success");
				return result;
			}
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("获取个人资料失败!");
			return result;
		}
		return result;
		
	}
	
	
	public BaseResult myfavorite(UserInfo userInfo){
		BaseResult result = new BaseResult();
		MyDetailAnswerResult myDetailAnswerResult = new MyDetailAnswerResult(); 
		result.setStat(2);
		try {
			Favorite query = new Favorite();
			query.setUid(userInfo.getUid());
			List<Favorite> favorites = favoriteMapper.select(query);
			if (favorites != null && favorites.size() > 0) {
				List<MyDetailAnswer> myDetailAnswers = new ArrayList<MyDetailAnswer>();
				for (Favorite favorite : favorites) {
					MyDetailAnswer myDetailAnswerListResult = new MyDetailAnswer();
					Answer answer = answerMapper.selectByPrimaryKey(favorite.getAid());
					Long qid = answer.getQid();
					
					Question question = questionMapper.selectByPrimaryKey(qid);
					if (question == null) {
						result.setMsg("提问已删除~");
						return result;
					}
					UserInfo owner = userInfoMapper.selectByPrimaryKey(question.getUid());
					myDetailAnswerListResult.setGuest(new SimpleUser(owner));
					int answerNum = answerMapper.selectByQid(question.getId()).size();
					myDetailAnswerListResult.setSimpleQuestion(new SimpleQuestion(question, userInfo.getUid(), uploadBo.follow(question.getId()), uploadBo.isFollow(question.getId(), userInfo.getUid()), uploadBo.isSameQuestion(question.getId(), userInfo.getUid()), uploadBo.getImage(uploadAo.arrayChangeList(question.getImageName())), answerNum));
					Answer myanswer = answerMapper.selectByPrimaryKey(favorite.getAid());
						UserInfo guest = userInfoMapper.selectByPrimaryKey(myanswer
								.getUid());
						DetailAnswerResult ar = new DetailAnswerResult();
						ar.setAnswer(new SimpleAnswer(myanswer, userInfo.getUid(), uploadBo.agree(myanswer.getId()), uploadBo.isAgree(myanswer.getId(), userInfo.getUid()), uploadBo.favorite(myanswer.getId()), uploadBo.isFavorite(myanswer.getId(), userInfo.getUid()),uploadBo.getImage(uploadAo.arrayChangeList(myanswer.getImageName())), uploadBo.commentNum(answer.getId())));
						ar.setGuest(new SimpleUser(guest));
					myDetailAnswerListResult.setAnswer(ar);
					myDetailAnswers.add(myDetailAnswerListResult);
				}
				myDetailAnswerResult.setMyDetailAnswers(myDetailAnswers);
				result.setStat(1);
				result.setMsg("success");
				result.setResult(myDetailAnswerResult);
				return result;
			}else {
				result.setStat(2);
				result.setMsg("没有收藏任何答案~");
				result.setResult(myDetailAnswerResult);
				return result;
			}
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("获取我的收藏失败!");
			return result;
		}
	}
	
	
	public BaseResult myanswer(UserInfo userInfo){
		BaseResult result = new BaseResult();
		MyDetailAnswerResult myDetailAnswerResult = new MyDetailAnswerResult(); 
		result.setStat(2);
		try {
			List<Answer> answers = answerMapper.selectByUid(userInfo.getUid());
			if (answers != null && answers.size() > 0) {
				List<MyDetailAnswer> myDetailAnswers = new ArrayList<MyDetailAnswer>();
				for (Answer answer : answers) {
					MyDetailAnswer myDetailAnswerListResult = new MyDetailAnswer();
					Long qid = answer.getQid();
					
					Question question = questionMapper.selectByPrimaryKey(qid);
					if (question == null) {
						result.setMsg("提问已删除~");
						return result;
					}
					UserInfo owner = userInfoMapper.selectByPrimaryKey(question.getUid());
					myDetailAnswerListResult.setGuest(new SimpleUser(owner));
					int answerNum = answerMapper.selectByQid(question.getId()).size();
					myDetailAnswerListResult.setSimpleQuestion(new SimpleQuestion(question, userInfo.getUid(), uploadBo.follow(question.getId()), uploadBo.isFollow(question.getId(), userInfo.getUid()), uploadBo.isSameQuestion(question.getId(), userInfo.getUid()), uploadBo.getImage(uploadAo.arrayChangeList(question.getImageName())), answerNum));
					Answer myanswer = answerMapper.selectByPrimaryKey(answer.getId());
						UserInfo guest = userInfoMapper.selectByPrimaryKey(myanswer
								.getUid());
						DetailAnswerResult ar = new DetailAnswerResult();
						ar.setAnswer(new SimpleAnswer(myanswer, userInfo.getUid(), uploadBo.agree(myanswer.getId()), uploadBo.isAgree(myanswer.getId(), userInfo.getUid()), uploadBo.favorite(myanswer.getId()), uploadBo.isFavorite(myanswer.getId(), userInfo.getUid()),uploadBo.getImage(uploadAo.arrayChangeList(myanswer.getImageName())), uploadBo.commentNum(answer.getId())));
						ar.setGuest(new SimpleUser(guest));
					myDetailAnswerListResult.setAnswer(ar);
					myDetailAnswers.add(myDetailAnswerListResult);
				}
				myDetailAnswerResult.setMyDetailAnswers(myDetailAnswers);
				result.setStat(1);
				result.setMsg("success");
				result.setResult(myDetailAnswerResult);
				return result;
			}else {
				result.setStat(2);
				result.setMsg("没有回答任何问题~");
				result.setResult(myDetailAnswerResult);
				return result;
			}
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("获取我的回答失败!");
			return result;
		}
	}
	
	
	public BaseResult myquestion(UserInfo userInfo){
		BaseResult result = new BaseResult();
		MyDetailAnswerResult myDetailAnswerResult = new MyDetailAnswerResult(); 
		result.setStat(2);
		try {
			List<Question> questions = questionMapper.selectByUid(userInfo.getUid());
			if (questions != null && questions.size() > 0) {
				List<MyDetailAnswer> myDetailAnswers = new ArrayList<MyDetailAnswer>();
				for (Question question : questions) {
					MyDetailAnswer myDetailAnswer = new MyDetailAnswer();
					if (question == null) {
						result.setMsg("提问已删除~");
						return result;
					}
					UserInfo owner = userInfoMapper.selectByPrimaryKey(question.getUid());
					myDetailAnswer.setGuest(new SimpleUser(owner));
					int answerNum = answerMapper.selectByQid(question.getId()).size();
					myDetailAnswer.setSimpleQuestion(new SimpleQuestion(question, userInfo.getUid(), uploadBo.follow(question.getId()), uploadBo.isFollow(question.getId(), userInfo.getUid()), uploadBo.isSameQuestion(question.getId(), userInfo.getUid()), uploadBo.getImage(uploadAo.arrayChangeList(question.getImageName())), answerNum));
					myDetailAnswers.add(myDetailAnswer);
				}
				myDetailAnswerResult.setMyDetailAnswers(myDetailAnswers);
				result.setStat(1);
				result.setMsg("success");
				result.setResult(myDetailAnswerResult);
				return result;
			}else {
				result.setStat(2);
				result.setMsg("没有提出任何问题~");
				result.setResult(myDetailAnswerResult);
				return result;
			}
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("获取我的问题失败!");
			return result;
		}
	}
	
	
	public BaseResult myfollow(UserInfo userInfo){
		BaseResult result = new BaseResult();
		MyDetailAnswerResult myDetailAnswerResult = new MyDetailAnswerResult(); 
		result.setStat(2);
		try {
			List<Question> questions = questionMapper.selectByUid(userInfo.getUid());
			if (questions != null && questions.size() > 0) {
				List<MyDetailAnswer> myDetailAnswers = new ArrayList<MyDetailAnswer>();
				for (Question question : questions) {
					MyDetailAnswer myDetailAnswer = new MyDetailAnswer();
					if (question == null) {
						result.setMsg("提问已删除~");
						return result;
					}
					UserInfo owner = userInfoMapper.selectByPrimaryKey(question.getUid());
					myDetailAnswer.setGuest(new SimpleUser(owner));
					int answerNum = answerMapper.selectByQid(question.getId()).size();
					myDetailAnswer.setSimpleQuestion(new SimpleQuestion(question, userInfo.getUid(), uploadBo.follow(question.getId()), uploadBo.isFollow(question.getId(), userInfo.getUid()), uploadBo.isSameQuestion(question.getId(), userInfo.getUid()), uploadBo.getImage(uploadAo.arrayChangeList(question.getImageName())), answerNum));
					myDetailAnswers.add(myDetailAnswer);
				}
				myDetailAnswerResult.setMyDetailAnswers(myDetailAnswers);
				result.setStat(1);
				result.setMsg("success");
				result.setResult(myDetailAnswerResult);
				return result;
			}else {
				result.setStat(2);
				result.setMsg("没有关注任何问题~");
				result.setResult(myDetailAnswerResult);
				return result;
			}
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("获取我的关注失败!");
			return result;
		}
	}
//	public BaseResult pushToken(Long uid, PushTokenParams params){
//		String pushToken = params.getPushToken();
//		BaseResult result = new BaseResult();
//		result.setStat(2);
//		try {
//			UserLoginInfo query = new UserLoginInfo();
//			query.setUid(uid.intValue());
//			query.setPushToken(pushToken);
//			userLoginInfoMapper.updateAuthPhone(query);
//			result.setStat(1);
//			result.setMsg("success");
//			
//		} catch (Exception e) {
//			// TODO: handle exception
//			logger.info(Util.exceptionMsg(e));
//			WxControl.Control(Util.exceptionMsg(e));
//		}
//		return result;
//		
//	}
//	
//	
	public BaseResult logout(Long uid, BaseParams params){
		BaseResult result = new BaseResult();
		result.setStat(2);
		try {
			userAuthMapper.logout(uid.intValue());
			deviceAuth(params, uid.intValue());
			result.setStat(1);
			result.setMsg("success");
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(Util.exceptionMsg(e));
		}
		return result;
		
	}
	
	
	/**
	 * 设备注册认证方法，提供给注销时使用
	 *
	 * @return
	 */
	public BaseResult deviceAuth(BaseParams params, Integer parentUid) {
		String appname = params.getP().getAppname();
		String imei = params.getP().getImei();
		String mac = params.getP().getMac();
		String os = params.getP().getOs();
		String platform = params.getP().getPlatform().toLowerCase();
		String ptype = params.getP().getPtype();
		String source = params.getP().getSource();
		String version = params.getP().getVersion();
		String deviceId = params.getP().getDeviceId();
		String webId = params.getP().getWebId();
		String adId = params.getP().getAdId();
		//iphone使用广告id作为唯一字段
		String deviceToken = secretBo.parseHexStr2Byte(imei, new Date().getTime() + Util.getVerifyCode());
		String random = new Date().getTime() + Util.getVerifyCode();
		AuthResult authResult = new AuthResult();
		BaseResult result = new BaseResult();
		try {
			UserAuth authQuery = new UserAuth();
			authQuery.setAppname(appname);
			authQuery.setImei(imei);
			List<UserAuth> userAuths = userAuthMapper.queryByData(authQuery);
			
			Long uid = 0l;
			if (userAuths != null && userAuths.size() > 0) {
				deviceToken = userAuths.get(0).getDeviceToken();
				uid = userAuths.get(0).getUid();
			}else {
				UserAuth query = new UserAuth();
				query.setImei(imei);
				query.setAppname(appname);
				query.setDeviceToken(deviceToken);
				query.setPlatform(platform);
				query.setSource(source);
				userAuthMapper.insert(query);		
			}
	
			authResult.setToken("device " + deviceToken);
			authResult.setUid(uid);
			result.setStat(1);
			result.setMsg("注册成功！");
			result.setResult(authResult);
				
		} catch (Exception e) {
			result.setStat(2);
			result.setMsg("注册失败！");
			logger.info(Util.exceptionMsg(e));
		}
		return result;
	}
	
//	
//	
//	/**
//	 * 用户微信绑定协议
//	 * @return
//	 */
//	public BaseResult authWx(UserLoginInfo user, AuthWx params) {
//		String code = params.getCode();
//		String appname = params.getP().getAppname();
//		String imei = params.getP().getImei();
//		String mac = params.getP().getMac();
//		String os = params.getP().getOs();
//		String platform = params.getP().getPlatform();
//		String ptype = params.getP().getPtype();
//		String source = params.getP().getSource();
//		String version = params.getP().getVersion();
//		if (platform.equalsIgnoreCase("iphone")) {
//			imei = mac;
//		}
//		
//		AuthResult authResult = new AuthResult();
//		BaseResult result = new BaseResult();
//		result.setStat(2);
//		result.setMsg("微信登录失败");
//		WxAccessTokenModel token = WxAccessTokenService.getWxAccessTokenByCode(code, version);
//		if (token == null) {
//			return result;
//		}
//		
//		WxUserModel userModel = null;
//		if(token.getErrcode() > 0){
//			return result;
//		}else{
//			//拿到用户的相关信息
//			userModel = WxUserModelService.getWxUserModel(token.getAccess_token(), token.getOpenid());
//			if(userModel == null){
//				return result;
//			}
//			logger.info("wxUser=" + userModel.getOriginaStr());
//			userModel.setNickname(Util.cleanText(userModel.getNickname()));
//			userModel.setOriginaStr(Util.cleanText(userModel.getOriginaStr()));
//		}		
//		
//	  
//		String wxId = userModel.getOpenid();
//		String nickname = userModel.getNickname();
//		int sex = userModel.getSex();
//		String province = userModel.getProvince();
//		String city = userModel.getCity();
//		String country = userModel.getCountry();
//		String privilege = userModel.getPrivilege();
//		String headimgurl = userModel.getHeadimgurl();
//		String unionid = userModel.getUnionid();
//		String originastr = userModel.getOriginaStr();
//		String wxToken = secretBo.parseHexStr2Byte(wxId, new Date().getTime() + Util.getVerifyCode());
//		try {
//			UserAuth userAuth = userAuthMapper.getUserByWxId(wxId);
//			if (userAuth == null) {
//				Date wxRegTime = null;
//				WxUserInfo wxUserInfo = wxUserInfoMapper.selectByOpenid(wxId);
//				if (wxUserInfo == null) {
//					WxUserInfo wxQuery = new WxUserInfo();
//					wxQuery.setOpenid(wxId);
//					wxQuery.setNickname(nickname);
//					wxQuery.setSex((byte)sex);
//					wxQuery.setProvince(province);
//					wxQuery.setCity(city);
//					wxQuery.setCountry(country);
//					wxQuery.setPrivilege(privilege);
//					wxQuery.setHeadimgurl(headimgurl);
//					wxQuery.setUnionid(unionid);
//					wxQuery.setOriginastr(originastr);
//					wxUserInfoMapper.insert(wxQuery);
//				}else {
//					if (wxUserInfo.getOpenid().startsWith("oi2r")) {
//						WxUserInfo wxquery = new WxUserInfo();
//						wxquery.setOpenid(wxId);
//						wxquery.setId(wxUserInfo.getId());
//						wxUserInfoMapper.updateById(wxquery);
//					}
//					wxRegTime = wxUserInfo.getCreatetime();
//				}
//				
//				
//				UserAuth query = new UserAuth();
//				query.setUid(user.getUid());
//				query.setWxId(wxId);
//				query.setWxToken(wxToken);
//				if (wxRegTime == null) {
//					query.setWxRegTime(new Date());
//				}else {
//					query.setWxRegTime(wxRegTime);
//				}
//				query.setPlatform(platform);
//				query.setWxBindingStatus((byte)1);
//				userAuthMapper.update(query);
//				
//				userLog(user.getUid(), source);
//				
//				UserLoginInfo userQuery = new UserLoginInfo();
//				userQuery.setUid(user.getUid());
//				userQuery.setAppname(appname);
//				userQuery.setPlatform(platform);
//				userQuery.setImei(imei);
//				userQuery.setMac(mac);
//				userQuery.setPtype(ptype);
//				userLoginInfoMapper.updateAuthPhone(userQuery);
//
//			}else if (userAuth != null && StringUtils.isNotBlank(user.getPhone())){
//				if (userAuth.getUid().intValue() != user.getUid().intValue()) {				
//					result.setMsg("微信号已注册！");
//					result.setStat(2);
//					return result;
//				}
//				
//				UserAuth query = new UserAuth();
//				query.setUid(userAuth.getUid());
//				query.setWxId(wxId);
//				query.setWxToken(wxToken);
//				query.setWxRegTime(new Date());
//				query.setPlatform(platform);
//				query.setWxBindingStatus((byte)1);
//				userAuthMapper.update(query);
//				
//				userLog(user.getUid(), source);
//				
//				//更新user_login_info表的用户资料信息
//				UserLoginInfo userQuery = new UserLoginInfo();
//				userQuery.setUid(user.getUid());
//				userQuery.setAppname(appname);
//				userQuery.setPlatform(platform);
//				userQuery.setImei(imei);
//				userQuery.setMac(mac);
//				userQuery.setPtype(ptype);
//				userLoginInfoMapper.updateAuthPhone(userQuery);
//				
//				
//				WxUserInfo wxQuery = new WxUserInfo();
//				wxQuery.setOpenid(wxId);
//				wxQuery.setNickname(nickname);
//				wxQuery.setSex((byte)sex);
//				wxQuery.setProvince(province);
//				wxQuery.setCity(city);
//				wxQuery.setCountry(country);
//				wxQuery.setPrivilege(privilege);
//				wxQuery.setHeadimgurl(headimgurl);
//				wxQuery.setUnionid(unionid);
//				wxQuery.setOriginastr(originastr);
//				wxUserInfoMapper.update(wxQuery);
//					
//			}else if (userAuth != null && StringUtils.isBlank(user.getPhone())){
//				UserAuth query = new UserAuth();
//				query.setUid(userAuth.getUid());
//				query.setPlatform(platform);
//				query.setWxBindingStatus((byte)1);
//				userAuthMapper.update(query);
//				wxToken = userAuth.getWxToken();
//				
//				userLog(userAuth.getUid(), source);
//				
//				//更新user_login_info表的用户资料信息
//				UserLoginInfo userQuery = new UserLoginInfo();
//				userQuery.setUid(userAuth.getUid());
//				userQuery.setAppname(appname);
//				userQuery.setPlatform(platform);
//				userQuery.setImei(imei);
//				userQuery.setMac(mac);
//				userQuery.setPtype(ptype);
//				userLoginInfoMapper.updateAuthPhone(userQuery);
//				
//				WxUserInfo wxUserInfo = wxUserInfoMapper.selectByOpenid(wxId);
//				if (wxUserInfo == null) {
//					WxUserInfo wxQuery = new WxUserInfo();
//					wxQuery.setOpenid(wxId);
//					wxQuery.setNickname(nickname);
//					wxQuery.setSex((byte)sex);
//					wxQuery.setProvince(province);
//					wxQuery.setCity(city);
//					wxQuery.setCountry(country);
//					wxQuery.setPrivilege(privilege);
//					wxQuery.setHeadimgurl(headimgurl);
//					wxQuery.setUnionid(unionid);
//					wxQuery.setOriginastr(originastr);
//					wxUserInfoMapper.insert(wxQuery);
//				}else{
//					WxUserInfo wxQuery = new WxUserInfo();
//					wxQuery.setOpenid(wxId);
//					wxQuery.setNickname(nickname);
//					wxQuery.setSex((byte)sex);
//					wxQuery.setProvince(province);
//					wxQuery.setCity(city);
//					wxQuery.setCountry(country);
//					wxQuery.setPrivilege(privilege);
//					wxQuery.setHeadimgurl(headimgurl);
//					wxQuery.setUnionid(unionid);
//					wxQuery.setOriginastr(originastr);
//					wxUserInfoMapper.update(wxQuery);
//				}
//				
//				this.merge(userAuth.getUid().longValue(), user.getUid().longValue());
//			}
//			if (wxToken.startsWith("wx ")) {
//				authResult.setToken(wxToken);
//			}else {
//				authResult.setToken("wx " + wxToken);
//			}
//			result.setStat(1);
//			result.setMsg("注册成功！");
//			result.setResult(authResult);
//			logger.info("authWx success: wxId=" + wxId + ", phoneToken=" + authResult.getToken());
//		} catch (Exception e) {
//			result.setStat(2);
//			result.setMsg("微信登录失败");
//			logger.info(Util.exceptionMsg(e));
//			WxControl.Control(Util.exceptionMsg(e));
//		}
//		return result;
//	}
//	
//	
//	
//	/**
//	 * 用户解绑微信协议
//	 * @return
//	 */
//	public BaseResult unBindingWx(UserLoginInfo user, WxUserInfo wxUser) {
//		AuthResult authResult = new AuthResult();
//		BaseResult result = new BaseResult();
//		result.setStat(2);
//		result.setMsg("fail");
//		if (wxUser == null) {
//			return result;
//		}
//		
//		try {
//			UserAuth userAuth = userAuthMapper.getUserById(wxUser.getUid());
//			if (userAuth == null) {
//				return result;
//			}
//			
//			userAuthMapper.unBindingWx(wxUser.getUid());
//			authResult.setToken("phone " + userAuth.getPhoneToken());
//			authResult.setUid(wxUser.getUid());
//			result.setStat(1);
//			result.setMsg("解绑成功");
//			result.setResult(authResult);
//		} catch (Exception e) {
//			result.setStat(2);
//			result.setMsg("解绑失败");
//			logger.info(Util.exceptionMsg(e));
//			WxControl.Control(Util.exceptionMsg(e));
//		}
//		return result;
//	}
}
