package me.y500.ao;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import me.y500.bo.UploadBo;
import me.y500.dataobject.Agree;
import me.y500.dataobject.Answer;
import me.y500.dataobject.CityInfo;
import me.y500.dataobject.Comment;
import me.y500.dataobject.Destination;
import me.y500.dataobject.Favorite;
import me.y500.dataobject.Follow;
import me.y500.dataobject.ProvinceInfo;
import me.y500.dataobject.Question;
import me.y500.dataobject.SameQuestion;
import me.y500.dataobject.UserAuth;
import me.y500.dataobject.UserInfo;
import me.y500.mapper.AgreeMapper;
import me.y500.mapper.AnswerMapper;
import me.y500.mapper.CityInfoMapper;
import me.y500.mapper.CommentMapper;
import me.y500.mapper.DestinationMapper;
import me.y500.mapper.FavoriteMapper;
import me.y500.mapper.FollowMapper;
import me.y500.mapper.ProvinceInfoMapper;
import me.y500.mapper.QuestionMapper;
import me.y500.mapper.SameQuestionMapper;
import me.y500.mapper.UserInfoMapper;
import me.y500.mongodb.AnswerRepository;
import me.y500.mongodb.QuestionRepository;
import me.y500.mongodb.UserRepository;
import me.y500.mongodb.dataobject.User;
import me.y500.param.AgreeParams;
import me.y500.param.AnswerDetailParams;
import me.y500.param.DetailParams;
import me.y500.param.FollowParams;
import me.y500.param.ListParams;
import me.y500.param.ReplyParams;
import me.y500.param.SubmitParams;
import me.y500.result.AnswerDetailResult;
import me.y500.result.BaseResult;
import me.y500.result.CLCity;
import me.y500.result.DRLabel;
import me.y500.result.DestinationResult;
import me.y500.result.CommentList;
import me.y500.result.DestinationDesc;
import me.y500.result.DestinationResult;
import me.y500.result.DetailAnswerListResult;
import me.y500.result.DetailAnswerResult;
import me.y500.result.DomesticLabel;
import me.y500.result.ForeignLabel;
import me.y500.result.QuestionList;
import me.y500.result.QuestionListResult;
import me.y500.result.SimpleAnswer;
import me.y500.result.SimpleComment;
import me.y500.result.SimpleQuestion;
import me.y500.result.SimpleUser;
import me.y500.util.Constant;
import me.y500.util.Util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.xml.resolver.apps.resolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;


public class MainAo {
	
	private final static Logger logger = Logger.getLogger(MainAo.class);
	@Autowired
	private AgreeMapper agreeMapper;
	@Autowired
	private FavoriteMapper favoriteMapper;
	@Autowired
	private SameQuestionMapper sameQuestionMapper;
	@Autowired
	private FollowMapper followMapper;
	@Autowired
	private UploadBo uploadBo;
	@Autowired
	private UploadAo uploadAo;
	@Autowired
	private ProvinceInfoMapper provinceInfoMapper;
	@Autowired
	private CityInfoMapper cityInfoMapper;
	@Autowired
	private QuestionMapper questionMapper;
	@Autowired
	private AnswerMapper answerMapper;
	@Autowired
	private CommentMapper commentMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private DestinationMapper destinationMapper;
	@Autowired
	private QuestionRepository questionRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AnswerRepository answerRepository;
//	@Autowired
//	private CommentRepository commentRepository;
	public static void main(String[] args) {
		String s = "0012424533";
		System.out.println(s.substring(0, 3));
		System.out.println(s.substring(3, s.length()));
	}
	/**
	 * 获取问题详情列表
	 * 
	 * @param params
	 * 
	 */
	public BaseResult getDetailQuestionAndAnswer(DetailParams params, UserInfo userInfo) {
		BaseResult result = new BaseResult();
		DetailAnswerListResult detailAnswerListResult = new DetailAnswerListResult();
		result.setStat(2);
		result.setMsg("网络有问题，请重试~");
		try {
			Long qid = Long.parseLong(params.getQid());
			String footTime = params.getFootTime();
			String type = params.getType();
			int num = params.getNum();
			
			Question question = questionMapper.selectByPrimaryKey(qid);
			if (question == null) {
				result.setMsg("提问已删除~");
				return result;
			}
			UserInfo owner = userInfoMapper.selectByPrimaryKey(question.getUid());
			detailAnswerListResult.setGuest(new SimpleUser(owner));
			int answerNum = answerMapper.selectByQid(question.getId()).size();
			detailAnswerListResult.setSimpleQuestion(new SimpleQuestion(question, userInfo.getUid(), uploadBo.follow(question.getId()), uploadBo.isFollow(question.getId(), userInfo.getUid()), uploadBo.isSameQuestion(question.getId(), userInfo.getUid()), uploadBo.getImage(uploadAo.arrayChangeList(question.getImageName())), answerNum));
			List<Answer> answers = this.answerListForQuestionId(question,
					footTime, type, num);
			List<DetailAnswerResult> answerList = new ArrayList<DetailAnswerResult>();
			for (Answer item : answers) {
				UserInfo guest = userInfoMapper.selectByPrimaryKey(item
						.getUid());
				DetailAnswerResult ar = new DetailAnswerResult();
				ar.setAnswer(new SimpleAnswer(item, userInfo.getUid(), uploadBo.agree(item.getId()), uploadBo.isAgree(item.getId(), userInfo.getUid()), uploadBo.favorite(item.getId()), uploadBo.isFavorite(item.getId(), userInfo.getUid()),uploadBo.getImage(uploadAo.arrayChangeList(item.getImageName())), uploadBo.commentNum(item.getId())));
				ar.setGuest(new SimpleUser(guest));
				answerList.add(ar);
			}
			detailAnswerListResult.setAnswers(answerList);
			result.setStat(1);
			result.setMsg("success");
			result.setResult(detailAnswerListResult);
			return result;
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("网络有问题，请重试~");
			return result;
		}
	}
	
	
	/**
	 * 问题的回复列表
	 * 
	 * @param question
	 * @param headerAid
	 * @param footAid
	 * @param type
	 * @param num
	 * @return
	 */
	private List<Answer> answerListForQuestionId(Question question,
			String footTime, String type, int num) {
		List<Answer> answerList = null;
		if (Constant.FLUSH_QUESTION_ANSWER_TYPE_NEW.equals(type)) {// new
				answerList = answerMapper
						.selectListByQid(question.getId(), num);
		} else if (Constant.FLUSH_QUESTION_ANSWER_TYPE_OLD.equals(type)) {// old
			answerList = answerMapper.selectOldListByQid(question.getId(), footTime, num);
		} else {
			answerList = answerMapper.selectOldListByQid(question.getId(), footTime, num);
		}

		return answerList;

	}
	
	
	/**
	 * 搜索问题的回复列表
	 * 
	 * @param question
	 * @param headerAid
	 * @param footAid
	 * @param type
	 * @param num
	 * @return
	 */
	private List<me.y500.mongodb.dataobject.Answer> searchAnswerListForQuestionId(me.y500.mongodb.dataobject.Question question,
			String footTime, String type, int num) {
		List<me.y500.mongodb.dataobject.Answer> answerList = null;
		if (Constant.FLUSH_QUESTION_ANSWER_TYPE_NEW.equals(type)) {// new
				answerList = answerRepository.selectListByQid(question.getQid(), num);
		} else if (Constant.FLUSH_QUESTION_ANSWER_TYPE_OLD.equals(type)) {// old
			answerList = answerRepository.selectOldListByQid(question.getQid(), footTime, num);
		} else {
			answerList = answerRepository.selectOldListByQid(question.getQid(), footTime, num);
		}

		return answerList;

	}
	
	/**
	 * 回答的评论列表
	 * 
	 * @param answer
	 * @param footAid
	 * @param type
	 * @param num
	 * @return
	 */
	private List<Comment> commentListForAnswerId(Answer answer,
			String footTime, String type, int num) {
		List<Comment> commentList = null;
		if (Constant.FLUSH_QUESTION_ANSWER_TYPE_NEW.equals(type)) {// new
			commentList = commentMapper.selectListByAid(answer.getId(), num);
		} else if (Constant.FLUSH_QUESTION_ANSWER_TYPE_OLD.equals(type)) {// old
			commentList = commentMapper.selectOldListByAid(answer.getId(), footTime, num);
		} else {
			commentList = commentMapper.selectOldListByAid(answer.getId(), footTime, num);
		}

		return commentList;

	}
	
	/**
	 * 获取问题列表
	 * 
	 * @param params
	 */
	public BaseResult listQuestion(ListParams params, UserInfo userInfo) {
		BaseResult result = new BaseResult();
		QuestionListResult questionListResult = new QuestionListResult();
		result.setStat(2);
		result.setMsg("网络有问题，请重试~");
		try {
			String destinationCode = params.getDestination();
			String footTime = params.getFootTime();
			String type = params.getType();
			Integer num = params.getNum() == null ? 5 : params.getNum() ;
			
			//type=new 问题最新列表 type=hot 问题最热列表
//			if (listType.equals("new")) {
				List<QuestionList> questionLists = new ArrayList<QuestionList>();
				List<Question> questions = this.questionList(footTime, type, num, destinationCode);
				if (questions != null && questions.size() > 0) {
					for (Question question : questions) {
						QuestionList questionList = new QuestionList();
						UserInfo guest = userInfoMapper.selectByPrimaryKey(question.getUid());
						questionList.setGuest(new SimpleUser(guest));
						int answerNum = answerMapper.selectByQid(question.getId()).size();
						questionList.setSimpleQuestion(new SimpleQuestion(question, userInfo.getUid(), uploadBo.follow(question.getId()), uploadBo.isFollow(question.getId(), userInfo.getUid()), uploadBo.isSameQuestion(question.getId(), userInfo.getUid()),uploadBo.getImage(uploadAo.arrayChangeList(question.getImageName())), answerNum));
						questionLists.add(questionList);
					}
					questionListResult.setQuestionList(questionLists);
					result.setStat(1);
					result.setMsg("success");
					result.setResult(questionListResult);
					return result;
				}
//			}
			return result;
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("网络有问题，请重试~");
			return result;
		}
	}
	
	
	/**
	 * 问题列表
	 * 
	 * @param headerAid
	 * @param footAid
	 * @param type
	 * @param num
	 * @return
	 */
	private List<Question> questionList(
			String footTime, String type, int num, String destinationCode) {
		List<Question> questionList = null;
		
		if (Constant.FLUSH_QUESTION_ANSWER_TYPE_NEW.equals(type)) {
		questionList = questionMapper
						.selectList(num, destinationCode);
		} else if (Constant.FLUSH_QUESTION_ANSWER_TYPE_OLD.equals(type)) {
			questionList = questionMapper.selectOldList(footTime, destinationCode, num);
		} else {
			questionList = questionMapper.selectList(num, destinationCode);
		}

		return questionList;

	}
	
	/**
	 * 提交问题
	 * 
	 * @param params
	 * @param userInfo
	 * @return
	 */
	public BaseResult submitQuestion(SubmitParams params, UserInfo userInfo){
		BaseResult result = new BaseResult();
		result.setStat(2);
		result.setMsg("网络问题，请重试~");
		String content = params.getContent();
		String labels = params.getLabels();
		String destinationCode = params.getDestination();
		if (StringUtils.isBlank(content)) {
			result.setStat(2);
			result.setMsg("写点什么呗？");
			return result;
		}
		try {
			Question record = new Question();
			record.setContent(content);
			record.setLabel(labels);
			record.setStatus(0);
			record.setUid(userInfo.getUid());
			record.setDestinationCode(destinationCode);
			questionMapper.insert(record);
			result.setStat(1);
			result.setMsg("success");
			return result;
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("网路有问题，请重试~");
			return result;
		}
	}

	
	
//	/**
//	 * 回复
//	 * 
//	 * @param params
//	 * @param userInfo
//	 * @return
//	 */
//	public BaseResult reply(ReplyParams params, UserInfo userInfo){
//		BaseResult result = new BaseResult();
//		result.setStat(2);
//		result.setMsg("网络问题，请重试~");
//		String content = params.getContent();
//		String type = params.getType();
//		Long aid = params.getAid();
//		Long qid = params.getQid();
//		Long fid = params.getFid();
//		
//		try {
//			if (StringUtils.isNotBlank(type) && type.equals("question")) {
//				//插入回答表
//				Answer answer = new Answer();
//				answer.setContent(content);
//				answer.setQid(qid);
//				answer.setUid(userInfo.getUid());
//				answerMapper.insert(answer);
//				//question表加上回答人的id
//				Question question = questionMapper.selectByPrimaryKey(qid);
//				if (question != null && StringUtils.isNotBlank(question.getAnwser())) {
//					if (question.getAnwser().indexOf(userInfo.getUid().toString()) == -1) {
//						Question questionQuery = new Question();
//						questionQuery.setAnwser(question.getAnwser() + "," + userInfo.getUid());
//						questionMapper.update(questionQuery);
//					}
//				}else if (question != null && StringUtils.isBlank(question.getAnwser())) {
//					Question questionQuery = new Question();
//					questionQuery.setAnwser(userInfo.getUid().toString());
//					questionMapper.update(questionQuery);
//				}
//			}else {
//				//插入评论表
//				Comment comment = new Comment();
//				comment.setAid(aid);
//				comment.setContent(content);
//				comment.setFid(fid);
//				comment.setUid(userInfo.getUid());
//				commentMapper.insert(comment);
//			}
//			result.setStat(1);
//			result.setMsg("success");
//			return result;
//		} catch (Exception e) {
//			logger.info(Util.exceptionMsg(e));
//			result.setStat(2);
//			result.setMsg("网路有问题，请重试~");
//			return result;
//		}
//	}
	
	
	/**
	 * 答案详情
	 * 
	 * @param params
	 * 
	 */
	public BaseResult answerDetail(AnswerDetailParams params, UserInfo userInfo) {
		BaseResult result = new BaseResult();
		AnswerDetailResult answerDetailResult = new AnswerDetailResult();
		result.setStat(2);
		result.setMsg("网络有问题，请重试~");
		try {
			Long aid = Long.parseLong(params.getAid());
			String footTime = params.getFootTime();
			String type = params.getType();
			
			
			int num = params.getNum();
			
			Answer answer = answerMapper.selectByPrimaryKey(aid);
			if (answer == null) {
				result.setMsg("答案已删除~");
				return result;
			}
			Question question = questionMapper.selectByPrimaryKey(answer.getQid());
			int answerNum = answerMapper.selectByQid(question.getId()).size();
			answerDetailResult.setQuestion(new SimpleQuestion(question, userInfo.getUid(), uploadBo.follow(question.getId()), uploadBo.isFollow(question.getId(), userInfo.getUid()), uploadBo.isSameQuestion(question.getId(), userInfo.getUid()),uploadBo.getImage(uploadAo.arrayChangeList(question.getImageName())), answerNum));
			UserInfo owner = userInfoMapper.selectByPrimaryKey(answer.getUid());
			answerDetailResult.setGuest(new SimpleUser(owner));
			answerDetailResult.setAnswer(new SimpleAnswer(answer, userInfo.getUid(), uploadBo.agree(answer.getId()), uploadBo.isAgree(answer.getId(), userInfo.getUid()), uploadBo.favorite(answer.getId()), uploadBo.isFavorite(answer.getId(), userInfo.getUid()),uploadBo.getImage(uploadAo.arrayChangeList(answer.getImageName())), uploadBo.commentNum(answer.getId())));
			List<Comment> comments = this.commentListForAnswerId(answer, footTime, type, num);
			List<CommentList> commentList = new LinkedList();
			for (Comment item : comments) {
				UserInfo guest = userInfoMapper.selectByPrimaryKey(item
						.getUid());
				String nick = "";
				if (item.getFid() != null) {
					UserInfo user = userInfoMapper.selectByPrimaryKey(item
							.getFid());
					if (user != null) {
						nick = user.getNick();
					}
				}
				CommentList ar = new CommentList();
				ar.setComment(new SimpleComment(item, nick));
				ar.setGuest(new SimpleUser(guest));
				commentList.add(ar);
			}
			answerDetailResult.setComments(commentList);
			result.setStat(1);
			result.setMsg("success");
			result.setResult(answerDetailResult);
			return result;
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("网络有问题，请重试~");
			return result;
		}
	}
	
	/**
	 * 点赞
	 * 
	 * @param params
	 * @param userInfo
	 */
	public BaseResult agree(AgreeParams params, UserInfo userInfo) {
		BaseResult result = new BaseResult();
		result.setStat(2);
		result.setMsg("网络有问题，请重试~");
		try {
			Long aid = params.getAid();

			Answer answer = answerMapper.selectByPrimaryKey(aid);
			if (answer == null) {
				result.setMsg("答案已删除~");
				return result;
			}
			Agree agreeQuery = new Agree();
			agreeQuery.setAid(answer.getId());
			agreeQuery.setUid(userInfo.getUid());
			List<Agree> agrees = agreeMapper.select(agreeQuery);
			if (agrees.size() > 0) {
				result.setMsg("你已经赞过了~");
				return result;
			}
			agreeQuery.setStatus((byte)1);
			agreeMapper.insert(agreeQuery);
			result.setStat(1);
			result.setMsg("success");
			return result;
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("网络有问题，请重试~");
			return result;
		}
	}
	
	
	/**
	 * 关注
	 * 
	 * @param params
	 * @param userInfo
	 */
	public BaseResult follow(FollowParams params, UserInfo userInfo) {
		BaseResult result = new BaseResult();
		result.setStat(2);
		result.setMsg("网络有问题，请重试~");
		try {
			Long qid = Long.parseLong(params.getQid());

			Question question = questionMapper.selectByPrimaryKey(qid);
			if (question == null) {
				result.setMsg("问题已删除~");
				return result;
			}
			Follow followQuery = new Follow();
			followQuery.setQid(question.getId());
			followQuery.setUid(userInfo.getUid());
			List<Follow> Follows = followMapper.select(followQuery);
			if (Follows.size() > 0) {
				result.setMsg("你已经关注过了~");
				return result;
			}
			followQuery.setStatus((byte)1);
			followMapper.insert(followQuery);
			result.setStat(1);
			result.setMsg("success");
			return result;
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("网络有问题，请重试~");
			return result;
		}
	}
	
	/**
	 * 收藏
	 * 
	 * @param params
	 * @param userInfo
	 */
	public BaseResult favorite(AgreeParams params, UserInfo userInfo) {
		BaseResult result = new BaseResult();
		result.setStat(2);
		result.setMsg("网络有问题，请重试~");
		try {
			long aid = params.getAid();

			Answer answer = answerMapper.selectByPrimaryKey(aid);
			if (answer == null) {
				result.setMsg("答案已删除~");
				return result;
			}
			Favorite favoriteQuery = new Favorite();
			favoriteQuery.setAid(answer.getId());
			favoriteQuery.setUid(userInfo.getUid());
			List<Favorite> favorites = favoriteMapper.select(favoriteQuery);
			if (favorites.size() > 0) {
				result.setMsg("你已经收藏过了~");
				return result;
			}
			favoriteQuery.setStatus((byte)1);
			favoriteMapper.insert(favoriteQuery);
			result.setStat(1);
			result.setMsg("success");
			return result;
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("网络有问题，请重试~");
			return result;
		}
	}
	
	
	/**
	 * 采纳
	 * 
	 * @param params
	 * @param userInfo
	 */
	public BaseResult accept(AgreeParams params, UserInfo userInfo) {
		BaseResult result = new BaseResult();
		result.setStat(2);
		result.setMsg("网络有问题，请重试~");
		try {
			long aid = params.getAid();

			Answer answer = answerMapper.selectByPrimaryKey(aid);
			if (answer == null) {
				result.setMsg("答案已删除~");
				return result;
			}
			
			answer.setIsAccept((byte)1);
			answerMapper.update(answer);
			result.setStat(1);
			result.setMsg("success");
			return result;
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("网络有问题，请重试~");
			return result;
		}
	}
	
	/**
	 * 同问
	 * 
	 * @param params
	 * @param userInfo
	 */
	public BaseResult sameQuestion(FollowParams params, UserInfo userInfo) {
		BaseResult result = new BaseResult();
		result.setStat(2);
		result.setMsg("网络有问题，请重试~");
		try {
			long qid = Long.parseLong(params.getQid());

			Question question = questionMapper.selectByPrimaryKey(qid);
			if (question == null) {
				result.setMsg("问题已删除~");
				return result;
			}
			SameQuestion sameQuestionQuery = new SameQuestion();
			sameQuestionQuery.setQid(question.getId());
			sameQuestionQuery.setUid(userInfo.getUid());
			List<SameQuestion> sameQuestions = sameQuestionMapper.select(sameQuestionQuery);
			if (sameQuestions.size() > 0) {
				result.setMsg("你已经问过了~");
				return result;
			}
			sameQuestionQuery.setStatus((byte)1);
			sameQuestionMapper.insert(sameQuestionQuery);
			result.setStat(1);
			result.setMsg("success");
			return result;
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("网络有问题，请重试~");
			return result;
		}
	}
	
	
	/**
	 * 获取问题列表
	 * 
	 * @param params
	 */
	public BaseResult cityList() {
		BaseResult result = new BaseResult();
		DestinationResult destinationResult = new DestinationResult();
		result.setStat(2);
		result.setMsg("网络有问题，请重试~");
		try {
			List<Destination> destinations = destinationMapper.select();
			List<DRLabel> drLabels = new ArrayList<DRLabel>();
			DRLabel foreignLabel = new DRLabel();
			DRLabel domesticLabel = new DRLabel();
			foreignLabel.setName("境外");
			domesticLabel.setName("境内");
			List<DestinationDesc> domesticDestinationDescs = new ArrayList<DestinationDesc>();
			List<DestinationDesc> foreignDestinationDescs = new ArrayList<DestinationDesc>();
			for (Destination destination : destinations) {
				DestinationDesc destinationDesc = new DestinationDesc();
				if (destination.getType() == 1) {
					destinationDesc.setCode(destination.getCode());
					destinationDesc.setName(destination.getNick());
					domesticDestinationDescs.add(destinationDesc);
				}else if (destination.getType() == 2) {
					destinationDesc.setCode(destination.getCode());
					destinationDesc.setName(destination.getNick());
					foreignDestinationDescs.add(destinationDesc);
				}
			}
			domesticLabel.setDestination(domesticDestinationDescs);
			foreignLabel.setDestination(foreignDestinationDescs);
			drLabels.add(domesticLabel);
			drLabels.add(foreignLabel);
			destinationResult.setLabels(drLabels);
			result.setStat(1);
			result.setMsg("success");
			result.setResult(destinationResult);
			return result;
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("网络有问题，请重试~");
			return result;
		}
	}
	
	
	/**
	 * 获取搜索问题详情列表
	 * 
	 * @param params
	 * 
	 */
	public BaseResult getSearchDetailQuestionAndAnswer(DetailParams params, UserInfo userInfo) {
		BaseResult result = new BaseResult();
		me.y500.mongodb.result.DetailAnswerListResult detailAnswerListResult = new me.y500.mongodb.result.DetailAnswerListResult();
		result.setStat(2);
		result.setMsg("网络有问题，请重试~");
		try {
			Long qid = Long.parseLong(params.getQid());
			String footTime = params.getFootTime();
			String type = params.getType();
			int num = params.getNum();
			
			me.y500.mongodb.dataobject.Question question = questionRepository.selectByPrimaryKey(qid + "");
			if (question == null) {
				result.setMsg("提问已删除~");
				return result;
			}
			User owner = userRepository.selectByPrimaryKey(question.getUid());
			detailAnswerListResult.setGuest(new me.y500.mongodb.result.SimpleUser(owner));
			long answerNum = answerRepository.selectByQid(question.getQid());
			detailAnswerListResult.setSimpleQuestion(new me.y500.mongodb.result.SimpleQuestion(question, userInfo.getUid(), uploadBo.follow(Long.parseLong(question.getQid())), uploadBo.isFollow(Long.parseLong(question.getQid()), userInfo.getUid()), uploadBo.isSameQuestion(Long.parseLong(question.getQid()), userInfo.getUid()), uploadBo.getImage(uploadAo.arrayChangeList(question.getImage_name())), answerNum));
			List<me.y500.mongodb.dataobject.Answer> answers = this.searchAnswerListForQuestionId(question,
					footTime, type, num);
			List<me.y500.mongodb.result.DetailAnswerResult> answerList = new ArrayList<me.y500.mongodb.result.DetailAnswerResult>();
			for (me.y500.mongodb.dataobject.Answer item : answers) {
				User guest = userRepository.selectByPrimaryKey(item
						.getUid());
				me.y500.mongodb.result.DetailAnswerResult ar = new me.y500.mongodb.result.DetailAnswerResult();
				ar.setAnswer(new me.y500.mongodb.result.SimpleAnswer(item, userInfo.getUid(),uploadBo.getImage(uploadAo.arrayChangeList(item.getImage_name()))));
				ar.setGuest(new me.y500.mongodb.result.SimpleUser(guest));
				answerList.add(ar);
			}
			detailAnswerListResult.setAnswers(answerList);
			result.setStat(1);
			result.setMsg("success");
			result.setResult(detailAnswerListResult);
			return result;
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
			result.setStat(2);
			result.setMsg("网络有问题，请重试~");
			return result;
		}
	}
}
