package me.y500.ao;

import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.y500.mongodb.AnswerRepository;
import me.y500.mongodb.dataobject.Answer;
import me.y500.mongodb.dataobject.Question;
import me.y500.param.SearchParams;
import me.y500.result.BaseResult;
import me.y500.result.SearchResult;
import me.y500.result.resultDesc;
import me.y500.util.Constant;
import me.y500.util.Util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriter.MaxFieldLength;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.wltea.analyzer.lucene.IKAnalyzer;
import org.wltea.analyzer.lucene.IKQueryParser;
import org.wltea.analyzer.lucene.IKSimilarity;

public class SearchAo {
	private final static Logger logger = Logger.getLogger(SearchAo.class);
	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private AnswerRepository answerRepository;
	
	
	public void index() {
		try {
			logger.info("come in");
			List<Question> questions = mongoTemplate.findAll(Question.class);
			List<Answer> answers = mongoTemplate.findAll(Answer.class);
			// 先在数据库中拿到要创建索引的数据
			// Mongo mongo = new Mongo();
			// DB db = mongo.getDB("zhang");
			// DBCollection msg = db.getCollection("test3");
			// 是否重新创建索引文件，false：在原有的基础上追加
			boolean create = true;
			// 创建索引
			Directory directory = FSDirectory.open(new File(
					"/home/hotel_biz/index"));
			Analyzer analyzer = new IKAnalyzer();// IK中文分词器
			for (Question question : questions) {
				// System.out.println(cursor.next().get("text").toString());
				if (StringUtils.isNotBlank(question.getQid()) ) {
					IndexWriter indexWriter = new IndexWriter(directory, analyzer,
							MaxFieldLength.LIMITED);
					Document doc = new Document();
					Field fieldQid = new Field("qid", question.getQid(), Field.Store.YES, Field.Index.ANALYZED,
							Field.TermVector.WITH_POSITIONS_OFFSETS);
					Field fieldQcontent = new Field("qcontent", question.getContent(), Field.Store.YES, Field.Index.ANALYZED,
							Field.TermVector.WITH_POSITIONS_OFFSETS);
					Long answersNum = answerRepository.selectByQid(question.getQid());
					Field fieldAnswersNum = new Field("answersnum", answersNum + "", Field.Store.YES, Field.Index.ANALYZED,
							Field.TermVector.WITH_POSITIONS_OFFSETS);
					doc.add(fieldQid);
					doc.add(fieldQcontent);
					doc.add(fieldAnswersNum);
					indexWriter.addDocument(doc);
					// optimize()方法是对索引进行优化
					indexWriter.optimize();
					// 最后关闭索引
					indexWriter.close();
				}
				
			}
			
			
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
		}

	}

	public BaseResult search(SearchParams params) {
		BaseResult result = new BaseResult();
		SearchResult searchResult = new SearchResult();
		try {

			String key = params.getKey();
			int foodId = StringUtils.isBlank(params.getFoodId()) ? 0 : Integer.parseInt(params.getFoodId());
			logger.info("begin search==" + key); // 查询条件
			long starttime = System.currentTimeMillis();
			IndexReader reader = IndexReader.open(
					FSDirectory.open(new File("/home/hotel_biz/index")), true);
			IndexSearcher searcher = new IndexSearcher(reader);
			searcher.setSimilarity(new IKSimilarity()); // 在索引器中使用IKSimilarity相似度评估器
			// String[] keys = {"4","testtest"}; //关键字数组
			// String[] fields = {"id","title"}; //搜索的字段
			// BooleanClause.Occur[] flags =
			// {BooleanClause.Occur.MUST,BooleanClause.Occur.MUST};
			// //BooleanClause.Occur[]数组,它表示多个条件之间的关系
			// 使用 IKQueryParser类提供的parseMultiField方法构建多字段多条件查询
			// Query query = IKQueryParser.parseMultiField(fields,keys, flags);
			// //IKQueryParser多个字段搜索
			Query query = IKQueryParser.parse("qcontent", key); // IK搜索单个字段
			// IKAnalyzer analyzer = new IKAnalyzer();
			// Query query =MultiFieldQueryParser.parse(Version.LUCENE_CURRENT,
			// keys, fields, flags,analyzer); //用MultiFieldQueryParser得到query对象
			logger.info("querytest:" + query); // 查询条件
			logger.info("query:" + query.toString()); // 查询条件
			/*
			 * TopScoreDocCollector topCollector =
			 * TopScoreDocCollector.create(searcher.maxDoc(), false);
			 * searcher.search(query,topCollector);
			 * 
			 * ScoreDoc[] docs = topCollector.topDocs(3).scoreDocs;
			 * System.out.println(docs.length);
			 */
			List<String> searchs = new ArrayList<String>();
			/**
			 * 得到TopDocs对象之后,可以获取它的成员变量totalHits和scoreDocs.这两个成员变量的访问权限是public的,
			 * 所以可以直接访问
			 */
			TopDocs topDocs = searcher.search(query, 100);
			Integer count = topDocs.totalHits;
			ScoreDoc[] scoreDocs = topDocs.scoreDocs;
			List<resultDesc> resultDescs = new ArrayList<resultDesc>();
			
			for (int i = foodId; i < foodId + Constant.SEARCH_NUM && i < count; i++) {
				resultDesc resultDesc = new resultDesc();
				ScoreDoc scoreDoc = scoreDocs[i];
				Document document = searcher.doc(scoreDoc.doc);
				resultDesc.setContent(document.get("qcontent"));
				resultDesc.setQid(Long.parseLong(document.get("qid")));
				resultDesc.setAnswerNum(Integer.parseInt(document.get("answersnum")));
				resultDescs.add(resultDesc);
			}
			searchResult.setResultDesc(resultDescs);
			searchResult.setNum(count);
			if (foodId + Constant.SEARCH_NUM < count) {
				searchResult.setFoodId(foodId + Constant.SEARCH_NUM);
			}else {
				searchResult.setFoodId(count);
			}
			
			logger.info("查找数据量:" + count);
			long endtime = System.currentTimeMillis();
			logger.info(endtime - starttime);
			reader.close(); // 关闭索引
			result.setMsg("success");
			result.setStat(1);
			result.setResult(searchResult);
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
		}
		return result;
	}
	
	
	public static void main(String[] args) {
		 String words = "中国是世界四大文明古国之一，有着悠久的历史，距今约5000年前，以中原地区为中心开始出现聚落组织进而成国家和朝代，后历经多次演变和朝代更迭，持续时间较长的朝代有夏、商、周、汉、晋、唐、宋、元、明、清等。中原王朝历史上不断与北方游牧民族交往、征战，众多民族融合成为中华民族。20世纪初辛亥革命后，中国的君主政体退出历史舞台，取而代之的是共和政体。1949年中华人民共和国成立后，在中国大陆建立了人民代表大会制度的政体。中国有着多彩的民俗文化，传统艺术形式有诗词、戏曲、书法和国画等，春节、元宵、清明、端午、中秋、重阳等是中国重要的传统节日。";  
	        try {
				System.out.println(IKQueryParser.parse("qcontent", words));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
	    
	}
}
