package me.y500.bo;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import me.y500.dataobject.UserVerifyInfo;
import me.y500.dataobject.UserVerifyLog;
import me.y500.mapper.UserVerifyInfoMapper;
import me.y500.mapper.UserVerifyLogMapper;
import me.y500.util.DateUtil;
import me.y500.util.Util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


public class UserBo {
	private final static Logger logger = Logger.getLogger(UserBo.class);
	@Autowired
	private UserVerifyInfoMapper userVerifyInfoMapper;
	@Autowired
	private UserVerifyLogMapper userVerifyLogMapper;
	public boolean checkVerifyCode(String phone,Integer uid, String verifyCode, Integer regType) {
		//配合苹果审核做的处理
		try {
			if (StringUtils.isNotBlank(phone) && "11012345678".equals(phone)) {
				return true;
			}
			UserVerifyInfo query = new UserVerifyInfo();
			query.setLoginname(phone);
			query.setUid(uid);
			query.setVerifyCode(verifyCode.toLowerCase());
			query.setRegType(regType);
			List<UserVerifyInfo> relist = userVerifyInfoMapper
					.queryOfVerifyInfo(query);
			return relist == null ? false : relist.size() > 0;
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
		}
		return false;
	}
	
//	public void merge(Long newloginId, Long loginId) {
//		//向上合并用户订单
//		MemberOrderInfo memberQuery = new MemberOrderInfo();
//		memberQuery.setNewuid(newloginId.longValue());
//		memberQuery.setUid(loginId);
////		long start8 = System.currentTimeMillis();
//		memberOrderInfoMapper.updateAuthUid(memberQuery);
//		
//		OrderInfo orderQuery = new OrderInfo();
//		orderQuery.setNewuid(newloginId.longValue());
//		orderQuery.setUid(loginId);
//		orderInfoMapper.updateAuthUid(orderQuery);
//		
//		CardBinding cardQuerys = new CardBinding();
//		cardQuerys.setLoginId(loginId.intValue());
//		List<CardBinding> cards = cardBindingMapper.select(cardQuerys);
//		if (cards != null && cards.size() > 0) {
//			for (CardBinding card : cards) {
//				CardBinding cardQ = new CardBinding();
//				cardQ.setLoginId(newloginId.intValue());
//				cardQ.setCardType(card.getCardType());
//				List<CardBinding> cardBindings = cardBindingMapper.select(cardQuerys);
//				if (cardBindings.size() == 0) {
//					CardBinding cardQuery = new CardBinding();
//					cardQuery.setNewloginid(newloginId.intValue());
//					cardQuery.setLoginId(loginId.intValue());
//					cardQuery.setCardType(card.getCardType());
////					long start9 = System.currentTimeMillis();
//					cardBindingMapper.updateAuthUid(cardQuery);
////					long end9 = System.currentTimeMillis();
////					logger.info("cardBindingMapper.updateAuthUid:" + (end9 - start9)  + " phone=" + phone);
//				}
//			}
//		}
//	}
	
	
	public void usertime(String phone, String verifyCode) {
		UserVerifyInfo query = new UserVerifyInfo();
		query.setUserTime(new Date());
		query.setLoginname(phone);
		query.setVerifyCode(verifyCode);
		query.setStatus(2);
		userVerifyInfoMapper.updateverify(query);
	}
	
	public void usertimebyuid(Integer uid, String verifyCode) {
		UserVerifyInfo query = new UserVerifyInfo();
		query.setUserTime(new Date());
		query.setUid(uid);
		query.setVerifyCode(verifyCode);
		query.setStatus(2);
		userVerifyInfoMapper.updateverify(query);
	}
	
	/**
	 * 获得 当前时间after 分钟后，某个登录手机号发送的短信数
	 * 
	 * @author Alvise
	 * @param loginName
	 * @param after
	 * @return
	 */
	public int getSendCount(String phone, int after) {
		UserVerifyInfo query = new UserVerifyInfo();
		query.setLoginname(phone);
		query.setRegTime(DateUtil.addMinute(new Date(), after));
		return userVerifyInfoMapper.getVerifyInfoCount(query);
	}
	
	
	public boolean limitCount(Integer uid) {
		UserVerifyLog query = new UserVerifyLog();
		query.setUid(uid);
		query.setDt(DateUtil.getToday());
		UserVerifyLog log = userVerifyLogMapper.select(query);
		if (log != null && log.getVerifyCount() < 5) {
			query.setVerifyCount(log.getVerifyCount() + 1);
			userVerifyLogMapper.update(query);
			return true;
		}else if (log != null && log.getVerifyCount() >= 5) {
			return false;
		}else if (log == null) {
			query.setVerifyCount(1);
			userVerifyLogMapper.insert(query);
			return true;
		}
		return false;
	}
}
