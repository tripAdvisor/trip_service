package me.y500.bo;

import java.util.ArrayList;
import java.util.List;

import me.y500.dataobject.Agree;
import me.y500.dataobject.Answer;
import me.y500.dataobject.Comment;
import me.y500.dataobject.Favorite;
import me.y500.dataobject.Follow;
import me.y500.dataobject.Image;
import me.y500.dataobject.SameQuestion;
import me.y500.mapper.AgreeMapper;
import me.y500.mapper.AnswerMapper;
import me.y500.mapper.CommentMapper;
import me.y500.mapper.FavoriteMapper;
import me.y500.mapper.FollowMapper;
import me.y500.mapper.ImageMapper;
import me.y500.mapper.SameQuestionMapper;
import me.y500.mapper.UserVerifyInfoMapper;
import me.y500.mapper.UserVerifyLogMapper;
import me.y500.util.Constant;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class UploadBo {
	private final static Logger logger = Logger.getLogger(UploadBo.class);
	@Autowired
	private FollowMapper followMapper;
	@Autowired
	private SameQuestionMapper sameQuestionMapper;
	@Autowired
	private AgreeMapper agreeMapper;
	@Autowired
	private FavoriteMapper favoriteMapper;
	@Autowired
	private ImageMapper imageMapper;
	@Autowired
	private AnswerMapper answerMapper;
	@Autowired
	private CommentMapper commentMapper;
	
//	@Autowired
//	private AnswerRepository answerRepository;
//	
	
	public Integer follow(Long qid){
		Follow query = new Follow();
		query.setQid(qid);
		List<Follow> follows = followMapper.select(query);
		return follows.size();
	}
	
	public boolean isFollow(Long qid, Long uid){
		Follow query = new Follow();
		query.setQid(qid);
		query.setUid(uid);
		List<Follow> myfollow = followMapper.select(query);
		if (myfollow.size() == 1) {
			return true;
		}
		return false;
	}
	
	
	public boolean isSameQuestion(Long qid, Long uid){
		SameQuestion sameQuery = new SameQuestion();
		sameQuery.setQid(qid);
		sameQuery.setUid(uid);
		List<SameQuestion> sameQuestions = sameQuestionMapper.select(sameQuery);
		if (sameQuestions.size() == 1) {
			return true;
		}
		return false;
	}
	
	
	public Integer agree(Long aid){
		Agree agreeQuery = new Agree();
		agreeQuery.setAid(aid);
		List<Agree> agrees = agreeMapper.select(agreeQuery);
		return agrees.size();
	}
	
	public boolean isAgree(Long aid, Long uid){
		Agree agreeQuery = new Agree();
		agreeQuery.setAid(aid);
		agreeQuery.setUid(uid);
		List<Agree> agreeList = agreeMapper.select(agreeQuery);
		if (agreeList.size() == 1) {
			return true;
		}
		return false;
	}
	
	
	public Integer favorite(Long aid){
		Favorite query = new Favorite();
		query.setAid(aid);
		List<Favorite> favorites = favoriteMapper.select(query);
		return favorites.size();
	}
	
	public boolean isFavorite(Long aid, Long uid){
		Favorite query = new Favorite();
		query.setAid(aid);
		query.setUid(uid);
		List<Favorite> favoriteList = favoriteMapper.select(query);
		if (favoriteList.size() == 1) {
			return true;
		}
		return false;
	}
	
	public List<me.y500.result.Image> getImage(List<String> imageNames){
		List<me.y500.result.Image> imageList = new ArrayList<me.y500.result.Image>();
		if (imageNames == null) {
			return null;
		}
		for (String imageName : imageNames) {
			List<Image> images = imageMapper.selectByName(Long.parseLong(imageName));
			if (images.size() > 0) {
				for (Image image : images) {
					me.y500.result.Image imageResult = new me.y500.result.Image();
					imageResult.setHeight(image.getImageLong());
					imageResult.setWidth(image.getWidth());
					imageResult.setLocation(image.getLocation());
					imageResult.setPath(Constant.IMAGE_PATH + image.getFullName());
					imageList.add(imageResult);
				}
			}
		}
		return imageList;
	}
	
	public long answerNum(Long qid){
		List<Answer> answers = answerMapper.selectByQid(qid);
		return answers.size();
	}
	
	public Integer commentNum(Long aid){
		List<Comment> comments = commentMapper.select(aid);
		return comments.size();
	}
}
