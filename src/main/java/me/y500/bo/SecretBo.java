package me.y500.bo;

import java.net.URLDecoder;
import java.util.Map;

import me.y500.util.AESCoder;
import me.y500.util.MD5;
import me.y500.util.Util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSON;



public class SecretBo {
	private final static Logger logger = Logger.getLogger(SecretBo.class);
	private static String salt = "our future";

	/**
	 * 请求报文解密
	 * 
	 * @param data
	 * @param time
	 * @param sign
	 * @return
	 */
	public String decrypt(String data, String time, String sign, String mode) {
		try {
			if (StringUtils.isNotBlank(mode) && mode.equals("plain")) {
				return data;
			}
			String dataMd5 = MD5.getMD5(data + time + salt);
			String realSign = dataMd5.substring(5, 10);
			if (!realSign.equals(sign)) {
				return "";
			} else {
				data = AESCoder.decryptHttpRes(data);
				data = URLDecoder.decode(data, "utf-8");
				return data;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "";

	}
	

	/**
	 * md5生成唯一码加密
	 * 
	 * @param code
	 * @return
	 */
	public static String parseHexStr2Byte(String str, String code) {
		try {
			return MD5.getMD5(MD5.getMD5(str) + code);
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
		}
		return "";
	}
	
	public static void main(String[] args) {
		String dataMd5 = MD5.getMD5("wYCbU8hdHBiP5Gu9+1LROA==" + salt);
		String data;
		try {
			data = AESCoder.decryptHttpRes(dataMd5);
			data = URLDecoder.decode(data, "utf-8");
			System.out.println(data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * 返回客户端加密字符串
	 * 
	 * @author wuc
	 * @param s
	 * @param mode
	 * @return
	 */
	public static String VerifyObjToXml(Object s, String mode) {
		try {
			String res = JSON.toJSONString(s);
			if (StringUtils.isNotBlank(mode) && mode.equals("plain")) {
				logger.info("result=" + res);
				return res;
			}
			logger.info("result=" + res);
			return AESCoder.encryptData(res);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

}
