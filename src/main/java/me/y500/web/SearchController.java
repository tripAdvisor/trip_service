package me.y500.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import me.y500.ao.SearchAo;
import me.y500.bo.SecretBo;
import me.y500.param.ListParams;
import me.y500.param.SearchParams;
import me.y500.result.BaseResult;
import me.y500.util.Constant;
import me.y500.util.Util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSON;

@Controller
@RequestMapping("/keysearch")
public class SearchController {
	private final static Logger logger = Logger.getLogger(SearchController.class);
	@Autowired
	private SecretBo secretBo;
	@Autowired
	private SearchAo searchAo;
	/**
	 * 搜索
	 * 
	 * @param time
	 * @param data
	 * @param sign
	 * @param mode
	 * @return
	 */
	@RequestMapping(value = "/search", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public void search(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode
			) throws IOException{
		PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	SearchParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, SearchParams.class);
		}
    	
		BaseResult result = searchAo.search(params);
    	long end = System.currentTimeMillis();
		logger.info("问题列表耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
	}
	
	
	/**
	 * 索引
	 * 
	 * @param time
	 * @param data
	 * @param sign
	 * @param mode
	 * @return
	 */
	@RequestMapping(value = "/index", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public void search(HttpServletRequest request, HttpServletResponse response
			) throws IOException{
		PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
		searchAo.index();
    	long end = System.currentTimeMillis();
		logger.info("更新索引耗时：" + (end - start));
	}
}
