package me.y500.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import me.y500.ao.MainAo;
import me.y500.bo.SecretBo;
import me.y500.param.AgreeParams;
import me.y500.param.AnswerDetailParams;
import me.y500.param.BaseParams;
import me.y500.param.DetailParams;
import me.y500.param.FollowParams;
import me.y500.param.ListParams;
import me.y500.param.ReplyParams;
import me.y500.param.SubmitParams;
import me.y500.result.BaseResult;
import me.y500.util.Constant;
import me.y500.util.Util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;



@Controller
@RequestMapping("/question")
public class MainController extends AbstractController{
	private final static Logger logger = Logger.getLogger(MainController.class);
	@Autowired
	private SecretBo secretBo;
	@Autowired
	private MainAo mainAo ;
	
	
	/**
	 * 问题列表
	 * 
	 * @param time
	 * @param data
	 * @param sign
	 * @param mode
	 * @return
	 */
	@RequestMapping(value = "/list", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public void listQuestion(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode
			) throws IOException{
		PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	ListParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, ListParams.class);
		}
    	if (StringUtils.isBlank(params.getType())){
    		params.setType(Constant.LIST_TYPE);
    	}

		BaseResult result = mainAo.listQuestion(params, getUser(request));
    	long end = System.currentTimeMillis();
		logger.info("问题列表耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
	}
	
	/**
	 * 问题详情
	 * 
	* @param time
	 * @param data
	 * @param sign
	 * @param mode
	 * @return
	 */
	@RequestMapping(value = "/detail", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public void detailQuestion(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode
			) throws IOException{
		PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	DetailParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, DetailParams.class);
		}
    	if (StringUtils.isNotBlank(params.getFootTime())) {
			params.setType(Constant.FLUSH_QUESTION_ANSWER_TYPE_OLD);
		}else {
			params.setType(Constant.FLUSH_QUESTION_ANSWER_TYPE_NEW);
		}
		if (params.getNum() == 0 || params.getNum() > 20)
			params.setNum(Constant.FLUSH_QUESTION_ANSWER_NUM);

		BaseResult result = mainAo
				.getDetailQuestionAndAnswer(params, getUser(request));
    	long end = System.currentTimeMillis();
		logger.info("问题详情耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
	}
	
	
//	/**
//	 * 提交问题
//	 * 
//	 * @param time
//	 * @param data
//	 * @param sign
//	 * @param mode
//	 * @return
//	 * @throws IOException
//	 */
//	@RequestMapping(value = "/submit", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
//	public void submitQuestion(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
//            @RequestParam(value = "time", required = false) String time,
//            @RequestParam(value = "sign", required = false) String sign,
//            @RequestParam(value = "mode", required = false) String mode
//			) throws IOException {
//		PrintWriter pwout = response.getWriter();
//    	long start = System.currentTimeMillis();
//    	data = secretBo.decrypt(data, time, sign, mode);
//    	SubmitParams params = null;
//    	if (StringUtils.isBlank(data)) {
//			BaseResult result = new BaseResult();
//			result.setStat(2);
//			result.setMsg("非法请求");
//		}else {
//			params = JSON.parseObject(data, SubmitParams.class);
//		}
//
//		BaseResult result = mainAo.submitQuestion(params, getUser(request));
//    	long end = System.currentTimeMillis();
//		logger.info("提交问题耗时：" + (end - start));
//		pwout.print(Util.VerifyObjToXml(result, mode));
//	}
	
//	/**
//	 * 提交问题
//	 * 
//	* @param time
//	 * @param data
//	 * @param sign
//	 * @param mode
//	 * @return
//	 * @throws IOException
//	 */
//	@RequestMapping(value = "/reply", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
//	public void reply(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
//            @RequestParam(value = "time", required = false) String time,
//            @RequestParam(value = "sign", required = false) String sign,
//            @RequestParam(value = "mode", required = false) String mode
//			) throws IOException {
//		PrintWriter pwout = response.getWriter();
//    	long start = System.currentTimeMillis();
//    	data = secretBo.decrypt(data, time, sign, mode);
//    	ReplyParams params = null;
//    	if (StringUtils.isBlank(data)) {
//			BaseResult result = new BaseResult();
//			result.setStat(2);
//			result.setMsg("非法请求");
//		}else {
//			params = JSON.parseObject(data, ReplyParams.class);
//		}
//
//		BaseResult result = mainAo.reply(params, getUser(request));
//    	long end = System.currentTimeMillis();
//		logger.info("回复问题耗时：" + (end - start));
//		pwout.print(Util.VerifyObjToXml(result, mode));
//	}
	
	
	/**
	 * 回答详情
	 * 
	 * @param time
	 * @param data
	 * @param sign
	 * @param mode
	 * @return
	 */
	@RequestMapping(value = "/answerdetail", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public void anwserdetail(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode
			) throws IOException{
		PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	AnswerDetailParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, AnswerDetailParams.class);
		}
		if (StringUtils.isNotBlank(params.getFootTime())) {
			params.setType(Constant.FLUSH_QUESTION_ANSWER_TYPE_OLD);
		}else {
			params.setType(Constant.FLUSH_QUESTION_ANSWER_TYPE_NEW);
		}
		if (params.getNum() == 0 || params.getNum() > 20)
			params.setNum(Constant.FLUSH_QUESTION_ANSWER_NUM);

		BaseResult result = mainAo
				.answerDetail(params, getUser(request));
    	long end = System.currentTimeMillis();
		logger.info("答案详情耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
	}
	
	/**
	 * 点赞
	 * 
	 * @param time
	 * @param data
	 * @param sign
	 * @param mode
	 * @return
	 */
	@RequestMapping(value = "/agree", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public void agree(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode
			) throws IOException{
		PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	AgreeParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, AgreeParams.class);
		}

		BaseResult result = mainAo
				.agree(params, getUser(request));
    	long end = System.currentTimeMillis();
		logger.info("点赞耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
	}
	
	/**
	 * 关注
	 * 
	 * @param time
	 * @param data
	 * @param sign
	 * @param mode
	 * @return
	 */
	@RequestMapping(value = "/follow", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public void follow(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode
			) throws IOException{
		PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	FollowParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, FollowParams.class);
		}

		BaseResult result = mainAo
				.follow(params, getUser(request));
    	long end = System.currentTimeMillis();
		logger.info("关注耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
	}
	
	/**
	 * 收藏
	 * 
	 * @param time
	 * @param data
	 * @param sign
	 * @param mode
	 * @return
	 */
	@RequestMapping(value = "/favorite", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public void favorite(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode
			) throws IOException{
		PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	AgreeParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, AgreeParams.class);
		}

		BaseResult result = mainAo
				.favorite(params, getUser(request));
    	long end = System.currentTimeMillis();
		logger.info("收藏耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
	}
	
	/**
	 * 采纳答案
	 * 
	 * @param time
	 * @param data
	 * @param sign
	 * @param mode
	 * @return
	 */
	@RequestMapping(value = "/accept", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public void accept(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode
			) throws IOException{
		PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	AgreeParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, AgreeParams.class);
		}

		BaseResult result = mainAo
				.accept(params, getUser(request));
    	long end = System.currentTimeMillis();
		logger.info("采纳耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
	}
	
	/**
	 * 同问
	 * 
	 * @param time
	 * @param data
	 * @param sign
	 * @param mode
	 * @return
	 */
	@RequestMapping(value = "/samequestion", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public void sameQuestion(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode
			) throws IOException{
		PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	FollowParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, FollowParams.class);
		}

		BaseResult result = mainAo
				.sameQuestion(params, getUser(request));
    	long end = System.currentTimeMillis();
		logger.info("采纳耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
	}
	
	
	
	/**
	 * 城市列表
	 * 
	 * @param time
	 * @param data
	 * @param sign
	 * @param mode
	 * @return
	 */
	@RequestMapping(value = "/destination", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public void cityList(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode
			) throws IOException{
		PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	BaseParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, BaseParams.class);
		}

		BaseResult result = mainAo.cityList();
    	long end = System.currentTimeMillis();
		logger.info("获取城市列表耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
	}
	
	
	/**
	 * 问题详情
	 * 
	* @param time
	 * @param data
	 * @param sign
	 * @param mode
	 * @return
	 */
	@RequestMapping(value = "/searchquestiondetail", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public void searchquestiondetail(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode
			) throws IOException{
		PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	DetailParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, DetailParams.class);
		}
    	if (StringUtils.isNotBlank(params.getFootTime())) {
			params.setType(Constant.FLUSH_QUESTION_ANSWER_TYPE_OLD);
		}else {
			params.setType(Constant.FLUSH_QUESTION_ANSWER_TYPE_NEW);
		}
		if (params.getNum() == 0 || params.getNum() > 20)
			params.setNum(Constant.FLUSH_QUESTION_ANSWER_NUM);

		BaseResult result = mainAo
				.getSearchDetailQuestionAndAnswer(params, getUser(request));
    	long end = System.currentTimeMillis();
		logger.info("搜索问题详情耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
	}
}
