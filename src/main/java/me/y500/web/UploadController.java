package me.y500.web;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import me.y500.ao.MainAo;
import me.y500.ao.UploadAo;
import me.y500.bo.SecretBo;
import me.y500.param.AnswerParams;
import me.y500.param.FollowParams;
import me.y500.param.ReplyParams;
import me.y500.param.SubmitParams;
import me.y500.param.UserInfoSettingParams;
import me.y500.result.BaseResult;
import me.y500.util.Util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.alibaba.fastjson.JSON;

@Controller
@RequestMapping("/submit")
// MultiActionController
public class UploadController extends AbstractController {
	private final static Logger logger = Logger
			.getLogger(UploadController.class);
	@Autowired
	private SecretBo secretBo;
	@Resource
	private UploadAo uploadAo;

	@RequestMapping(value = "/submitquestion", method = RequestMethod.POST)
	public void uploadFiles(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "data") String data,
			@RequestParam(value = "time", required = false) String time,
			@RequestParam(value = "sign", required = false) String sign,
			@RequestParam(value = "mode", required = false) String mode)
			throws IllegalStateException, IOException {
		PrintWriter pwout = response.getWriter();
		long start = System.currentTimeMillis();
		data = secretBo.decrypt(data, time, sign, mode);
		SubmitParams params = null;
		if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		} else {
			params = JSON.parseObject(data, SubmitParams.class);
		}

		BaseResult result = uploadAo.submitQuestion(request, params, getUser(request));
		long end = System.currentTimeMillis();
		logger.info("提交问题耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));

	}

	@RequestMapping(value = "/answer", method = RequestMethod.POST)
	public void answer(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "data") String data,
			@RequestParam(value = "time", required = false) String time,
			@RequestParam(value = "sign", required = false) String sign,
			@RequestParam(value = "mode", required = false) String mode)
			throws IllegalStateException, IOException {
		PrintWriter pwout = response.getWriter();
		long start = System.currentTimeMillis();
		data = secretBo.decrypt(data, time, sign, mode);
		AnswerParams params = null;
		if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		} else {
			params = JSON.parseObject(data, AnswerParams.class);
		}

		BaseResult result = uploadAo.answer(request, params, getUser(request));
		long end = System.currentTimeMillis();
		logger.info("提交问题耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));

	}
	
	@RequestMapping(value = "/reply", method = RequestMethod.POST)
	public void reply(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "data") String data,
			@RequestParam(value = "time", required = false) String time,
			@RequestParam(value = "sign", required = false) String sign,
			@RequestParam(value = "mode", required = false) String mode)
			throws IllegalStateException, IOException {
		PrintWriter pwout = response.getWriter();
		long start = System.currentTimeMillis();
		data = secretBo.decrypt(data, time, sign, mode);
		ReplyParams params = null;
		if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		} else {
			params = JSON.parseObject(data, ReplyParams.class);
		}

		BaseResult result = uploadAo.reply(params, getUser(request));
		long end = System.currentTimeMillis();
		logger.info("回复评论耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));

	}

}

// //创建一个通用的多部分解析器
// CommonsMultipartResolver multipartResolver = new
// CommonsMultipartResolver(request.getSession().getServletContext());
// //判断 request 是否有文件上传,即多部分请求
// if(multipartResolver.isMultipart(request)){
// //转换成多部分request
// MultipartHttpServletRequest multiRequest =
// (MultipartHttpServletRequest)request;
// //取得request中的所有文件名
// Iterator<String> iter = multiRequest.getFileNames();
// while(iter.hasNext()){
// //记录上传过程起始时的时间，用来计算上传时间
// int pre = (int) System.currentTimeMillis();
// //取得上传文件
// MultipartFile file = multiRequest.getFile(iter.next());
// if(file != null){
// //取得当前上传文件的文件名称
// String myFileName = file.getOriginalFilename();
// //如果名称不为“”,说明该文件存在，否则说明该文件不存在
// if(myFileName.trim() !=""){
// System.out.println(myFileName);
// //重命名上传后的文件名
// String fileName = "demoUpload" + file.getOriginalFilename();
// //定义上传路径
// String path = request.getSession().getServletContext().getRealPath("/") +
// "upload/" + file.getOriginalFilename();
// File localFile = new File(path);
// file.transferTo(localFile);
// logger.info("upload success=====" + fileName);
// }
// }
// //记录上传该文件后的时间
// int finaltime = (int) System.currentTimeMillis();
// logger.info(finaltime - pre);
// }
//
// }
// return "/success";
// }
