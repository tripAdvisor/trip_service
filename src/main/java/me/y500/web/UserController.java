package me.y500.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import me.y500.ao.UserAo;
import me.y500.bo.SecretBo;
import me.y500.param.AuthPhoneParams;
import me.y500.param.BaseParams;
import me.y500.param.PhoneAuthParams;
import me.y500.param.UserInfoSettingParams;
import me.y500.result.BaseResult;
import me.y500.util.Util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSON;

@Controller
@RequestMapping("/user")
public class UserController extends AbstractController{
	private final static Logger logger = Logger.getLogger(UserController.class);
	@Autowired
	private SecretBo secretBo;
	@Autowired
	private UserAo userAo;
	
    @RequestMapping(value = { "/authdevice" })
    public void userAuth(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode)
            throws IOException {
    	PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	BaseParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, BaseParams.class);
		}
    	BaseResult result = userAo.userAuth(getUser(request), params);
    	long end = System.currentTimeMillis();
		logger.info("用户设备注册耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
    }
    
    @RequestMapping(value = { "/authphone" })
    public void authPhone(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode)
            throws IOException {
    	PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	AuthPhoneParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, AuthPhoneParams.class);
		}
    	BaseResult result = userAo.authPhone(getUser(request), getWxUser(request), params);
    	long end = System.currentTimeMillis();
		logger.info("用户绑定手机耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
    }
//    
//    @RequestMapping("/authparse")
//    public void authparse(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
//            @RequestParam(value = "time", required = false) String time,
//            @RequestParam(value = "sign", required = false) String sign,
//            @RequestParam(value = "mode", required = false) String mode) throws IOException {
//    	PrintWriter pwout = response.getWriter();
//    	long start = System.currentTimeMillis();
//    	data = secretBo.decrypt(data, time, sign, mode);
//    	AuthParseParams params = null;
//    	if (StringUtils.isBlank(data)) {
//			BaseResult result = new BaseResult();
//			result.setStat(2);
//			result.setMsg("非法请求");
//		}else {
//			params = JSON.parseObject(data, AuthParseParams.class);
//		}
//    	BaseResult result = userAo.authparse(params);
//    	long end = System.currentTimeMillis();
//		logger.info("认证头认证耗时：" + (end - start));
//		pwout.print(Util.VerifyObjToXml(result, mode));
//    }
//    
    @RequestMapping("/phoneauth")
    public void preregister(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode) throws IOException {
    	PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	PhoneAuthParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, PhoneAuthParams.class);
		}
    	BaseResult result = userAo.phoneauth(getLoginId(request), params);
    	long end = System.currentTimeMillis();
		logger.info("用户发送验证码耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
    } 
    
    
    @RequestMapping("/setting")
    public void setting(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode) throws IOException {
    	PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	UserInfoSettingParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, UserInfoSettingParams.class);
		}
    	BaseResult result = userAo.setting(getUser(request), params);
    	long end = System.currentTimeMillis();
		logger.info("用户完善身份信息耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
    }
    
    
    @RequestMapping("/show")
    public void show(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode) throws IOException {
    	PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	BaseParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, BaseParams.class);
		}
    	BaseResult result = userAo.show(getUser(request));
    	long end = System.currentTimeMillis();
		logger.info("用户获取个人资料耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
    }
    
    
    
    
    @RequestMapping("/myfavorite")
    public void myfavorite(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode) throws IOException {
    	PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	BaseParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, BaseParams.class);
		}
    	BaseResult result = userAo.myfavorite(getUser(request));
    	long end = System.currentTimeMillis();
		logger.info("用户获取我的收藏耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
    }
    
    
    
    @RequestMapping("/myanswer")
    public void myanswer(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode) throws IOException {
    	PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	BaseParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, BaseParams.class);
		}
    	BaseResult result = userAo.myanswer(getUser(request));
    	long end = System.currentTimeMillis();
		logger.info("用户获取我的答案耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
    }
    
    
    
    @RequestMapping("/myquestion")
    public void myquestion(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode) throws IOException {
    	PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	BaseParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, BaseParams.class);
		}
    	BaseResult result = userAo.myquestion(getUser(request));
    	long end = System.currentTimeMillis();
		logger.info("用户获取我的问题耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
    }
    
    
    
    @RequestMapping("/myfollow")
    public void myfollow(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode) throws IOException {
    	PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	BaseParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, BaseParams.class);
		}
    	BaseResult result = userAo.myfollow(getUser(request));
    	long end = System.currentTimeMillis();
		logger.info("用户获取我的关注耗时：" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
    }
    
//    @RequestMapping("/pushtoken")
//    public void pushtoken(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
//            @RequestParam(value = "time", required = false) String time,
//            @RequestParam(value = "sign", required = false) String sign,
//            @RequestParam(value = "mode", required = false) String mode) throws IOException {
//    	PrintWriter pwout = response.getWriter();
//    	long start = System.currentTimeMillis();
//    	data = secretBo.decrypt(data, time, sign, mode);
//    	PushTokenParams params = null;
//    	if (StringUtils.isBlank(data)) {
//			BaseResult result = new BaseResult();
//			result.setStat(2);
//			result.setMsg("非法请求");
//		}else {
//			params = JSON.parseObject(data, PushTokenParams.class);
//		}
//    	BaseResult result = userAo.pushToken(getAuthLoginId(request), params);
//    	long end = System.currentTimeMillis();
//		logger.info("上传pushToken耗时:" + (end - start));
//		pwout.print(Util.VerifyObjToXml(result, mode));
//    }
//    
//    
    @RequestMapping("/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
            @RequestParam(value = "time", required = false) String time,
            @RequestParam(value = "sign", required = false) String sign,
            @RequestParam(value = "mode", required = false) String mode) throws IOException {
    	PrintWriter pwout = response.getWriter();
    	long start = System.currentTimeMillis();
    	data = secretBo.decrypt(data, time, sign, mode);
    	BaseParams params = null;
    	if (StringUtils.isBlank(data)) {
			BaseResult result = new BaseResult();
			result.setStat(2);
			result.setMsg("非法请求");
		}else {
			params = JSON.parseObject(data, BaseParams.class);
		}
    	BaseResult result = userAo.logout(getLoginId(request), params);
    	long end = System.currentTimeMillis();
		logger.info("用户注销耗时:" + (end - start));
		pwout.print(Util.VerifyObjToXml(result, mode));
    }
//    
//    @RequestMapping(value = { "/authwx" })
//    public void authWx(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
//            @RequestParam(value = "time", required = false) String time,
//            @RequestParam(value = "sign", required = false) String sign,
//            @RequestParam(value = "mode", required = false) String mode)
//            throws IOException {
//    	PrintWriter pwout = response.getWriter();
//    	long start = System.currentTimeMillis();
//    	data = secretBo.decrypt(data, time, sign, mode);
//    	AuthWx params = null;
//    	if (StringUtils.isBlank(data)) {
//			BaseResult result = new BaseResult();
//			result.setStat(2);
//			result.setMsg("非法请求");
//		}else {
//			params = JSON.parseObject(data, AuthWx.class);
//		}
//    	BaseResult result = userAo.authWx(getUser(request), params);
//    	long end = System.currentTimeMillis();
//		logger.info("用户绑定微信耗时：" + (end - start));
//		pwout.print(Util.VerifyObjToXml(result, mode));
//    }
//    
//    @RequestMapping(value = { "/unbindingwx" })
//    public void unBindingWx(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
//            @RequestParam(value = "time", required = false) String time,
//            @RequestParam(value = "sign", required = false) String sign,
//            @RequestParam(value = "mode", required = false) String mode)
//            throws IOException {
//    	PrintWriter pwout = response.getWriter();
//    	long start = System.currentTimeMillis();
//    	BaseResult result = userAo.unBindingWx(getUser(request), getWxUser(request));
//    	long end = System.currentTimeMillis();
//		logger.info("微信解绑耗时：" + (end - start));
//		pwout.print(Util.VerifyObjToXml(result, mode));
//    }
//    
//    @RequestMapping(value = { "/alidatecode" })
//    public void alidatecode(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "data") String data,
//            @RequestParam(value = "time", required = false) String time,
//            @RequestParam(value = "sign", required = false) String sign,
//            @RequestParam(value = "mode", required = false) String mode)
//            throws IOException {
//    	PrintWriter pwout = response.getWriter();
//    	long start = System.currentTimeMillis();
//    	BaseResult result = userAo.alidateCode(getLoginId(request));
//    	long end = System.currentTimeMillis();
//		logger.info("获取图形验证码耗时：" + (end - start));
//		pwout.print(Util.VerifyObjToXml(result, mode));
//    }
}
