package me.y500.web;

import javax.servlet.http.HttpServletRequest;

import me.y500.dataobject.UserInfo;
import me.y500.dataobject.WxUserInfo;
import me.y500.util.Constant;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;


public abstract class AbstractController extends MultiActionController{
	private final static Logger logger = Logger.getLogger(AbstractController.class);
    public UserInfo getUser(HttpServletRequest request) {
    	UserInfo user = (UserInfo) request.getAttribute(Constant.SESSION_USERBEAN);
        return user;
    }

    public Long getLoginId(HttpServletRequest request) {
    	UserInfo user = getUser(request);
        if (user != null) {
            return new Long(user.getUid());
        }
        return new Long(0);
    }

    
    
    
    
    public WxUserInfo getWxUser(HttpServletRequest request) {
    	WxUserInfo wxUserInfo = (WxUserInfo) request.getAttribute(Constant.SESSION_WX);
        return wxUserInfo;
    }
    
}
