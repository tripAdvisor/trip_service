package me.y500.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Tools {

	@SuppressWarnings("rawtypes")
	private static ThreadLocal threadLocal = new ThreadLocal() {
		protected synchronized Object initialValue() {
			Map<String, String> publicParams = new HashMap<String, String>();
			publicParams.put("source", "kj_jdgj");
			return publicParams;
		}
	};

	public Map<String, String> getPublicParams() {
		return (Map<String, String>) this.threadLocal.get();
	}

	public String getPublicParamByKey(String key) {
		Map<String, String> publicParams = (Map<String, String>) this.threadLocal.get();
		return publicParams.get(key);
	}

	public String addPublicParams(String key, String value) {
		Map<String, String> publicParams = (Map<String, String>) this.threadLocal.get();
		return publicParams.put(key, value);
	}
	public void removePublicParams(){
		this.threadLocal.remove();
	}
	
	
}
