package me.y500.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Character.UnicodeBlock;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class Util {
	private static Pattern phonePattern = Pattern.compile("^1[3-9]\\d{9}$");
	private final static Logger logger = Logger.getLogger(Util.class);

	public static void main(String[] args) {
		
	}
	
	
	
	/**
	 * 对返回给客户端的结果做处理（压缩，加密）
	 * 
	 * @author wuc
	 * @param s
	 * @param mode
	 * @return
	 */
	public static String VerifyObjToXml(Object s, String mode) {
		try {
			String res = JSON.toJSONString(s,SerializerFeature.DisableCircularReferenceDetect);
			if (StringUtils.isNotBlank(mode) && mode.equals("plain")) {
				logger.info("result:" + res);
				return res;
			}
			logger.info("result:" + res);
			return AESCoder.encryptData(res);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	

	/**
	 * 输出详细错误信息
	 * 
	 * @author wuc
	 * @param e
	 * @return
	 */
	public static String exceptionMsg(Exception e) {
		StringWriter output = new StringWriter();
		PrintWriter writer = new PrintWriter(output, true);
		try {
			e.printStackTrace(writer);
			return output.toString();
		} finally {
			writer.close();
		}
	}

	/**
	 * 去空格，去换行，去制表符
	 * 
	 * @author wuc
	 * @param str
	 * @return
	 */
	public static String replaceBlank(String str) {
		String dest = "";
		if (str != null) {
			Pattern p = Pattern.compile("\\s*|\t|\r|\n");
			Matcher m = p.matcher(str);
			dest = m.replaceAll("");
		}
		return dest;
	}



	/**
	 * 取得4位随机校验码
	 * 
	 * @author wuc
	 * @author Alvise
	 * @return
	 */
	public static String getVerifyCode() {
		Random rm = new Random();
		double pross = (1 + rm.nextDouble()) * Math.pow(10, 4);
		String fixLenthString = String.valueOf(pross);
		return fixLenthString.substring(1, 5);
	}

//	/**
//	 * 校验名字
//	 * @author wuc
//	 * @param nick
//	 * @return
//	 */
//	public static boolean checkNick(String nick) {
//		if (StringUtils.isBlank(nick)) {
//			return false;
//		}
//		Pattern pattern = Pattern.compile("[\u4e00-\u9fa5]{2,4}");
//		Matcher m = pattern.matcher(nick);
//		return m.matches();
//	}
	
	/**
	 * 校验昵称
	 * @author wuc
	 * @param nick
	 * @return
	 */
	public static boolean checkNick(String nick) {
		if (StringUtils.isBlank(nick)) {
			return false;
		}
		Pattern pattern = Pattern.compile("^[\u4e00-\u9fa5a-zA-Z0-9_]+$");
		Matcher m = pattern.matcher(nick);
		return m.matches();
	}

	/**
	 * 校验手机电话号码
	 * @author wuc
	 * @param phoneNumber
	 * @return
	 */
	public static boolean phoneNumberCheck(String phoneNumber) {
		if (StringUtils.isBlank(phoneNumber)
				|| !StringUtils.isNumeric(phoneNumber)) {
			return false;
		}

		Matcher matcher1 = phonePattern.matcher(phoneNumber);
		return matcher1.find();

	}

	/**
	 * 删除参数中的null
	 * 
	 * @author wuc
	 * @param str
	 *            
	 * @return String
	 */
	public static String delNull(String str) {
		if (str == null) {
			return "";
		} else if (str.trim().equals("null") || str.trim().indexOf("null") > 0) {
			return "";
		} else {
			return str.trim();
		}
	}

	/**
	 * 判断字符串是否double型
	 * 
	 * @author wuc
	 * @param str
	 * @return
	 */
	public static boolean isDouble(String str) {
		if (str == null || StringUtils.isBlank(str)) {
			return false;
		}
		int sz = str.length();
		for (int i = 0; i < sz; i++) {
			if ('.' == str.charAt(i)) {
				continue;
			}
			if (Character.isDigit(str.charAt(i)) == false) {
				return false;
			}
		}
		return true;
	}

	// 特殊字符过滤
	public static List<Character> specialChar = new ArrayList<Character>();

	static {
		specialChar.add('');
	}

	/**
	 * 过滤微信昵称特殊字符
	 * 
	 * @author wuc
	 * @param text
	 * @return
	 */
	public static String cleanText(String text) {
		String realText = text;
		try {
			char[] myBuffer = text.toCharArray();

			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < text.length(); i++) {
				if (specialChar.contains(myBuffer[i])) {
					continue;
				}

				UnicodeBlock ub = UnicodeBlock.of(myBuffer[i]);
				if (ub == UnicodeBlock.BASIC_LATIN) {
					// 英文及数字等
					sb.append(myBuffer[i]);
				} else if (ub == UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
					// 全角半角字符
					sb.append(myBuffer[i]);
				} else if (myBuffer[i] >= 0xD800 && myBuffer[i] <= 0xDFFF) {
					i++;
					continue;
				} else {
					// 汉字
					String temp = String.valueOf(myBuffer[i]);
					if (judgeIfSpecial(temp.getBytes("unicode"))) {
						continue;
					} else {
						sb.append(myBuffer[i]);
					}
				}
			}
			realText = sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
			realText = "weixin";
		}
		return realText;
	}

	public static boolean judgeIfSpecial(byte[] c) {
		if (c.length != 2 && c.length != 4)
			return false;
		byte[] b = new byte[2];
		if (c.length == 4) {
			b[0] = c[2];
			b[1] = c[3];
		} else {
			b[0] = c[0];
			b[1] = c[1];
		}
		if (b[0] != -32 && b[0] != -31 && b[0] != -30 && b[0] != -29
				&& b[0] != -28 && b[0] != -27 && b[0] != 38 && b[0] != 39) {
			return false;
		}
		switch (b[0]) {
		case -32: // 0xe0 0xE001,0xE05A
			if (b[1] >= 1 && b[1] <= 90) // 0x01, 0x5A
			{
				return true;
			}
			break;
		case -31: // 0xe1 0xE101,0xE15A
			if (b[1] >= 1 && b[1] <= 90) // 0x01, 0x5A
			{
				return true;
			}
			break;
		case -30: // 0xe2 0xE201,0xE253
			if (b[1] >= 1 && b[1] <= 83) // 0x01, 0x53
			{
				return true;
			}
			break;
		case -29: // 0xe3 0xE301,0xE34d
			if (b[1] >= 1 && b[1] <= 77) // 0x01, 0x4d
			{
				return true;
			}
			break;
		case -28: // 0xe4 0xE401,0xE44C
			if (b[1] >= 1 && b[1] <= 76) // 0x01, 0x4c
			{
				return true;
			}
			break;
		case -27: // 0xe5 0xE501,0xE537
			if (b[1] >= 1 && b[1] <= 55) // 0x01, 0x37
			{
				return true;
			}
			break;
		case 38: // 0xe0 0xE001,0xE05A
			if (b[1] >= -90 && b[1] <= 90) // 0x01, 0x5A
			{
				return true;
			}
			break;
		case 39: // 0xe0 0xE001,0xE05A
			if (b[1] >= -90 && b[1] <= 90) // 0x01, 0x5A
			{
				return true;
			}
			break;
		default:
			break;
		}
		return false;
	}

	public static boolean judgeIfSpecial5(byte[] c) {
		if (c.length != 2 && c.length != 4)
			return false;
		byte[] b = new byte[2];
		if (c.length == 4) {
			b[0] = c[2];
			b[1] = c[3];
		} else {
			b[0] = c[0];
			b[1] = c[1];
		}
		if (b[0] != 38 && b[0] != 39 && b[0] != 12 && b[0] != 14 && b[0] != 15) {
			return false;
		}
		switch (b[0]) {
		case 12: // 0xe0 0xE001,0xE05A
			if (b[1] >= -90 && b[1] <= 100) // 0x01, 0x5A
			{
				return true;
			}
			break;
		case 14: // 0xe0 0xE001,0xE05A
			if (b[1] >= -90 && b[1] <= 100) // 0x01, 0x5A
			{
				return true;
			}
			break;
		case 15: // 0xe0 0xE001,0xE05A
			if (b[1] >= -90 && b[1] <= 100) // 0x01, 0x5A
			{
				return true;
			}
			break;
		case 38: // 0xe0 0xE001,0xE05A
			if (b[1] >= -90 && b[1] <= 90) // 0x01, 0x5A
			{
				return true;
			}
			break;
		case 39: // 0xe0 0xE001,0xE05A
			if (b[1] >= -90 && b[1] <= 90) // 0x01, 0x5A
			{
				return true;
			}
			break;
		default:
			break;
		}
		return false;
	}

	public static int getDate(long currentTimeMillis, String string) {
		// TODO Auto-generated method stub
		SimpleDateFormat formatter = new SimpleDateFormat(string);
		Date date = new Date(currentTimeMillis);
		return Integer.parseInt(formatter.format(date));
	}

	public static String getRefundDate() {
		// TODO Auto-generated method stub
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		return formatter.format(date);
	}

	public static String getCurrentDate() {
		// TODO Auto-generated method stub
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		return formatter.format(date);
	}

	public static String getId(){
		String s = new Date().getTime() + Util.getVerifyCode();
		return s.substring(10, s.length());
	}
	
}
