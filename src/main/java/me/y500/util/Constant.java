package me.y500.util;

public class Constant {
	public final static String FLUSH_QUESTION_ANSWER_TYPE_OLD = "old";// 刷新获取历史回答
	public final static String FLUSH_QUESTION_ANSWER_TYPE_NEW = "new";// 刷新获取最新回答
	public final static int FLUSH_QUESTION_ANSWER_NUM = 5;// 默认消息条数
	
	public final static String SESSION_USERBEAN = "hotel_session_userbean";
	public final static String SESSION_WX = "hotel_session_wxuserinfo";
	
	public final static String LIST_TYPE = "new";
	
	public final static String IMAGE_PATH = "http://h.y500.me/image/";
	
	public final static String UPLOAD_PATH = "/usr/local/nginx/static/image/";
	
	public final static String IMAGE_ICON_PATH = "http://h.y500.me/image/icon/user_icon_default@2x.png";
	
	public final static int SEARCH_NUM = 10;// 默认搜索条数
	
}
