package me.y500.util;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;



/**
 * 日期Util类:负责日期、字符口互转
 * 
 * @author r
 * 
 */
public class DateUtil {
	private static String defaultDatePattern = "yyyy-MM-dd";

	/**
	 * 获得默认的 date pattern
	 * 
	 * @return
	 */
	public static String getDatePattern() {
		return defaultDatePattern;
	}

	/**
	 * 返回预设Format的当前日期字符串
	 * 
	 * @return
	 */
	public static String getToday() {
		Date today = new Date();
		return format(today);
	}

	/**
	 * 返回指定Format的当前日期字符串
	 * 
	 * @param ft
	 * @return
	 */
	public static String getToday(String ft) {
		Date today = new Date();
		return format(today, ft);
	}

	/**
	 * 使用预设Format格式化Date成字符串
	 * 
	 * @param date
	 * @return
	 */
	public static String format(Date date) {
		return date == null ? "" : format(date, getDatePattern());
	}

	/**
	 * 使用参数Format格式化Date成字符串
	 * 
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static String format(Date date, String pattern) {
		return date == null ? "" : new SimpleDateFormat(pattern).format(date);
	}

	/**
	 * 使用预设格式将字符串转为Date
	 * 
	 * @param strDate
	 * @return
	 * @throws ParseException
	 */
	public static Date parse(String strDate) throws ParseException {
		return (strDate == null || strDate.equals("")) ? null : parse(strDate, getDatePattern());
	}

	/**
	 * 使用参数Format将字符串转为Date
	 * 
	 * @param strDate
	 * @param pattern
	 * @return
	 * @throws ParseException
	 */
	public static Date parse(String strDate, String pattern)
			throws ParseException {
		return (strDate == null || strDate.equals("")) ? null : new SimpleDateFormat(pattern)
				.parse(strDate);
	}

	/**
	 * 在日期上增加分钟
	 * 
	 * @param date
	 * @param n
	 * @return
	 */
	public static Date addMinute(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MINUTE, n);
		return cal.getTime();
	}

	/**
	 * 在日期上增加小时
	 * 
	 * @param date
	 *            日期
	 * @param n
	 *            小时数
	 * @return
	 */
	public static Date addHour(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR_OF_DAY, n);
		return cal.getTime();
	}

	/**
	 * 在日期上增加数个整日
	 * 
	 * @param date
	 *            日期
	 * @param n
	 *            天数
	 * @return
	 */
	public static Date addDay(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, n);
		return cal.getTime();
	}

	/**
	 * 在日期上增加数个整月
	 * 
	 * @param date
	 *            日期
	 * @param n
	 *            月数
	 * @return
	 */
	public static Date addMonth(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, n);
		return cal.getTime();
	}

	/**
	 * 在日期上增加数个整年
	 * 
	 * @param date
	 *            日期
	 * @param n
	 *            年数
	 * @return
	 */
	public static Date addYear(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, n);
		return cal.getTime();
	}

	/**
	 * 取得指定日期的小时
	 * 
	 * @param date
	 *            日期
	 * @param n
	 *            小时数
	 * @return
	 */
	public static int getHour(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR_OF_DAY, n);
		return cal.get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * 取得指定日期的日
	 * 
	 * @param date
	 *            日期
	 * @param n
	 *            天数
	 * @return
	 */
	public static int getDay(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, n);
		return cal.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 返回补零的日
	 * 
	 * @param n
	 * @return
	 */
	public static String getDay(int n) {
		String str = "" + n;
		return (str.length() > 1 ? str : "0" + str);
	}

	/**
	 * 取得指定日期的月份
	 * 
	 * @param date
	 *            日期
	 * @param n
	 *            月数
	 * @return
	 */
	public static int getMonth(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, n);
		return cal.get(Calendar.MONTH);

	}

	/**
	 * 返回对应月数
	 * 
	 * @param n
	 * @return
	 */
	public static String getMonth(int n) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("0", "01");
		map.put("1", "02");
		map.put("2", "03");
		map.put("3", "04");
		map.put("4", "05");
		map.put("5", "06");
		map.put("6", "07");
		map.put("7", "08");
		map.put("8", "09");
		map.put("9", "10");
		map.put("10", "11");
		map.put("11", "12");

		return (String) map.get("" + n);
	}

	/**
	 * 取得指定日期的年份
	 * 
	 * @param date
	 *            日期
	 * @param n
	 *            年数
	 * @return
	 */
	public static int getYear(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, n);
		return cal.get(Calendar.YEAR);
	}

	/**
	 * 比较两个日期的大小:转为时间辍形式
	 * 
	 * @param date1
	 * @param date2
	 * @return-1 0 1
	 */
	public static int max(Date date1, Date date2) {
		int ret = -1;
		if (date1.getTime() > date2.getTime()) {
			ret = 1;
		} else if (date1.getTime() == date2.getTime()) {
			ret = 0;
		}
		return ret;
	}

	/**
	 * 检验输入是否为正确的日期格式(不含秒的任何情况),严格要求日期正确性,格式:yyyy-MM-dd HH:mm (需要的时候变为有时、分)
	 * 
	 * @param sourceDate
	 * @return
	 */
	public static boolean checkDate(String sourceDate) {
		if (sourceDate == null) {
			return false;
		}
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			dateFormat.setLenient(false);
			dateFormat.parse(sourceDate);
			return true;
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return false;
	}
	

	/**
	 * 检验输入是否为正确的日期格式(不含秒的任何情况),严格要求日期正确性,自定议格式:yyyy-MM-dd HH:mm (需要的时候变为有时、分)
	 * 
	 * @param sourceDate
	 * @param _format
	 * @return
	 */
	public static boolean checkDate(String sourceDate, String _format) {
		if (sourceDate == null) {
			return false;
		}
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(_format);
			dateFormat.setLenient(false);
			dateFormat.parse(sourceDate);
			return true;
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return false;
	}

	/**
	 * 返回两个日期之间相差几天
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static long getDifferDay(String startDate, String endDate) {
		long days = 0;
		try {
			Date start = parse(startDate);
			Date end = parse(endDate);
			days = (end.getTime() - start.getTime()) / (3600 * 24 * 1000);
		} catch (ParseException e) {
//			e.printStackTrace();
		}
		return days;
	}
	
	/**
	 * 获取两个日期间每天的日期集合
	 * 
	 * @param startDate
	 * @param 
	 * @return
	 */
	public static List<String> getBetDate(String startDate, String endDate) {
		List<String> dateList = new ArrayList<String>();
		long days = 0;
		try {
			long day = getDifferDay(startDate, endDate);
			for (int i = 0; i < day; i++) {
				 String date = format(addDay(parse(startDate), i));
				dateList.add(date);
			}
		} catch (ParseException e) {
//			e.printStackTrace();
		}
		return dateList;
	}	
	
	/**
	 * 返回两个日期之间相差多少分钟
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static long getDiffertime(String startDate, String endDate) {
		long days = 0;
		try {
			if(DateUtil.checkDate(startDate,"yyyy-MM-dd HH:mm") && DateUtil.checkDate(endDate,"yyyy-MM-dd HH:mm")){
				Date start = parse(startDate,"yyyy-MM-dd HH:mm");
				Date end = parse(endDate,"yyyy-MM-dd HH:mm");
				days = (end.getTime() - start.getTime())/(60 *1000);
			}
		} catch (ParseException e) {
//			e.printStackTrace();
		}
		return days;
	}
	
	/**
	 * 返回两个时间之间相差多少分钟
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static long getNewDiffertime(String startDate, String endDate) {
		long days = 0;
		try {
			if(DateUtil.checkDate(startDate,"HH:mm") && DateUtil.checkDate(endDate,"HH:mm")){
				Date start = parse(startDate,"HH:mm");
				Date end = parse(endDate,"HH:mm");
				days = (end.getTime() - start.getTime())/(60 *1000);
			}
		} catch (ParseException e) {
//			e.printStackTrace();
		}
		return days;
	}
	
	/**
	 * 返回两个时间之间相差多少小时
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static long getNewDiffertimeHour(String startDate, String endDate) {
		long days = 0;
		try {
			if(DateUtil.checkDate(startDate,"yyyy-MM-dd HH:mm") && DateUtil.checkDate(endDate,"yyyy-MM-dd HH:mm")){
				Date start = parse(startDate,"yyyy-MM-dd HH:mm");
				Date end = parse(endDate,"yyyy-MM-dd HH:mm");
				days = (end.getTime() - start.getTime())/(60 * 60 * 1000);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return days;
	}
	
	/**
	 * 时分比大小
	 * 
	 * @param startDate
	 * @return
	 */
	public static boolean  getDifferH(String startDate,String endDate) {
		boolean days = false;
		try {
			if(checkDate(startDate,"HH:mm") && checkDate(endDate,"HH:mm")){
				Date start = parse(startDate,"HH:mm");
				Date end = parse(endDate,"HH:mm");
				if(start.getTime()>=end.getTime()){
					days = true;
				}
			}
		} catch (ParseException e) {
//			e.printStackTrace();
//			throw new BuilderException(e);
		}
		return days;
	}
	
	/**
	 * 判断时间是不是0-4点之间
	 * 
	 * @param startDate
	 * @return
	 */
	public static boolean getDifferH(String startDate) {
		boolean days = false;
		try {
			if(checkDate(startDate,"HH:mm")){
				Date start = parse(startDate,"HH:mm");
				Date da1  = parse("00:00","HH:mm");
				Date da2  = parse("04:00","HH:mm");
				if(start.getTime()>=da1.getTime() && start.getTime()<=da2.getTime()){
					days = true;
				}
			}
		} catch (ParseException e) {
//			e.printStackTrace();
		}
		return days;
	}
	
	/**
	 * 输入一个日期获得周几
	 * 
	 * @param date
	 * @return
	 */
	public static int getDayOfWeek1(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int ret = cal.get(Calendar.DAY_OF_WEEK);
		return ret;
	}
	/**
	 * 输入一个日期获得周几
	 * 
	 * @param date
	 * @return
	 */
	public static int getDayOfWeek(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int ret = cal.get(Calendar.DAY_OF_WEEK);
		ret --;
		if (ret == 0)
			ret = 7;

		return ret;
	}
	/**
	 * 获得中文周名
	 * 
	 * @param dateStr
	 * @return
	 */
	public static String getWeek(String dateStr) {
		String week = "";
		Date date = null;
		try {
			date = DateUtil.parse(dateStr);
		} catch (Exception e) {
			date = new Date();
		}
		switch (getDayOfWeek1(date)) {
		case 1:
			week = "日";
			break;
		case 2:
			week = "一";
			break;
		case 3:
			week = "二";
			break;
		case 4:
			week = "三";
			break;
		case 5:
			week = "四";
			break;
		case 6:
			week = "五";
			break;
		case 7:
			week = "六";
			break;
		default:
			week = "";
		}
		return week;
	}

	/**
	 * 获得中文周名
	 * 
	 * @param date
	 * @return
	 */
	public static String getWeek(Date date) {
		String week = "";
		switch (getDayOfWeek1(date)) {
		case 1:
			week = "日";
			break;
		case 2:
			week = "一";
			break;
		case 3:
			week = "二";
			break;
		case 4:
			week = "三";
			break;
		case 5:
			week = "四";
			break;
		case 6:
			week = "五";
			break;
		case 7:
			week = "六";
			break;
		default:
			week = "";
		}
		return week;
	}

	// ///////////////
	/**
	 * 获得英文周名
	 * 
	 * @param dateStr
	 * @return
	 */
	public static String getEnglishWeek(String dateStr) {
		String week = "";
		Date date = null;
		try {
			date = DateUtil.parse(dateStr);
		} catch (Exception e) {
			date = new Date();
		}
		switch (getDayOfWeek1(date)) {
		case 1:
			week = "Sunday";
			break;
		case 2:
			week = "Monday";
			break;
		case 3:
			week = "Tuesday";
			break;
		case 4:
			week = "Wednesday";
			break;
		case 5:
			week = "Thursday";
			break;
		case 6:
			week = "Friday";
			break;
		case 7:
			week = "Saturday";
			break;
		default:
			week = "";
		}
		return week;
	}

	/**
	 * 获得英文周名
	 * 
	 * @param date
	 * @return
	 */
	public static String getEnglishWeek(Date date) {
		String week = "";
		switch (getDayOfWeek1(date)) {
		case 1:
			week = "Sunday";
			break;
		case 2:
			week = "Monday";
			break;
		case 3:
			week = "Tuesday";
			break;
		case 4:
			week = "Wednesday";
			break;
		case 5:
			week = "Thursday";
			break;
		case 6:
			week = "Friday";
			break;
		case 7:
			week = "Saturday";
			break;
		default:
			week = "";
		}
		return week;
	}

	/**
	 * 英文表达输出
	 * 
	 * @param strDate
	 *            yyyy-MM-dd
	 * @param ft
	 *            MMMM,d
	 * @return
	 */
	public static String getDateEnglishForm(String strDate, String ft) {
		String res = "";
		try {
			Date date = DateUtil.parse(strDate);
			// MMMM,d
			SimpleDateFormat format = new SimpleDateFormat(ft, Locale.ENGLISH);
			res = format.format(date);
		} catch (Exception e) {
			res = "";
		}
		return res;
	}

	/**
	 * 只取年月日数据，根据时间str，如果格式解析失败只提取当前时间
	 * @param dateStr "yyyy-MM-dd HH:mm:ss"
	 */
	public static String getYYYYMMHH(String dateStr) {
		Date date;
		try {
			date = parse(dateStr, "yyyy-MM-dd HH:mm:ss");
		} catch (Exception e) {
			 String curtime = format(new Date(), "HH:mm");					
             if (getDifferH(curtime)) {
            	 date = addDay(new Date(),-1);
             } else {
            	 date = new Date();
             }
		}
		return format(date);
	}

	/**
	 * 英文表达输出
	 * 
	 * @param date
	 * @param ft
	 *            MMMM,d
	 * @return
	 */
	public static String getDateEnglishForm(Date date, String ft) {
		String res = "";
		try {
			// MMMM,d
			SimpleDateFormat format = new SimpleDateFormat(ft, Locale.ENGLISH);
			res = format.format(date);
		} catch (Exception e) {
			res = "";
		}
		return res;
	}

	// ///////////
	/**
	 * 获得数字周名
	 * 
	 * @param dateStr
	 * @return
	 */
	public static String getWeekNum(String dateStr) {
		String week = "";
		Date date = null;
		try {
			date = DateUtil.parse(dateStr);
		} catch (Exception e) {
			date = new Date();
		}
		switch (getDayOfWeek1(date)) {
		case 1:
			week = "7";
			break;
		case 2:
			week = "1";
			break;
		case 3:
			week = "2";
			break;
		case 4:
			week = "3";
			break;
		case 5:
			week = "4";
			break;
		case 6:
			week = "5";
			break;
		case 7:
			week = "6";
			break;
		default:
			week = "";
		}
		return week;
	}

	/**
	 * 获得数字周名
	 * 
	 * @param date
	 * @return
	 */
	public static String getWeekNum(Date date) {
		String week = "";
		switch (getDayOfWeek1(date)) {
		case 1:
			week = "7";
			break;
		case 2:
			week = "1";
			break;
		case 3:
			week = "2";
			break;
		case 4:
			week = "3";
			break;
		case 5:
			week = "4";
			break;
		case 6:
			week = "5";
			break;
		case 7:
			week = "6";
			break;
		default:
			week = "";
		}
		return week;
	}
	
	/**
	 * 解析时间格式 HH:mm YYYY-MM-DD HH:mm:SS格式，在0-4点自动添加1天时间
	 * @param str
	 * @param toDay
	 * @return
	 */
	public static Date parseTime(String str, String toDay) {
		if (str != null && !str.equals("") && !str.equals("false")) {
			try {
				if (str.length() == 5 || str.length() == 4) {
					Date date = new Date();
					if (toDay != null && !getToday(defaultDatePattern).equals(toDay)) {
						date = parse(toDay, defaultDatePattern);
					}
					str = str.replace(":", "");
					int datetime = Integer.parseInt(str);
					StringBuilder sb = new StringBuilder();
					if (datetime < 400 && datetime >= 0) {
						sb.append(format(addDay(date, 1), "yyyy-MM-dd"));
					} else {
						sb.append(toDay);
					}
					sb.append(" ").append(str);
					return new SimpleDateFormat("yyyy-MM-dd HHmm").parse(sb.toString());
				} else {
					return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str);
				}
			} catch (Exception e) {
			}
		}
		return null;
	}
	
	/**
	 * 获得今天航班日期
	 * @return
	 */
	public static String getFlyToday() {
		Date today = new Date();
		int hours = Integer.parseInt(format(today, "HHmm"));
		if (hours < 400 && hours >= 0) {
			today = addDay(today, -1);
		}
		return format(today);
	}
	
	public static String formatDate(String date){
		try {
			Date d = DateUtil.parse(date, "yyyy-MM-dd");
			return DateUtil.format(d, "M月d日");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	 /**
     * 获取查询时间，当在凌晨0-4点时，使用昨天的日期
     * 
     * @author 
     * @return
     */
    public static String getSearchDate() {
        Calendar calendar = Calendar.getInstance();
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        if (hours >= 0 && hours < 4) {
            calendar.add(Calendar.DAY_OF_MONTH, -1);
            return DateUtil.format(DateUtil.addDay(new Date(), -1));
        }
        return DateUtil.getToday();
    }
    
    /**
	 * 返回预设Format的当前日期字符串
	 * 
	 * @return
	 */
	public static String getTomorrow() {
		Date today = new Date();
		Date tomorrow = addDay(today, 1);
		return format(tomorrow);
	}
	
	public static void main(String[] args) {
		try {
	//		System.out.println(getDiffertime("2013-09-11 15:00:00", "2013-09-11 13:00:00"));
			System.out.println(DateUtil.parse("2013-12-18 00:00:00").getTime());
//			List<String> s = getBetDate("2013-10-31","2013-11-01");
			System.out.println(getDifferH(DateUtil.format(new Date(), "HH:mm")));
	        System.out.println(DateUtil.format(new Date(1406672644847l),"yyyy-MM-dd HH:mm:ss")); 
	        System.out.println(DateUtil.formatDate("2014-12-20"));
	        
	        System.out.println(DateUtil.getDiffertime("2014-10-27 15:12:00", "2014-10-28 15:25:00"));
	        System.out.println(DateUtil.getDifferDay("2014-11-05", "2014-11-07"));
	        
	        
	        System.out.println(DateUtil.getDifferDay("2014-11-28", "2014-11-30"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
	}
}