package me.y500.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.ServletContext;

import me.y500.ao.SearchAo;
import me.y500.bo.UploadBo;
import me.y500.mongodb.dataobject.Question;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.log4j.Logger;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

public class SearchIndexTimerTask extends TimerTask{
	private final static Logger logger = Logger.getLogger(SearchIndexTimerTask.class);
	
	public SearchIndexTimerTask(){
	     SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	 }
	 @Override
	public void run() {
		try {
			logger.info("comming");
			this.executePostMethodStatus("http://120.24.92.237:8199/trip/keysearch/index", new HashMap<String, String>(), 1000000000);
			
		} catch (Exception e) {
			logger.info(Util.exceptionMsg(e));
		}

	}
	
	
	public static String executePostMethodStatus(String url,
			Map<String, String> paramMap, int timeout) throws HttpException,
			IOException {
		HttpClient client = new HttpClient();
		client.getParams().setIntParameter("http.socket.timeout", timeout);
		BufferedReader reader = null;
		PostMethod method = null;
		try {
			method = new PostMethod(url);
			method.getParams().setParameter(
					HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");
			client.getHttpConnectionManager().getParams()
					.setConnectionTimeout(timeout);
			client.getHttpConnectionManager()
					.getParams()
					.setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET,
							"UTF-8");
			ArrayList<NameValuePair> paramList = new ArrayList<NameValuePair>();
			for (String key : paramMap.keySet()) {
				NameValuePair pair = new NameValuePair(key, paramMap.get(key));
				paramList.add(pair);
			}
			method.setRequestBody(paramList.toArray(new NameValuePair[paramList
					.size()]));
			client.executeMethod(method);
			String response = new String(method.getResponseBodyAsString());
			response = new String(response.getBytes("utf-8"), "utf-8");
			return response;
		} finally {
			if (reader != null) {
				reader.close();
			}
			if (null != method) {
				method.releaseConnection();
			}
		}
	}
	
}
