package me.y500.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.StringUtils;

/**
 * AES Coder<br/>
 * secret key length: 128bit, default: 128 bit<br/>
 * mode: ECB/CBC/PCBC/CTR/CTS/CFB/CFB8 to CFB128/OFB/OBF8 to OFB128<br/>
 * padding: Nopadding/PKCS5Padding/ISO10126Padding/
 * 
 * @author Aub
 * 
 */
public class AESCoder {

	/**
	 * 密钥算法
	 */
	private static final String KEY_ALGORITHM = "AES";

	private static final String DEFAULT_CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";
	
	private static String KEY = "http://innapp.cn";

	/**
	 * 初始化密钥
	 * 
	 * @return byte[] 密钥
	 * @throws Exception
	 */
	public static byte[] initSecretKey() {
		String keystr = "huoli-aes-20130408-zgq";
		// 返回生成指定算法的秘密密钥的 KeyGenerator 对象
		KeyGenerator kg = null;
		try {
			kg = KeyGenerator.getInstance(KEY_ALGORITHM);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return new byte[0];
		}
		// 初始化此密钥生成器，使其具有确定的密钥大小
		// AES 要求密钥长度为 128
		// kg.init(128);
		kg.init(128, new SecureRandom(keystr.getBytes()));
		// 生成一个密钥
		SecretKey secretKey = kg.generateKey();

		return secretKey.getEncoded();
	}

	/**
	 * 转换密钥
	 * 
	 * @param key
	 *            二进制密钥
	 * @return 密钥
	 */
	public static Key toKey(byte[] key) {
		// 生成密钥
		return new SecretKeySpec(key, KEY_ALGORITHM);
	}

	/**
	 * 加密
	 * 
	 * @param data
	 *            待加密数据
	 * @param key
	 *            密钥
	 * @return byte[] 加密数据
	 * @throws Exception
	 */
	public static byte[] encrypt(byte[] data, Key key) throws Exception {
		return encrypt(data, key, DEFAULT_CIPHER_ALGORITHM);
	}

	/**
	 * 加密
	 * 
	 * @param data
	 *            待加密数据
	 * @param key
	 *            二进制密钥
	 * @return byte[] 加密数据
	 * @throws Exception
	 */
	public static byte[] encrypt(byte[] data, byte[] key) throws Exception {
		return encrypt(data, key, DEFAULT_CIPHER_ALGORITHM);
	}

	/**
	 * 加密
	 * 
	 * @param data
	 *            待加密数据
	 * @param key
	 *            二进制密钥
	 * @param cipherAlgorithm
	 *            加密算法/工作模式/填充方式
	 * @return byte[] 加密数据
	 * @throws Exception
	 */
	public static byte[] encrypt(byte[] data, byte[] key, String cipherAlgorithm)
			throws Exception {
		// 还原密钥
		Key k = toKey(key);
		return encrypt(data, k, cipherAlgorithm);
	}

	/**
	 * 加密
	 * 
	 * @param data
	 *            待加密数据
	 * @param key
	 *            密钥
	 * @param cipherAlgorithm
	 *            加密算法/工作模式/填充方式
	 * @return byte[] 加密数据
	 * @throws Exception
	 */
	public static byte[] encrypt(byte[] data, Key key, String cipherAlgorithm)
			throws Exception {
		// 实例化
		Cipher cipher = Cipher.getInstance(cipherAlgorithm);
		// 使用密钥初始化，设置为加密模式
		cipher.init(Cipher.ENCRYPT_MODE, key);
		// 执行操作
		return cipher.doFinal(data);
	}

	/**
	 * 解密
	 * 
	 * @param data
	 *            待解密数据
	 * @param key
	 *            二进制密钥
	 * @return byte[] 解密数据
	 * @throws Exception
	 */
	public static byte[] decrypt(byte[] data, byte[] key) throws Exception {
		return decrypt(data, key, DEFAULT_CIPHER_ALGORITHM);
	}

	/**
	 * 解密
	 * 
	 * @param data
	 *            待解密数据
	 * @param key
	 *            密钥
	 * @return byte[] 解密数据
	 * @throws Exception
	 */
	public static byte[] decrypt(byte[] data, Key key) throws Exception {
		return decrypt(data, key, DEFAULT_CIPHER_ALGORITHM);
	}

	/**
	 * 解密
	 * 
	 * @param data
	 *            待解密数据
	 * @param key
	 *            二进制密钥
	 * @param cipherAlgorithm
	 *            加密算法/工作模式/填充方式
	 * @return byte[] 解密数据
	 * @throws Exception
	 */
	public static byte[] decrypt(byte[] data, byte[] key, String cipherAlgorithm)
			throws Exception {
		// 还原密钥
		Key k = toKey(key);
		return decrypt(data, k, cipherAlgorithm);
	}

	/**
	 * 解密
	 * 
	 * @param data
	 *            待解密数据
	 * @param key
	 *            密钥
	 * @param cipherAlgorithm
	 *            加密算法/工作模式/填充方式
	 * @return byte[] 解密数据
	 * @throws Exception
	 */
	public static byte[] decrypt(byte[] data, Key key, String cipherAlgorithm)
			throws Exception {
		// 实例化
		Cipher cipher = Cipher.getInstance(cipherAlgorithm);
		// 使用密钥初始化，设置为解密模式
		cipher.init(Cipher.DECRYPT_MODE, key);
		// 执行操作
		return cipher.doFinal(data);
	}

	private static String showByteArray(byte[] data) {
		if (null == data) {
			return null;
		}
		StringBuilder sb = new StringBuilder("{");
		for (byte b : data) {
			sb.append(b).append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append("}");
		return sb.toString();
	}

	/**
	 * 将二进制转换成16进制
	 * 
	 * @param buf
	 * @return
	 */
	public static String parseByte2HexStr(byte buf[]) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < buf.length; i++) {
			String hex = Integer.toHexString(buf[i] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			sb.append(hex.toUpperCase());
		}
		return sb.toString();
	}

	/**
	 * 将16进制转换为二进制
	 * 
	 * @param hexStr
	 * @return
	 */
	public static byte[] parseHexStr2Byte(String hexStr) {
		if (hexStr.length() < 1)
			return null;
		byte[] result = new byte[hexStr.length() / 2];
		for (int i = 0; i < hexStr.length() / 2; i++) {
			int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);
			int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2),
					16);
			result[i] = (byte) (high * 16 + low);
		}
		return result;
	}
	
	 /**
     * 先unbase64,再解密，再去对齐，再解压缩
     *
     * @param content
     * @return
     * @throws Exception
     */
    public static String decryptHttpRes(String content) throws Exception {
    	content = Util.replaceBlank(content);
        byte[] unbse64 = Base64Coder.decode(content);
        byte[] decryptbyte = AESCoder.decryptByte(unbse64, KEY);
        String ungzip = new String(decryptbyte);
        return ungzip;
    }
    
    
    public static String decryptHex(String content, String key) throws Exception {
        if (content == null || "".equalsIgnoreCase(content)) {
            return "";
        }
        byte[] data = parseHexStr2Byte(content);
        return new String(decryptByte(data, key));
    }
    
    public static byte[] decryptByte(byte[] data, String key) throws Exception {
        if (data == null || data.length == 0) {
            return new byte[0];
        }
        byte[] keyData = initSecretKey(key);
        byte[] encryptData = decrypt(data, keyData);
        return encryptData;
    }
    
    
    public static String encryptData(String data) throws Exception {
      byte[] b = null;
      if (!StringUtils.isEmpty(data)) {
    	  ByteArrayOutputStream bos = new ByteArrayOutputStream();
    	  GZIPOutputStream gzip = new GZIPOutputStream(bos);
    	  gzip.write(data.getBytes());
    	  gzip.finish();
    	  gzip.close();
    	  b = bos.toByteArray();
    	  bos.close(); 
 
      }
      return new String(Base64Coder.encode(AESCoder.encryptTobyte(b, KEY)));
    }
    
    public static byte[] encryptTobyte(byte[] data) throws Exception {
        return encryptTobyte(data, KEY);
    }
    
    public static byte[] encryptTobyte(byte[] data, String key) throws Exception {
        if (data == null || data.length == 0) {
            return new byte[0];
        }
//      byte[] keyData = parseHexStr2Byte(key);
        byte[] keyData = initSecretKey(key);
        byte[] encryptData = encrypt(data, keyData);
        return encryptData;
    }
    
    /**
     * 初始化密钥
     *
     * @return byte[] 密钥
     * @throws Exception
     */
    public static byte[] initSecretKey(String keystr) {
        try {
            return keystr.getBytes("utf-8");
        } catch (Exception e) {
            return new byte[0];
        }
    }

	public static void main(String[] args) throws Exception {
//		String huoliKey = "4F6DEF26E1E6917F2A4F27D754B70B6D";
//
//		Key k = toKey(parseHexStr2Byte(huoliKey));
//
//		String data = "89860311700252047295" + new java.util.Date().getTime();
//		System.out.println("加密前数据: string:" + data);
//		System.out.println("加密前数据: byte[]:" + showByteArray(data.getBytes()));
//		System.out.println();
//		byte[] encryptData = encrypt(data.getBytes(), k);
//		String encryptStr = parseByte2HexStr(encryptData);
//		System.out.println("加密后数据: byte[]:" + showByteArray(encryptData));
//		System.out.println("加密后数据: hexStr:" + encryptStr);
//		System.out.println();
//		byte[] decryptData = decrypt(parseHexStr2Byte(encryptStr), k);
//		System.out.println("解密后数据: byte[]:" + showByteArray(decryptData));
//		System.out.println("解密后数据: string:" + new String(decryptData));

		
		System.out.println(AESCoder.decryptHttpRes("DIBpd4SuRhvid5N4TxlEaI57mjs4ksDKoiBmNJlXZqMMQSappPNI5pC/bxCKGAUOpOzEWOnzLt2yVTjEMqdaLjUU5lp6InnGKgI3cPqOYmiVhNpdqw+Mnp+oJrGihm2I4Rk66wnEorXYHhQYcYLMrAVncY0XlxNhW5pQA3K4svkCgFoqG0R+rWWWAZVR2yhRTnn/xpFMRjd15gSElBBcoXGGYcV4MoPCQCWgceDA/4dxYx+FDpjRXvrzidQkBPsz79gRq83cVrboEzepfs+uTenw+gl5D0fbczq6szYjV2gRl0fZFXdvm+UT+jAYTDRK4ulZKtgjd5vAhdqto9tuxwPgL4e86p/hSf131RMLZFNGuknGRvfM0EyvTovql0SXHSHKoaWcU81AAeahi4DCnf3WHveMWMdl6ridQyyuttaTblpgLFpuOEGWJjeP/31JparNJDkShRqIpK0MMv5FqN80uBIS6nTGkBnGQyuTlr31WsdZd9xPVyS4BuJghk1Q"));
		
		System.out.println(AESCoder.encryptData("1234567890"));
	}
}