package me.y500.util;

import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Repository
public class IndexTimer implements ServletContextListener{
	private final static Logger logger = Logger.getLogger(IndexTimer.class);
	private Timer timer;
	private SearchIndexTimerTask task;
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		logger.info("begin");
		 timer = new Timer(true);
		 task = new SearchIndexTimerTask();
		  System.out.println("定时器已启动");
		  timer.schedule(task, 1000, 24 * 60 * 1000);
		  logger.info("已经添加任务调度表");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		timer.cancel();
		System.out.println("定时器已销毁");
	}

}
